<?php

declare(strict_types=1);

namespace Benhauer\Salesmanago\Test\Unit\Block\Tracking;

use PHPUnit\Framework\MockObject\MockObject;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Block\Tracking\Monitoring;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Config as ConfModel;
use Benhauer\Salesmanago\Test\Unit\TestCase;

class MonitoringTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var Monitoring
     */
    protected $block;

    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var Context
     */
    public $context;

    /**
     * @var MockObject|FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * @var ConfModel|MockObject
     */
    public $confModel;

    /**
     * @var StoreInterface|MockObject
     */
    public $store;

    /**
     * Basic test setup
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->context          = $this->createMock(Context::class);
        $this->factoryOrganizer = $this->createMock(FactoryOrganizer::class);
        $this->confModel        = $this->createMock(ConfModel::class);

        $this->store = $this->createMock(StoreInterface::class);
        $this->store
            ->method('getId')
            ->willReturn(1);

        $this->storeManager = $this->createMock(StoreManagerInterface::class);
        $this->storeManager
            ->method('getStore')
            ->willReturn($this->store);

        $this->context
            ->method('getStoreManager')
            ->willReturn($this->storeManager);
    }

    /**
     * Test generate monitoring code success
     *
     * @return void
     */
    public function testGenerateNotEmptyMonitoringCodeSuccess(): void
    {
        Conf::getInstance()->setClientId('notEmptySmClientId');
        Conf::getInstance()->setDisableMonitoringCode(false);

        $this->confModel
            ->method('getCachedConf')
            ->willReturn(Conf::getInstance());

        $this->block = new Monitoring(
            $this->context,
            $this->factoryOrganizer,
            $this->confModel
        );

        $code = $this->block->getMonitoringCode();
        $this->assertTrue(!empty($code));
    }

    /**
     * Test generate empty monitoring code success
     *
     * @return void
     */
    public function testGenerateEmptyMonitoringCodeSuccess(): void
    {
        Conf::getInstance()->setClientId(null);
        Conf::getInstance()->setDisableMonitoringCode(false);

        $this->confModel
            ->method('getCachedConf')
            ->willReturn(Conf::getInstance());

        $this->block = new Monitoring(
            $this->context,
            $this->factoryOrganizer,
            $this->confModel
        );

        $code = $this->block->getMonitoringCode();
        $this->assertTrue(empty($code));
    }

    /**
     * Test disable monitoring code success
     *
     * @return void
     */
    public function testDisableMonitoringCodeSuccess(): void
    {
        Conf::getInstance()->setClientId(null);
        Conf::getInstance()->setDisableMonitoringCode(true);

        $this->confModel
            ->method('getCachedConf')
            ->willReturn(Conf::getInstance());

        $this->block = new Monitoring(
            $this->context,
            $this->factoryOrganizer,
            $this->confModel
        );

        $code = $this->block->getMonitoringCode();
        $this->assertTrue(empty($code));
    }
}
