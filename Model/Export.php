<?php

namespace Benhauer\Salesmanago\Model;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Locale\Resolver;
use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Customer\Model\AddressFactory;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Newsletter\Model\ResourceModel\Subscriber\Collection as SubscriberCollection;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\ResourceModel\Quote\Collection as QuoteCollection;

use SALESmanago\Entity\Contact\Address;
use SALESmanago\Entity\Contact\Contact;
use SALESmanago\Entity\Contact\Options;
use SALESmanago\Entity\Event\Event;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\EventsCollection;
use SALESmanago\Model\Collections\ContactsCollection;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Model\Exports\ExportQuotes;

/**
 * @deprecated since v4.1.2 please use Model/Exports/ models instead
 * @see \Benhauer\Salesmanago\Model\Exports\Export
 */
class Export extends MagentoAbstractModel
{
    public const
        EXP_PACKAGE_SIZE            = 300,
        CUSTOMER_COLLECTION_TABLE   = 'customer_entity',
        SUBSCRIBER_COLLECTION_TABLE = 'newsletter_subscriber',
        EVENT_TYPE_PURCHASE         = 'PURCHASE';

    /**
     * @var string[]
     */
    public static $exportTypes      = [
        'exportSubscribers' => 'exportSubscribers',
        'exportContacts'    => 'exportContacts',
        'exportEvents'      => 'exportEvents',
        'exportQuotes'      => 'exportQuotes'
    ];

    /**
     * @var string[]
     */
    public static $exportOrderTypes = [
        'complete',
        'closed',
        'pending'
    ];

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerCollectionFactory
     */
    protected $customerFactory;

    /**
     * @var SubscriberCollectionFactory
     */
    protected $subscriberFactory;

    /**
     * @var ContactsCollection
     */
    protected $contactsCollection;

    /**
     * @var EventsCollection
     */
    protected $eventsCollection;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderFactory;

    /**
     * @var AddressFactory
     */
    protected $addressFactory;

    /**
     * @var Event
     */
    protected $Event;

    /**
     * @var Address
     */
    protected $Address;

    /**
     * @var Contact
     */
    protected $Contact;

    /**
     * @var Options
     */
    protected $Options;

    /**
     * @var QuoteCollectionFactory
     */
    public $quoteCollectionFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfigInterface;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Export constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param CustomerCollectionFactory $customerFactory
     * @param SubscriberCollectionFactory $subscriberFactory
     * @param OrderCollectionFactory $orderFactory
     * @param AddressFactory $addressFactory
     * @param TimezoneInterface $timezone
     * @param ContactsCollection $contactsCollection
     * @param EventsCollection $eventsCollection
     * @param Event $Event
     * @param Address $Address
     * @param Contact $Contact
     * @param Options $Options
     * @param QuoteCollectionFactory $quoteCollectionFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param ProductRepository $productRepository
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager,
        CustomerCollectionFactory $customerFactory,
        SubscriberCollectionFactory $subscriberFactory,
        OrderCollectionFactory $orderFactory,
        AddressFactory $addressFactory,
        TimezoneInterface $timezone,
        ContactsCollection $contactsCollection,
        EventsCollection $eventsCollection,
        Event $Event,
        Address $Address,
        Contact $Contact,
        Options $Options,
        QuoteCollectionFactory $quoteCollectionFactory,
        ScopeConfigInterface $scopeConfigInterface,
        ProductRepository $productRepository
    ) {
        parent::__construct(
            $context,
            $registry
        );
        $this->storeManager           = $storeManager;
        $this->customerFactory        = $customerFactory;
        $this->subscriberFactory      = $subscriberFactory;
        $this->orderFactory           = $orderFactory;
        $this->addressFactory         = $addressFactory;
        $this->timezone               = $timezone;
        $this->contactsCollection     = $contactsCollection;
        $this->eventsCollection       = $eventsCollection;
        $this->Event                  = $Event;
        $this->Address                = $Address;
        $this->Contact                = $Contact;
        $this->Options                = $Options;
        $this->quoteCollectionFactory = $quoteCollectionFactory;
        $this->scopeConfigInterface   = $scopeConfigInterface;
        $this->productRepository      = $productRepository;
    }

    /**
     * Get customer collection based on request parameters
     *
     * @param mixed $params
     * @param int $store_id
     * @return Collection
     */
    public function getCustomersCollection($params, $store_id = 0)
    {
        $dataRange           = $this->setDataRange($params["dateFrom"], $params["dateTo"]);
        $lastExportedPackage = empty($params["lastExportedPackage"]) ? 0 : (int) $params["lastExportedPackage"];

        try {
            $customerFactory = $this->customerFactory
                ->create()
                ->addFieldToSelect(['*'])
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->setCurPage($lastExportedPackage+1);

            $customerFactory->addFieldToFilter(
                'created_at',
                ['from' => $dataRange['from'], 'to' => $dataRange['to'], 'date' => true ]
            );

            if ($store_id !== 0) {
                $customerFactory->addFieldToFilter('store_id', ['eq' => $store_id]);
            }

            return $customerFactory;

        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Gets subscriber collection based on request
     *
     * @param mixed $params
     * @param int $store_id
     * @return SubscriberCollection
     */
    public function getSubscribersCollection($params, $store_id = 0)
    {
        $dataRange           = $this->setDataRange($params["dateFrom"], $params["dateTo"]);
        $lastExportedPackage = empty($params["lastExportedSubscribersPackage"])
            ? 0
            : (int) $params["lastExportedSubscribersPackage"];

        try {
            $subscriberFactory = $this->subscriberFactory->create();

            $subscriberFactory
                ->addFieldToSelect(['*'])
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->addFieldToFilter('customer_id', ['eq' => '0'])
                ->addFieldToFilter(
                    'change_status_at',
                    ['from' => $dataRange['from'], 'to' => $dataRange['to'], 'date' => true ]
                )
                ->setCurPage($lastExportedPackage);

            if ($store_id !== 0) {
                $subscriberFactory->addFieldToFilter('store_id', ['eq' => $store_id]);
            }

            return $subscriberFactory;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Get orders collection based on request params
     *
     * @param mixed $params
     * @param int $store_id
     * @return OrderCollection
     */
    public function getOrdersCollection($params, $store_id = 0)
    {
        $dataRange              = $this->setDataRange($params["dateFrom"], $params["dateTo"]);
        $lastExportedPackage    = empty($params["lastExportedPackage"]) ? 0 : (int) $params["lastExportedPackage"];

        try {
            $orderFactory = $this->orderFactory
                ->create()
                ->addFieldToFilter('status', ['in' => self::$exportOrderTypes])
                ->addFieldToSelect(['*'])
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->setCurPage($lastExportedPackage+1);

            $orderFactory->addFieldToFilter(
                'created_at',
                ['from' => $dataRange['from'], 'to' => $dataRange['to'], 'date' => true ]
            );

            if ($store_id !== 0) {
                $orderFactory->addFieldToFilter('store_id', ['eq' => $store_id]);
            }

            return $orderFactory;

        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Get Quotes collection to export
     *
     * @param mixed $params
     * @param int $store_id
     * @return QuoteCollection
     * @todo must be moved to Benhauer\Salesmanago\Model\Exports\ExportQuotes after refactoring
     */
    public function getQuotesCollection($params, $store_id = 0)
    {
        try {
            $dataRange           = $this->setDataRange($params["dateFrom"], $params["dateTo"]);
            $lastExportedPackage = empty($params["lastExportedPackage"])
                ? 0
                : (int) $params["lastExportedPackage"];

            $quoteCollection = $this->quoteCollectionFactory
                ->create()
                ->addFieldToSelect([
                    'customer_id',
                    'customer_email',
                    'customer_firstname',
                    'customer_middlename',
                    'customer_lastname',
                    'grand_total',
                    'subtotal',
                    'items_qty',
                    'orig_order_id',
                    'quote_currency_code',
                    'entity_id',
                    'created_at',
                    'store_id',
                    'quote_currency_code'
                ])
                ->addFieldToFilter(
                    'customer_email',
                    ['notnull' => true]
                )
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->setCurPage($lastExportedPackage+1);

            if ($store_id !== 0) {
                $quoteCollection->addFieldToFilter('store_id', ['eq' => $store_id]);
            }

            $quoteCollection->addFieldToFilter(
                'created_at',
                [
                    'from' => $dataRange['from'],
                    'to'   => $dataRange['to'],
                    'date' => true
                ]
            );

            return $quoteCollection;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Map data to sm contacts collection
     *
     * @param mixed $collection
     * @param mixed $subscriber
     * @param mixed $exportTags
     * @return ContactsCollection
     * @throws Exception
     */
    public function prepareContactsToExport($collection, $subscriber, $exportTags)
    {
        $isCustomer = ($collection->getMainTable() == self::CUSTOMER_COLLECTION_TABLE) ? true : false;

        foreach ($collection as $customer) {
            $customerEmail      = $customer->getEmail();
            $billingAddressId   = $customer->getDefaultBilling();

            if ($billingAddressId) {
                $billingAddress = $this->addressFactory->create()->load($billingAddressId);
                $addr           = $billingAddress->getData();
            } else {
                $addr = [];
            }

            $cSubscribed        = $subscriber->loadByEmail($customerEmail);
            $isSubscribed       = ($customerEmail != $cSubscribed->getEmail())
                ? false
                : boolval($cSubscribed->isSubscribed());
            $exportTag          = explode(",", $exportTags);

            $customerBirthday   = (!empty($customer->getDob())) ? date('Ymd', strtotime($customer->getDob())) : '';

            $Contact = clone $this->Contact;
            $Address = clone $this->Address;
            $Options = clone $this->Options;

            $Options
                ->setTags($exportTag)
                ->setIsSubscribed($isSubscribed)
                ->setCreatedOn(time());

            $Address
                ->setStreetAddress($addr ? $addr['street'] : '')
                ->setZipCode($addr ? $addr['postcode'] : '')
                ->setCity($addr ? $addr['city'] : '')
                ->setCountry($addr ? $addr['country_id'] : '');

            $Contact
                ->setName($customer->getName())
                ->setEmail($customerEmail)
                ->setCompany($addr ? $addr['company'] : '')
                ->setExternalId($customer->getId())
                ->setPhone($addr ? $addr['telephone'] : '')
                ->setFax($addr ? $addr['fax'] : '')
                ->setBirthday($customerBirthday)
                ->setOptions($Options)
                ->setAddress($Address);

            try {
                $this->contactsCollection->addItem($Contact);
            } catch (Exception $e) {
                $this->_logger->critical($e->getViewMessage());
            }
        }

        return $this->contactsCollection;
    }

    /**
     * Map data to sm events collection
     *
     * @param mixed $collection
     * @param mixed $subscriber
     * @return EventsCollection
     * @throws NoSuchEntityException
     */
    public function prepareEventsToExport($collection, $subscriber)
    {
        foreach ($collection as $order) {
            $Event      = clone $this->Event;
            $products   = $order->getAllVisibleItems();

            $store      = $this->storeManager->getStore();
            $storeUrl   = $store->getBaseUrl(UrlInterface::URL_TYPE_WEB);

            switch (Conf::getInstance()->getProductTypeId()) {
                case EventModel::$productIdDetail['sku']:
                    $mainProductId = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getSku();
                            },
                            $products
                        )
                    );
                    $detail3 = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getProductId();
                            },
                            $products
                        )
                    );
                    break;

                default:
                    $mainProductId = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getProductId();
                            },
                            $products
                        )
                    );
                    $detail3 = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getSku();
                            },
                            $products
                        )
                    );
            }

            try {
                $categoriesIds = implode(
                    ",",
                    array_map(
                        function ($product) {
                            $categoryIds = $this->productRepository
                                ->get($product->getSku())
                                ->getCategoryIds();

                            return empty($categoryIds) ? 'ncat' : implode('/', $categoryIds);
                        },
                        $products
                    )
                );
            } catch (\Exception $e) {
                $categoriesIds = '';
            }

            try {
                $origData = $order->getOrigData()['shipping_description'];
                $shippingDescription = (empty($origData) || !isset($origData['shipping_description']))
                    ? ''
                    : $origData['shipping_description'];

                $metadata = [
                    'metadata' => [
                        'from'         => 'exportEventsPurchase',
                        'timeOfAction' => time()
                    ]
                ];

                $this->eventsCollection->addItem(
                    $Event
                        ->setEmail($order->getCustomerEmail())
                        ->setContactExtEventType(Event::EVENT_TYPE_PURCHASE)
                        ->setProducts($mainProductId)
                        ->setDescription($order->getPayment()->getMethod())
                        ->setDate(strtotime($order->getData('created_at')))
                        ->setExternalId($order->getRealOrderId())
                        ->setLocation(Conf::getInstance()->getLocation())
                        ->setValue((float) $order->getGrandTotal())
                        ->setShopDomain($storeUrl)
                        ->setDetails([
                            1 => implode(
                                ",",
                                array_map(
                                    function ($product) {
                                        return $product->getName();
                                    },
                                    $products
                                )
                            ),
                            2 => implode(
                                "/",
                                array_map(
                                    function ($product) {
                                        return (int)$product->getQtyOrdered();
                                    },
                                    $products
                                )
                            ),
                            3 => $detail3,
                            4 => $order->getOrderCurrencyCode(),
                            5 => $shippingDescription,
                            6 => $order->getPayment()->getMethod(),
                            7 => ($order->getShippingAddress() == null)
                                ? ''
                                : $order->getShippingAddress()->getCountryId(),
                            8 => $categoriesIds,
                            9 => $this->getStoreLocale((int) $order->getStoreId()),
                            10 => json_encode($metadata)
                        ])
                );
            } catch (Exception $e) {
                $this->_logger->critical($e->getViewMessage());
            } catch (\Exception $e) {
                $this->_logger->critical($e->getMessage());
            }
        }

        return $this->eventsCollection;
    }

    /**
     * Function called by first AJAX query
     *
     * @param mixed $data
     * @param int $storeId
     * @return array
     */
    public function setDataToExport($data, $storeId)
    {
        switch ($data['exportType']) {
            case self::$exportTypes['exportContacts']:
                $SubscribersCollection = $this->getSubscribersCollection($data, $storeId);
                $SubscribersCollectionSize = $SubscribersCollection->getSize();

                $data['subscriberLoops'] = (int) ceil($SubscribersCollectionSize / self::EXP_PACKAGE_SIZE);
                $data['lastExportedSubscribersPackage'] = 0;

                $DataCollection = $this->getCustomersCollection($data, $storeId);
                $DataCollectionSize = $DataCollection->getSize();

                $TotalToExport = $SubscribersCollectionSize + $DataCollectionSize;
                break;

            case self::$exportTypes['exportEvents']:
                $DataCollection     = $this->getOrdersCollection($data, $storeId);
                $DataCollectionSize = $DataCollection->getSize();
                $TotalToExport      = $DataCollectionSize;
                break;

            case ExportQuotes::$exportTypes['exportQuotes']:
                $DataCollection     = $this->getQuotesCollection($data, $storeId);
                $DataCollectionSize = $DataCollection->getSize();
                $TotalToExport      = $DataCollectionSize;
                break;

            default:
                $this->_logger->critical('Undefined export type');
                return;
        }

        $dateRange = $this->setDataRange($data["dateFrom"], $data["dateTo"]);

        $data['dateFrom']               = $dateRange['from'];
        $data['dateTo']                 = $dateRange['to'];

        $data['loops']                  = (int) ceil($DataCollectionSize / self::EXP_PACKAGE_SIZE);
        $data['startDate']              = date('d.m.Y H:i', time());
        $data['packageSize']            = self::EXP_PACKAGE_SIZE;
        $data['totalToExport']          = $TotalToExport;
        $data['lastExportedPackage']    = 0;
        $data['orderTypes']             = !empty(self::$exportOrderTypes) ? implode(',', self::$exportOrderTypes) : '';

        return $data;
    }

    /**
     * Sets data range to filter collections to exports
     *
     * @param string $from
     * @param string $to
     * @return array
     */
    protected function setDataRange($from, $to)
    {
        if (empty($from)) {
            $from = 946688461; //default old date couldn't be 0
        } elseif (is_numeric($from)) {
            $from = (int) $from;
        }

        $from = $this->timezone->date($from)->format('Y-m-d H:i:s');

        if (empty($to)) {
            $to = time();
        } elseif (is_numeric($to)) {
            $to = (int) $to;
        }

        $to = $this->timezone->date($to)->format('Y-m-d H:i:s');

        return [
            'to' => strtotime($to) + 86400,
            'from' => strtotime($from)
        ];
    }

    /**
     * Sets order types which must be exported
     *
     * @param mixed $param
     * @return bool
     */
    public function setOrdersTypesToExport($param = null)
    {
        if (empty($param)) {
            return false;
        }

        self::$exportOrderTypes = explode(',', $param);
        return true;
    }

    /**
     * Get store locale by store id
     *
     * @param int $storeId
     */
    protected function getStoreLocale($storeId = 0)
    {
        try {
            $currentLocaleCode = $this->scopeConfigInterface
                ->getValue('general/locale/code', ScopeInterface::SCOPE_STORE, $storeId);
            return strstr($currentLocaleCode, '_', true);
        } catch (\Exception $e) {
            return strtoupper(strstr(Resolver::DEFAULT_LOCALE, '_', true));
        }
    }
}
