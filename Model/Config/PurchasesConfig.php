<?php

namespace Benhauer\Salesmanago\Model\Config;

use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Benhauer\Salesmanago\Helper\Conf;

class PurchasesConfig extends MagentoAbstractModel
{
    /**
     * P_T - Processing Type;
     * C_L - Config Label;
     */
    public const
        P_T_DEFAULT = 0,
        P_T_ON_CHOOSEN_EVENT = 1,
        P_T_ON_EVENT = 2,
        P_T_CRON = 3,

        C_PLATFORM_SETTING_PATH = 'salesmanago_platform_settings/',
        C_ACCOUNT_SETTINGS_PATH = 'salesmanago_account_settings/';

    /**
     * @var string
     */
    public static $defaultPurchaseEvent = 'checkout_onepage_controller_success_action';

    /**
     * @var string
     */
    public static $lastEventToSendPurchase = 'checkout_submit_all_after';

    /**
     * @var array processing types labels;
     */
    public static $processingTypes = [
        ['value' => self::P_T_DEFAULT, 'label' => 'Default'],
        ['value' => self::P_T_ON_CHOOSEN_EVENT, 'label' => 'System event'],
        ['value' => self::P_T_ON_EVENT, 'label' => 'After all purchase system events']
    ];

    /**
     * @var array list of events for purchase;
     */
    public static $processingSystemEvents = [
        0 => 'checkout_onepage_controller_success_action',
        1 => 'multishipping_checkout_controller_success_action',
        2 => 'checkout_submit_all_after',
        3 => 'checkout_controller_onepage_saveOrder',
        4 => 'restore_quote',
        5 => 'sales_convert_order_to_quote',
        6 => 'sales_order_invoice_register',
        7 => '_set_sales_order',
        8 => 'checkout_type_onepage_save_order_after',
        9 => 'checkout_type_multishipping_create_orders_single',
        10 => 'paypal_express_place_order_success'
    ];

    /**
     * @var Conf
     */
    protected $config;

    /**
     * @var int
     */
    protected $type = self::P_T_DEFAULT;

    /**
     * @var int Default CRON cycle in hrs;
     */
    protected $defaultCroneCycle = 3;

    /**
     * Get config
     *
     * @return false|int|string
     */
    public function getConfig()
    {
        $type = Conf::getInstance()->getPurchaseProcessingType();

        switch ($type) {
            case self::P_T_ON_CHOOSEN_EVENT:
                $this->type = self::P_T_ON_CHOOSEN_EVENT;
                $this->config = Conf::getInstance()->getPurchaseProcessingTypeSystemEvents();
                return $this->config;
            case self::P_T_ON_EVENT:
                $this->type = self::P_T_ON_EVENT;
                $this->config = self::$lastEventToSendPurchase;
                return $this->config;
            case self::P_T_DEFAULT:
                $this->type = self::P_T_DEFAULT;
                $this->config = self::$defaultPurchaseEvent;
                return $this->config;
            default:
                return false;
        }
    }

    /**
     * Get type
     *
     * @return int configuration type
     */
    public function getType()
    {
        $this->getConfig();
        return $this->type;
    }

    /**
     * Check if hook is active
     *
     * @param string $hookName
     * @return boolean
     */
    public function isHookActive($hookName)
    {
        $this->getConfig();

        if ($this->type == self::P_T_CRON) {
            return false;
        }

        if (isset(self::$processingSystemEvents[(int) $this->config])
            && (self::$processingSystemEvents[(int) $this->config] == $hookName)
        ) {
            return true;
        }

        return false;
    }
}
