<?php

namespace Benhauer\Salesmanago\Model\Config\Source;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\App\Request\Http;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Config as ConfigModel;

use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;

class SelectOwnersList
{
    /**
     * @var FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * @var ConfigModel
     */
    public $confModel;

    /**
     * @var Http
     */
    public $request;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var RequestInterface
     */
    public $requestInterface;

    /**
     * SelectOwnersList constructor.
     *
     * @param FactoryOrganizer $factoryOrganizer
     * @param ConfigModel $confModel
     * @param Http $request
     * @param LoggerInterface $logger
     * @param RequestInterface $requestInterface
     */
    public function __construct(
        FactoryOrganizer $factoryOrganizer,
        ConfigModel $confModel,
        Http $request,
        LoggerInterface $logger,
        RequestInterface $requestInterface
    ) {
        $this->factoryOrganizer = $factoryOrganizer;
        $this->confModel        = $confModel;
        $this->request          = $request;
        $this->logger           = $logger;
        $this->requestInterface = $requestInterface;
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store
     */
    protected function resolveStoreScope()
    {
        try {
            //set store id by top select option chosen
            $scopeId = (int) $this->requestInterface->getParam('store');
            $scope   = $scopeId === 0
                ? 'default'
                : ScopeInterface::SCOPE_STORES;

            $this->confModel
                ->setScopeId($scopeId)
                ->setScope($scope)
                ->getCachedConf();
        } catch (Exception $exception) {
            $this->logger->critical($exception->getLogMessage());
        }
    }

    /**
     * Return default option array for configuration select
     *
     * @return array
     */
    public function toOptionArray()
    {
        $this->resolveStoreScope();
        return $this->getOwnersEmailsList();
    }

    /**
     * Return default option array for configuration select
     *
     * @return array - owners emails list for config select;
     */
    public function getOwnersEmailsList()
    {
        try {
            $answer = [];

            if (empty(Conf::getInstance()->getClientId())) {
                return $answer;
            }

            $owners = Conf::getInstance()->getOwnersList();

            if (!empty($owners)) {
                foreach ($owners as $owner) {
                    $answer[] = [
                        'value' => $owner,
                        'label' => $owner
                    ];
                }
                return $answer;
            } elseif (!empty(Conf::getInstance()->getOwner())) {
                $answer[] = [
                    'value' => Conf::getInstance()->getOwner(),
                    'label' => Conf::getInstance()->getOwner()
                ];
            }

            return $answer;
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getTraceAsString());
        }
    }
}
