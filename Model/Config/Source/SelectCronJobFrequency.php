<?php

namespace Benhauer\Salesmanago\Model\Config\Source;

use \Magento\Framework\Data\OptionSourceInterface;
use \SALESmanago\Factories\FactoryOrganizer;

class SelectCronJobFrequency implements OptionSourceInterface
{
    /**
     * @var FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * SelectCronJobFrequency constructor.
     *
     * @param FactoryOrganizer $factoryOrganizer
     */
    public function __construct(
        FactoryOrganizer $factoryOrganizer
    ) {
        $this->factoryOrganizer = $factoryOrganizer;
    }

    /**
     * Return default option array for configuration select
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => '1'],
            ['value' => 2, 'label' => '2'],
            ['value' => 3, 'label' => '3'],
            ['value' => 4, 'label' => '4'],
            ['value' => 6, 'label' => '6'],
            ['value' => 10, 'label' => '10'],
            ['value' => 15, 'label' => '15'],
            ['value' => 24, 'label' => '24'],
            ['value' => 32, 'label' => '32'],
            ['value' => 48, 'label' => '48']
        ];
    }
}
