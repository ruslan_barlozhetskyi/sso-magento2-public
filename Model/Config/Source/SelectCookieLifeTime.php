<?php

namespace Benhauer\Salesmanago\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class SelectCookieLifeTime implements OptionSourceInterface
{
    /**
     * Show default values for cookie in configuration
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 3, 'label' => '3'],
            ['value' => 12, 'label' => '12'],
            ['value' => 24, 'label' => '24'],
            ['value' => 48, 'label' => '48'],
            ['value' => 72, 'label' => '72'],
            ['value' => 100, 'label' => '100'],
            ['value' => 200, 'label' => '200'],
            ['value' => 300, 'label' => '300'],
            ['value' => 400, 'label' => '400'],
            ['value' => 500, 'label' => '500']
        ];
    }
}
