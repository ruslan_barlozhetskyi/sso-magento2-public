<?php

namespace Benhauer\Salesmanago\Model\Config\Source;

use \Magento\Framework\Data\OptionSourceInterface;
use \Benhauer\Salesmanago\Model\Config\PurchasesConfig;

use \SALESmanago\Factories\FactoryOrganizer;

class SelectSystemEventsForPurchase implements OptionSourceInterface
{
    /**
     * @var FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * SelectSystemEventsForPurchase constructor.
     *
     * @param FactoryOrganizer $factoryOrganizer
     */
    public function __construct(
        FactoryOrganizer $factoryOrganizer
    ) {
        $this->factoryOrganizer = $factoryOrganizer;
    }

    /**
     * Return default option array for configuration select
     *
     * @return array
     */
    public function toOptionArray()
    {
        return PurchasesConfig::$processingSystemEvents;
    }
}
