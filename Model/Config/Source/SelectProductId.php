<?php

namespace Benhauer\Salesmanago\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class SelectProductId implements OptionSourceInterface
{
    /**
     * Show default values for cookie in configuration
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'id', 'label' => 'id'],
            ['value' => 'sku', 'label' => 'SKU'],
        ];
    }
}
