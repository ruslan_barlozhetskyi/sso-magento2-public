<?php


namespace Benhauer\Salesmanago\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

use Benhauer\Salesmanago\Model\Config;

class SelectAfterCartRecovery implements OptionSourceInterface
{
    /**
     * Return default option array for configuration select
     *
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => Config::TO_PURCHASE_SUMMARY, 'label' => 'Purchase summary page'],
            ['value' => Config::TO_CART_PAGE, 'label' => 'Cart page']
        ];
    }
}
