<?php

namespace Benhauer\Salesmanago\Model;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\Api\Filter;

class DummyDataProvider extends AbstractDataProvider
{
    /**
     * DummyDataProvider constructor.
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     */
    public function __construct(
        $name = '',
        $primaryFieldName = '',
        $requestFieldName = ''
    ) {
        $this->name = $name;
        $this->primaryFieldName = $primaryFieldName;
        $this->requestFieldName = $requestFieldName;
    }

    /**
     * Add required method
     *
     * @return array
     */
    public function getData()
    {
        return [];
    }

    /**
     * Add required method
     *
     * @param Filter $filter
     * @return array
     */
    public function addFilter(Filter $filter)
    {
        return [];
    }
}
