<?php


namespace Benhauer\Salesmanago\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory as ConfigCollection;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ProductMetadata;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Exception\FileSystemException;

use SALESmanago\Exception\Exception;
use SALESmanago\Entity\Configuration;
use SALESmanago\Entity\cUrlClientConfiguration;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Config\PurchasesConfig;
use Benhauer\Salesmanago\Helper\CookieManager;

class Config extends MagentoAbstractModel
{
    public const
        PLATFORM_NAME           = 'MAGENTO2',
        ACTIVE                  = 'active',
        CONF_SCHEMA_VERSION     = 'confSchemaVer',
        CLIENT_ID               = 'clientId',
        SHA                     = 'sha',
        ENDPOINT                = 'endpoint',
        TOKEN                   = 'token',
        EMAIL                   = 'email',

        STORE_ID                = 'storeId',

        OWNER                   = 'owner',
        C_EVENT_COOKIE_TIME     = 'eventCookie',
        C_EVENT_PRODUCT_TYPE_ID = 'productTypeId',

        C_CALLBACK_OPTIN        = 'callbackOptIn',
        C_CALLBACK_OPTOUT       = 'callbackOptOut',

        C_CLIENT_ID             = 'clientId',
        C_SHA                   = 'sha',
        C_ENDPOINT              = 'endpoint',

        C_OWNER_EMAIL           = 'ownerEmail',
        C_OWNERS_LIST           = 'ownersList',
        C_SYNCHRONIZE           = 'synchronize',

        C_LOCATION              = 'location',
        PLUGIN_VERSION          = 'pluginVersion',

        C_API_KEY               = 'apiKey',
        C_TOKEN                 = 'token',

        C_PATH_DEVIDER          = '/',
        C_SETTINGS_PATH         = 'salesmanago/settings',
        //C_STORES_WITH_CONFIGURATION = 'storesWithConfiguration',
        C_CHECK_STORE_SCOPE_ID  = 'checkStoreScopeId',

        SECTION_PLATFORM_SETTING = 'salesmanago_platform_settings/',
        SECTION_ACCOUNT_SETTINGS = 'salesmanago_account_settings/',

        GROUP_GENERAL     = 'general/',
        GROUP_CONTACTS    = 'contacts/',
        GROUP_EVENTS      = 'events/',
        GROUP_CALLBACKS   = 'callbacks/',
        GROUP_API_DOUBLE_OPT_IN = 'apiDoubleOptIn/',
        GROUP_SM_ATTENTION      = 'smattention/',

        TAGS                    = 'tags',
        T_NEWS                  = 'subscription',
        T_REGISTER              = 'registration',
        T_PURCHASE              = 'purchase',
        T_GUEST_PURCHASE        = 'purchaseGuest',
        T_LOGIN                 = 'login',

        MONITORING              = 'monitoring',
        BANNERS                 = 'bannersEnabled',
        CUSTOM_JS               = 'customJsEnabled',
        POPUP_JS                = 'popupJsEnabled',
        MONIT_CODE_DISABLED     = 'monitoringCodeDisabled',
        EMAIL_DOMAINS_TO_IGNORE = 'emailDomainsToIgnore',

        DOUBLE_OPT_IN           = 'useApiDoubleOptIn',
        DOUBLE_OPT_IN_EMAIL_TEMPLATE_ID = 'apiDoubleOptInEmailTemplateId',
        DOUBLE_OPT_IN_EMAIL_ACCOUNT_ID  = 'apiDoubleOptInEmailAccountId',
        DOUBLE_OPT_IN_EMAIL_SUBJECT_ID  = 'apiDoubleOptInEmailSubject',

        CART_CONF_PATH          = 'salesmanago_platform_settings/cart/',
        CART_RECOVERY_REDIRECT  = 'cartRecoveryRedirectTo',
        TO_CART_PAGE            = 'cart',
        TO_PURCHASE_SUMMARY     = 'purchase_summary',

        COOKIES_CONF_PATH       = 'salesmanago_platform_settings/cookies/',
        COOKIES_TTL             = 'cookiesTimeToLife',

        CONN_CONF_PATH           = 'salesmanago_platform_settings/connection/',
        TIMEOUT                  = 'timeout',
        TIMEOUT_MS               = 'timeoutms',
        CONNECT_TIMEOUT_MS       = 'connecttimeoutms',
        RETRY_REQUEST_IF_TIMEOUT = 'retryrequestiftimeout',

        PURCHASE_CONF_PATH      = 'salesmanago_platform_settings/purchases/',
        C_L_TYPE                = 'purchaseProcessingType',
        C_L_PURCHASE_VALUE      = 'purchaseValueAsBaseGrandTotal',
        C_L_ON_CHOOSEN_EVENT    = 'purchaseProcessingTypeSystemEvents',
        C_L_ON_CRON             = 'purchaseProcessingTypeCron',
        FIELD_SM_ATTENTION      = 'smattention';

    /**
     * @var int
     */
    protected $scopeId = 0;

    /**
     * @var string
     */
    protected $scope = 'default';

    /**
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ConfigCollection
     */
    protected $collectionFactory;

    /**
     * @var array with Conf element for each scopeId
     */
    public static $tmpConf = [];

    /**
     * @var ProductMetadata
     */
    protected $magentoProductMeta;

    /**
     * @var File
     */
    protected $fileSystemManager;

    /**
     * @var string[] - match magento config path with config item name;
     */
    public static $confPaths = [
        Conf::ACTIVE => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::ACTIVE,
        Conf::SCHEMA_VER => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::CONF_SCHEMA_VERSION,
        Conf::ENDPOINT => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_ENDPOINT,
        Conf::CLIENT_ID => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_CLIENT_ID,
        Conf::API_KEY => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_API_KEY,
        Conf::SHA => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_SHA,
        Conf::OWNER => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_OWNER_EMAIL,
        Conf::OWNERS_LIST => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_OWNERS_LIST,
        Conf::TOKEN => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_TOKEN,
        Conf::EVENT_COOKIE_TTL => self::SECTION_PLATFORM_SETTING . self::GROUP_EVENTS . self::C_EVENT_COOKIE_TIME,
        Conf::PRODUCT_TYPE_ID => self::SECTION_PLATFORM_SETTING . self::GROUP_EVENTS . self::C_EVENT_PRODUCT_TYPE_ID,
        Conf::LOCATION => self::SECTION_PLATFORM_SETTING . self::GROUP_EVENTS . self::C_LOCATION,
        Conf::TAGS_NEWSLETTER => self::SECTION_PLATFORM_SETTING . self::TAGS . self::C_PATH_DEVIDER . self::T_NEWS,
        Conf::TAGS_REGISTER => self::SECTION_PLATFORM_SETTING . self::TAGS . self::C_PATH_DEVIDER . self::T_REGISTER,
        Conf::TAGS_LOGIN => self::SECTION_PLATFORM_SETTING . self::TAGS . self::C_PATH_DEVIDER . self::T_LOGIN,
        Conf::TAGS_GUEST_PURCHASE => self::SECTION_PLATFORM_SETTING
            . self::TAGS . self::C_PATH_DEVIDER . self::T_GUEST_PURCHASE,
        Conf::TAGS_PURCHASE => self::SECTION_PLATFORM_SETTING . self::TAGS . self::C_PATH_DEVIDER . self::T_PURCHASE,
        Conf::DISABLE_MONIT_CODE  => self::SECTION_PLATFORM_SETTING
            . self::MONITORING . self::C_PATH_DEVIDER . self::MONIT_CODE_DISABLED,
        Conf::BANNERS_ENABLED => self::SECTION_PLATFORM_SETTING
            . self::MONITORING . self::C_PATH_DEVIDER . self::BANNERS,
        Conf::CUSTOM_JS_ENABLED => self::SECTION_PLATFORM_SETTING
            . self::MONITORING . self::C_PATH_DEVIDER . self::CUSTOM_JS,
        Conf::POPUP_JS_ENABLED => self::SECTION_PLATFORM_SETTING
            . self::MONITORING . self::C_PATH_DEVIDER . self::POPUP_JS,
        Conf::ACTIVE_SYNC => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_SYNCHRONIZE,
        Conf::ADOI_ENABLED => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_API_DOUBLE_OPT_IN . self::DOUBLE_OPT_IN,
        Conf::ADOI_SUBJECT => self::SECTION_ACCOUNT_SETTINGS
            . self::GROUP_API_DOUBLE_OPT_IN . self::DOUBLE_OPT_IN_EMAIL_SUBJECT_ID,
        Conf::ADOI_TEMPLATE_ID => self::SECTION_ACCOUNT_SETTINGS
            . self::GROUP_API_DOUBLE_OPT_IN . self::DOUBLE_OPT_IN_EMAIL_TEMPLATE_ID,
        Conf::ADOI_ACCOUNT_ID => self::SECTION_ACCOUNT_SETTINGS
            . self::GROUP_API_DOUBLE_OPT_IN . self::DOUBLE_OPT_IN_EMAIL_ACCOUNT_ID,
        Conf::CHECK_SCOPE_ID => self::C_SETTINGS_PATH . self::C_PATH_DEVIDER . self::C_CHECK_STORE_SCOPE_ID,
        Conf::CART_RECOVERY_REDIRECT_TO => self::CART_CONF_PATH . self::CART_RECOVERY_REDIRECT,
        Conf::CONTACT_COOKIE_TTL => self::COOKIES_CONF_PATH . self::COOKIES_TTL,
        Conf::IGNORED_DOMAINS => self::SECTION_PLATFORM_SETTING . self::GROUP_CONTACTS . self::EMAIL_DOMAINS_TO_IGNORE,
        Conf::PURCHASE_VALUE_AS_GRAND_TOTAL => self::PURCHASE_CONF_PATH . self::C_L_PURCHASE_VALUE,
        Conf::VERSION_OF_INTEGRATION => self::SECTION_PLATFORM_SETTING . self::GROUP_GENERAL . self::PLUGIN_VERSION,
        Conf::CALLBACK_OPTIN => self::SECTION_PLATFORM_SETTING . self::GROUP_CALLBACKS . self::C_CALLBACK_OPTIN,
        Conf::CALLBACK_OPTOUT => self::SECTION_PLATFORM_SETTING . self::GROUP_CALLBACKS . self::C_CALLBACK_OPTOUT,
        Conf::PURCHASE_PROCESSING_TYPE => self::PURCHASE_CONF_PATH . self::C_L_TYPE,
        Conf::PURCHASE_PROCESSING_EVENT => self::PURCHASE_CONF_PATH . self::C_L_ON_CHOOSEN_EVENT,
        Conf::PURCHASE_PROCESSING_TYPE_CRON => self::PURCHASE_CONF_PATH . self::C_L_ON_CRON,
        //next one are a helper fields:
        Conf::ATTENTION . '_1' => self::SECTION_ACCOUNT_SETTINGS . self::GROUP_SM_ATTENTION . self::FIELD_SM_ATTENTION,
        Conf::ATTENTION . '_2' => self::SECTION_PLATFORM_SETTING . self::GROUP_SM_ATTENTION . self::FIELD_SM_ATTENTION,

        cUrlClientConfiguration::TIMEOUT => self::CONN_CONF_PATH . self::TIMEOUT,
        cUrlClientConfiguration::TIMEOUT_MS => self::CONN_CONF_PATH . self::TIMEOUT_MS,
        cUrlClientConfiguration::CONNECT_TIMEOUT_MS => self::CONN_CONF_PATH . self::CONNECT_TIMEOUT_MS,

        conf::RETRY_REQUEST_IF_TIMEOUT => self::CONN_CONF_PATH . self::RETRY_REQUEST_IF_TIMEOUT
    ];

    /**
     * Config constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param WriterInterface $configWriter
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigCollection $collectionFactory
     * @param ProductMetadata $magentoProductMeta
     * @param File $fileSystemManager
     */
    public function __construct(
        Context               $context,
        Registry              $registry,
        StoreManagerInterface $storeManager,
        WriterInterface       $configWriter,
        ScopeConfigInterface  $scopeConfig,
        ConfigCollection      $collectionFactory,
        ProductMetadata       $magentoProductMeta,
        File                  $fileSystemManager
    ) {
        parent::__construct($context, $registry);

        $this->storeManager = $storeManager;
        $this->configWriter = $configWriter;
        $this->scopeConfig = $scopeConfig;
        $this->collectionFactory = $collectionFactory;
        $this->magentoProductMeta = $magentoProductMeta;
        $this->fileSystemManager = $fileSystemManager;
    }

    /**
     * Set scope id
     *
     * @param int $param
     * @return Config
     */
    public function setScopeId($param): Config
    {
        $this->scopeId = (int)$param;
        return $this;
    }

    /**
     * Set scope
     *
     * @param string $param
     * @return $this
     */
    public function setScope($param): Config
    {
        $this->scope = $param;
        return $this;
    }

    /**
     * Get configuration directly form database
     *
     * @return Conf
     * @throws Exception
     */
    public function getNoCachedConf()
    {
        $this->addPlatformDataToConfiguration();
        $RequestClientConf = Conf::getInstance()->getRequestClientConf();

        return Conf::getInstance()
            ->setActive(
                $this->getNoCachedItem(self::$confPaths[Conf::ACTIVE])
            )->setConfSchemaVer(
                $this->getNoCachedItem(self::$confPaths[Conf::SCHEMA_VER])
                ?? Conf::getInstance()->getConfSchemaVer()
            )->setEndpoint(
                $this->getNoCachedItem(self::$confPaths[Conf::ENDPOINT])
                ?? Conf::getInstance()->getEndpoint()
            )->setClientId(
                $this->getNoCachedItem(self::$confPaths[Conf::CLIENT_ID])
            )->setApiKey(
                $this->getNoCachedItem(self::$confPaths[Conf::API_KEY])
            )->setSha(
                $this->getNoCachedItem(self::$confPaths[Conf::SHA])
            )->setOwner(
                $this->getNoCachedItem(self::$confPaths[Conf::OWNER])
            )->setOwnersList(
                json_decode(
                    $this->getNoCachedItem(
                        $this->getCachedItem(
                            self::$confPaths[Conf::OWNERS_LIST]
                        )
                    )
                )
            )->setToken(
                $this->getNoCachedItem(self::$confPaths[Conf::TOKEN])
            )->setEventCookieTtl(
                $this->getNoCachedItem(self::$confPaths[Conf::EVENT_COOKIE_TTL])
                ?? Conf::getInstance()->getContactCookieTtl()
            )->setProductTypeId(
                $this->getNoCachedItem(self::$confPaths[Conf::PRODUCT_TYPE_ID])
                ?? Conf::getInstance()->getProductTypeId()
            )->setLocation(
                $this->getNoCachedItem(self::$confPaths[Conf::LOCATION])
            )->setNewsletterTags(
                $this->getNoCachedItem(self::$confPaths[Conf::TAGS_NEWSLETTER])
                ?? ''
            )->setRegistrationTags(
                $this->getNoCachedItem(self::$confPaths[Conf::TAGS_REGISTER])
                ?? ''
            )->setLoginTags(
                $this->getNoCachedItem(self::$confPaths[Conf::TAGS_LOGIN])
                ?? ''
            )->setGuestPurchaseTags(
                $this->getNoCachedItem(self::$confPaths[Conf::TAGS_GUEST_PURCHASE])
                ?? ''
            )->setPurchaseTags(
                $this->getNoCachedItem(self::$confPaths[Conf::TAGS_PURCHASE])
                ?? ''
            )->setDisableMonitoringCode(
                $this->getNoCachedItem(self::$confPaths[Conf::DISABLE_MONIT_CODE])
                ?? Conf::getInstance()->getDisableMonitoringCode()
            )->setBannersEnabled(
                $this->getNoCachedItem(self::$confPaths[Conf::BANNERS_ENABLED])
                ?? Conf::getInstance()->getBannersEnabled()
            )->setCustomJsEnabled(
                $this->getNoCachedItem(self::$confPaths[Conf::CUSTOM_JS_ENABLED])
                ?? Conf::getInstance()->getCustomJsEnabled()
            )->setPopupJsEnabled(
                $this->getNoCachedItem(self::$confPaths[Conf::POPUP_JS_ENABLED])
                ?? Conf::getInstance()->getPopupJsEnabled()
            )->setStoreDomain(
                $this->getStoreDomain()
            )->setActiveSynchronization(
                $this->getNoCachedItem(self::$confPaths[Conf::ACTIVE_SYNC])
                ?? Conf::getInstance()->getActiveSynchronization()
            )->setApiDoubleOptIn(
                Conf::getInstance()
                    ->getApiDoubleOptIn()//return/create ApiDoubleOptIn object
                    ->setEnabled(boolval($this->getNoCachedItem(self::$confPaths[Conf::ADOI_ENABLED])))
                    ->setSubject(
                        $this->getNoCachedItem(self::$confPaths[Conf::ADOI_SUBJECT])
                        ?? Conf::getInstance()->getApiDoubleOptIn()->getSubject()
                    )->setTemplateId(
                        $this->getNoCachedItem(self::$confPaths[Conf::ADOI_TEMPLATE_ID])
                        ?? Conf::getInstance()->getApiDoubleOptIn()->getTemplateId()
                    )->setAccountId(
                        $this->getNoCachedItem(self::$confPaths[Conf::ADOI_ACCOUNT_ID])
                        ?? Conf::getInstance()->getApiDoubleOptIn()->getAccountId()
                    )
            )->setCheckScopeId(
                $this->getNoCachedItem(self::$confPaths[Conf::CHECK_SCOPE_ID])
            )->setCartRecoveryRedirectTo(
                $this->getNoCachedItem(self::$confPaths[Conf::CART_RECOVERY_REDIRECT_TO])
                ?? Conf::getInstance()->getCartRecoveryRedirectTo()
            )->setContactCookieTtl(
                $this->getNoCachedItem(self::$confPaths[Conf::CONTACT_COOKIE_TTL])
                ?? Conf::getInstance()->getContactCookieTtl()
            )->setIgnoredDomains(
                $this->getConfIgnoreDomainsToArray("nocached")
            )->setPurchaseValueAsBaseGrandTotal(
                $this->getNoCachedItem(
                    self::$confPaths[Conf::PURCHASE_VALUE_AS_GRAND_TOTAL]
                ) ?? Conf::getInstance()->getPurchaseValueAsBaseGrandTotal()
            )->setVersionOfIntegration(
                $this->getNoCachedItem(self::$confPaths[Conf::VERSION_OF_INTEGRATION])
                ?? Conf::getInstance()->getVersionOfIntegration()
            )->setRequestClientConf(
                Conf::getInstance()->getRequestClientConf(
                    [
                        cUrlClientConfiguration::TIMEOUT =>
                            $this->getNoCachedItem(self::$confPaths[cUrlClientConfiguration::TIMEOUT])
                            ?? $RequestClientConf->getTimeOut(),
                        cUrlClientConfiguration::TIMEOUT_MS =>
                            $this->getNoCachedItem(self::$confPaths[cUrlClientConfiguration::TIMEOUT_MS])
                            ?? $RequestClientConf->getTimeOutMs(),
                        cUrlClientConfiguration::CONNECT_TIMEOUT_MS =>
                            $this->getNoCachedItem(self::$confPaths[cUrlClientConfiguration::CONNECT_TIMEOUT_MS])
                            ?? $RequestClientConf->getConnectTimeOutMs(),
                        cUrlClientConfiguration::HOST =>
                            $this->getNoCachedItem(self::$confPaths[Conf::ENDPOINT])
                            ?? $RequestClientConf->getUrl()
                    ]
                )
            )->setRetryRequestIfTimeout(
                $this->getNoCachedItem(self::$confPaths[Conf::RETRY_REQUEST_IF_TIMEOUT])
            );
    }

    /**
     * Sets configuration values for component.
     *
     * Returns only helper Conf, but set Singleton Entity\Configuration variables;
     *
     * @return Conf - platform configuration
     * @throws Exception
     */
    public function getCachedConf()
    {
        $this->addPlatformDataToConfiguration();
        $RequestClientConf = Conf::getInstance()->getRequestClientConf();

        return Conf::getInstance()
            ->setActive(
                $this->getCachedItem(self::$confPaths[Conf::ACTIVE])
            )->setConfSchemaVer(
                $this->getCachedItem(self::$confPaths[Conf::SCHEMA_VER])
                ?? Conf::getInstance()->getConfSchemaVer()
            )->setEndpoint(
                $this->getCachedItem(self::$confPaths[Conf::ENDPOINT])
                ?? Conf::getInstance()->getEndpoint()
            )->setClientId(
                $this->getCachedItem(
                    self::$confPaths[Conf::CLIENT_ID]
                )
            )->setApiKey(
                $this->getCachedItem(
                    self::$confPaths[Conf::API_KEY]
                )
            )->setSha(
                $this->getCachedItem(self::$confPaths[Conf::SHA])
            )->setOwner(
                $this->getCachedItem(self::$confPaths[Conf::OWNER])
            )->setOwnersList(
                ($this->getCachedItem(
                    self::$confPaths[Conf::OWNERS_LIST]
                )) ? json_decode($this->getCachedItem(self::$confPaths[Conf::OWNERS_LIST])) : []
            )->setToken(
                $this->getCachedItem(self::$confPaths[Conf::TOKEN])
            )->setEventCookieTtl(
                $this->getCachedItem(
                    self::$confPaths[Conf::EVENT_COOKIE_TTL]
                ) ?? Conf::getInstance()->getEventCookieTtl()
            )->setProductTypeId(
                $this->getCachedItem(
                    self::$confPaths[Conf::PRODUCT_TYPE_ID]
                ) ?? Conf::getInstance()->getProductTypeId()
            )->setLocation(
                $this->getCachedItem(
                    self::$confPaths[Conf::LOCATION]
                )
            )->setNewsletterTags(
                $this->getCachedItem(
                    self::$confPaths[Conf::TAGS_NEWSLETTER]
                ) ?? ''
            )->setRegistrationTags(
                $this->getCachedItem(
                    self::$confPaths[Conf::TAGS_REGISTER]
                ) ?? ''
            )->setLoginTags(
                $this->getCachedItem(
                    self::$confPaths[Conf::TAGS_LOGIN]
                ) ?? ''
            )->setGuestPurchaseTags(
                $this->getCachedItem(
                    self::$confPaths[Conf::TAGS_GUEST_PURCHASE]
                ) ?? ''
            )->setPurchaseTags(
                $this->getCachedItem(
                    self::$confPaths[Conf::TAGS_PURCHASE]
                ) ?? ''
            )->setDisableMonitoringCode(
                $this->getCachedItem(
                    self::$confPaths[Conf::DISABLE_MONIT_CODE]
                ) ?? Conf::getInstance()->getDisableMonitoringCode()
            )->setBannersEnabled(
                $this->getCachedItem(
                    self::$confPaths[Conf::BANNERS_ENABLED]
                ) ?? Conf::getInstance()->getBannersEnabled()
            )->setCustomJsEnabled(
                $this->getCachedItem(
                    self::$confPaths[Conf::CUSTOM_JS_ENABLED]
                ) ?? Conf::getInstance()->getCustomJsEnabled()
            )->setPopupJsEnabled(
                $this->getCachedItem(
                    self::$confPaths[Conf::POPUP_JS_ENABLED]
                ) ?? Conf::getInstance()->getPopupJsEnabled()
            )->setStoreDomain(
                $this->getStoreDomain()
            )->setActiveSynchronization(
                $this->getCachedItem(
                    self::$confPaths[Conf::ACTIVE_SYNC]
                ) ?? Conf::getInstance()->getActiveSynchronization()
            )->setCheckScopeId(
                $this->getCachedItem(
                    self::$confPaths[Conf::CHECK_SCOPE_ID]
                )
            )->setApiDoubleOptIn(
                Conf::getInstance()
                    ->getApiDoubleOptIn()//return/create ApiDoubleOptIn object
                    ->setEnabled(
                        boolval(
                            $this->getCachedItem(
                                self::$confPaths[Conf::ADOI_ENABLED]
                            )
                        )
                    )->setSubject(
                        $this->getCachedItem(
                            self::$confPaths[Conf::ADOI_SUBJECT]
                        ) ?? Conf::getInstance()->getApiDoubleOptIn()->getSubject()
                    )->setTemplateId(
                        $this->getCachedItem(
                            self::$confPaths[Conf::ADOI_TEMPLATE_ID]
                        ) ?? Conf::getInstance()->getApiDoubleOptIn()->getTemplateId()
                    )->setAccountId(
                        $this->getCachedItem(
                            self::$confPaths[Conf::ADOI_ACCOUNT_ID]
                        ) ?? Conf::getInstance()->getApiDoubleOptIn()->getAccountId()
                    )
            )->setPurchaseProcessingType(
                $this->getCachedItem(
                    self::$confPaths[Conf::PURCHASE_PROCESSING_TYPE]
                ) ?? PurchasesConfig::P_T_DEFAULT
            )->setPurchaseProcessingTypeCron(
                $this->getCachedItem(
                    self::$confPaths[Conf::PURCHASE_PROCESSING_TYPE_CRON]
                )
            )->setPurchaseProcessingTypeSystemEvents(
                $this->getCachedItem(
                    self::$confPaths[Conf::PURCHASE_PROCESSING_EVENT]
                )
            )->setCartRecoveryRedirectTo(
                $this->getCachedItem(
                    self::$confPaths[Conf::CART_RECOVERY_REDIRECT_TO]
                ) ?? Conf::getInstance()->getCartRecoveryRedirectTo()
            )->setContactCookieTtl(
                $this->getCachedItem(
                    self::$confPaths[Conf::CONTACT_COOKIE_TTL]
                ) ?? Conf::getInstance()->getContactCookieTtl()
            )->setIgnoredDomains(
                $this->getConfIgnoreDomainsToArray("cached")
            )->setVersionOfIntegration(
                $this->getCachedItem(
                    self::$confPaths[Conf::VERSION_OF_INTEGRATION]
                ) ?? Conf::getInstance()->getVersionOfIntegration()
            )->setPurchaseValueAsBaseGrandTotal(
                $this->getCachedItem(
                    self::$confPaths[Conf::PURCHASE_VALUE_AS_GRAND_TOTAL]
                ) ?? Conf::getInstance()->getPurchaseValueAsBaseGrandTotal()
            )->setRequestClientConf(
                Conf::getInstance()
                    ->getRequestClientConf(
                        [
                            cUrlClientConfiguration::TIMEOUT =>
                                $this->getCachedItem(self::$confPaths[cUrlClientConfiguration::TIMEOUT])
                                ?? $RequestClientConf->getTimeOut(),
                            cUrlClientConfiguration::TIMEOUT_MS =>
                                $this->getCachedItem(self::$confPaths[cUrlClientConfiguration::TIMEOUT_MS])
                                ?? $RequestClientConf->getTimeOutMs(),
                            cUrlClientConfiguration::CONNECT_TIMEOUT_MS =>
                                $this->getCachedItem(self::$confPaths[cUrlClientConfiguration::CONNECT_TIMEOUT_MS])
                                ?? $RequestClientConf->getConnectTimeOutMs(),
                            cUrlClientConfiguration::HOST =>
                                $this->getCachedItem(self::$confPaths[Conf::ENDPOINT])
                                ?? $RequestClientConf->getUrl()
                        ]
                    )
            )->setRetryRequestIfTimeout(
                $this->getCachedItem(self::$confPaths[Conf::RETRY_REQUEST_IF_TIMEOUT])
            );
    }

    /**
     * Save configuration after login in from Entity\Configuration
     *
     * And set default platform settings;
     *
     * @return Configuration
     * @throws Exception
     * @uses Configuration $Conf
     */
    public function saveConfAfterLogin()
    {
        //set configuration to active for particular scope
        //set SALESmanago 'location'/store id
        Conf::getInstance()
            ->setActive(true)
            ->setLocation($this->generateLocationId($this->getStoreDomain()))
            ->setPlatformDomain($this->getStoreDomain());

        $this->updatePluginVersion();

        $this->addPlatformDataToConfiguration();

        //to set value in hours:
        $cookieEventTTL = Conf::getInstance()->getEventCookieTtl() / 3600;

        //to set value in days:
        $cookieContactTTL = 10;

        //fixing return null from service config:
        $ownersList = empty(Conf::getInstance()->getOwnersList())
            ? []
            : Conf::getInstance()->getOwnersList();

        $this->saveItem(
            self::$confPaths[Conf::ACTIVE],
            Conf::getInstance()->isActive()
        )->saveItem(
            self::$confPaths[Conf::SCHEMA_VER],
            Conf::getInstance()->getConfSchemaVer()
        )->saveItem(
            self::$confPaths[Conf::ENDPOINT],
            Conf::getInstance()->getEndpoint()
        )->saveItem(
            self::$confPaths[Conf::CLIENT_ID],
            Conf::getInstance()->getClientId()
        )->saveItem(
            self::$confPaths[Conf::API_KEY],
            Conf::getInstance()->getApiKey()
        )->saveItem(
            self::$confPaths[Conf::SHA],
            Conf::getInstance()->getSha()
        )->saveItem(
            self::$confPaths[Conf::OWNER],
            Conf::getInstance()->getOwner()
        )->saveItem(
            self::$confPaths[Conf::OWNERS_LIST],
            json_encode($ownersList)
        )->saveItem(
            self::$confPaths[Conf::TOKEN],
            Conf::getInstance()->getToken()
        )->saveItem(//set platform default configuration:
            self::$confPaths[Conf::EVENT_COOKIE_TTL],
            $cookieEventTTL
        )->saveItem(//set platform default configuration:
            self::$confPaths[Conf::CONTACT_COOKIE_TTL],
            $cookieContactTTL
        )->saveItem(//set platform default configuration:
            self::$confPaths[Conf::PRODUCT_TYPE_ID],
            Conf::getInstance()->getProductTypeId()
        )->saveItem(
            self::$confPaths[Conf::TAGS_NEWSLETTER],
            'magento2_newsletter'
        )->saveItem(
            self::$confPaths[Conf::TAGS_GUEST_PURCHASE],
            'magento2_guest_purchase'
        )->saveItem(
            self::$confPaths[Conf::TAGS_PURCHASE],
            'magento2_purchase'
        )->saveItem(
            self::$confPaths[Conf::TAGS_REGISTER],
            'magento2_register'
        )->saveItem(
            self::$confPaths[Conf::TAGS_LOGIN],
            'magento2_login'
        )->saveItem(
            self::$confPaths[Conf::DISABLE_MONIT_CODE],
            Conf::getInstance()->getDisableMonitoringCode() ?: 0
        )->saveItem(
            self::$confPaths[Conf::BANNERS_ENABLED],
            Conf::getInstance()->getBannersEnabled() ?: 0
        )->saveItem(
            self::$confPaths[Conf::CUSTOM_JS_ENABLED],
            Conf::getInstance()->getCustomJsEnabled() ?: 0
        )->saveItem(
            self::$confPaths[Conf::POPUP_JS_ENABLED],
            Conf::getInstance()->getPopupJsEnabled() ?: 0
        )->saveItem(
            self::$confPaths[Conf::ACTIVE_SYNC],
            Conf::getInstance()->getActiveSynchronization() ?: 0
        )->saveItem(
            self::$confPaths[Conf::ADOI_ENABLED],
            Conf::getInstance()->getApiDoubleOptIn()->getEnabled() ?: 0
        )->saveItem(
            self::$confPaths[Conf::ADOI_TEMPLATE_ID],
            Conf::getInstance()->getApiDoubleOptIn()->getTemplateId() ?? ''
        )->saveItem(
            self::$confPaths[Conf::ADOI_ACCOUNT_ID],
            Conf::getInstance()->getApiDoubleOptIn()->getAccountId() ?? ''
        )->saveItem(
            self::$confPaths[Conf::ADOI_SUBJECT],
            Conf::getInstance()->getApiDoubleOptIn()->getSubject() ?? ''
        )->setItem(
            self::$confPaths[Conf::IGNORED_DOMAINS],
            $this->getConfIgnoreDomainsToString(Conf::getInstance()->getIgnoredDomains())
        )->setItem(
            self::$confPaths[cUrlClientConfiguration::TIMEOUT],
            Conf::getInstance()->getRequestClientConf()->getTimeOut()
        )->setItem(
            self::$confPaths[cUrlClientConfiguration::TIMEOUT_MS],
            Conf::getInstance()->getRequestClientConf()->getTimeOutMs()
        )->setItem(
            self::$confPaths[cUrlClientConfiguration::CONNECT_TIMEOUT_MS],
            Conf::getInstance()->getRequestClientConf()->getConnectTimeOutMs()
        );

        //next set auto generated values:
        $callbackUrls = $this->generateCallbacks(Conf::getInstance()->getClientId());

        $this->saveItem(
            self::$confPaths[Conf::CALLBACK_OPTIN],
            $callbackUrls[self::C_CALLBACK_OPTIN]
        )->saveItem(
            self::$confPaths[Conf::CALLBACK_OPTOUT],
            $callbackUrls[self::C_CALLBACK_OPTOUT]
        )->saveItem(
            self::$confPaths[Conf::LOCATION],
            Conf::getInstance()->getLocation()
        )->saveItem(
            self::$confPaths[Conf::CART_RECOVERY_REDIRECT_TO],
            Conf::getInstance()->getCartRecoveryRedirectTo()
        );

        /* Set scope id - this need for check which stores configuration set (for which store view),
         * magento load next one configuration if current (by store id) is not exist:
         */
        $this->saveItem(
            self::$confPaths[Conf::CHECK_SCOPE_ID],
            $this->scopeId
        );

        return Conf::getInstance();
    }

    /**
     * Removing configuration items
     */
    public function removeConf()
    {
        $pathToRemove = [
            self::$confPaths[Conf::ACTIVE],
            self::$confPaths[Conf::SCHEMA_VER],
            self::$confPaths[Conf::ENDPOINT],
            self::$confPaths[Conf::CLIENT_ID],
            self::$confPaths[Conf::API_KEY],
            self::$confPaths[Conf::SHA],
            self::$confPaths[Conf::OWNER],
            self::$confPaths[Conf::TOKEN],
            self::$confPaths[Conf::EVENT_COOKIE_TTL],
            self::$confPaths[Conf::PRODUCT_TYPE_ID],
            self::$confPaths[Conf::TAGS_NEWSLETTER],
            self::$confPaths[Conf::TAGS_GUEST_PURCHASE],
            self::$confPaths[Conf::TAGS_PURCHASE],
            self::$confPaths[Conf::TAGS_REGISTER],
            self::$confPaths[Conf::TAGS_LOGIN],
            self::$confPaths[Conf::BANNERS_ENABLED],
            self::$confPaths[Conf::CUSTOM_JS_ENABLED],
            self::$confPaths[Conf::DISABLE_MONIT_CODE],
            self::$confPaths[Conf::ACTIVE_SYNC],
            self::$confPaths[Conf::ADOI_ENABLED],
            self::$confPaths[Conf::ADOI_TEMPLATE_ID],
            self::$confPaths[Conf::ADOI_ACCOUNT_ID],
            self::$confPaths[Conf::ADOI_SUBJECT],
            self::$confPaths[Conf::CALLBACK_OPTIN],
            self::$confPaths[Conf::CALLBACK_OPTOUT],
            self::$confPaths[Conf::LOCATION],
            self::$confPaths[Conf::CART_RECOVERY_REDIRECT_TO],
            self::$confPaths[Conf::CHECK_SCOPE_ID],
            self::$confPaths[Conf::CONTACT_COOKIE_TTL],
            self::$confPaths[Conf::EVENT_COOKIE_TTL],
            self::$confPaths[Conf::ATTENTION . '_1'],
            self::$confPaths[Conf::ATTENTION . '_2'],
            self::$confPaths[Conf::IGNORED_DOMAINS],
            self::$confPaths[Conf::VERSION_OF_INTEGRATION],
            self::$confPaths[cUrlClientConfiguration::TIMEOUT],
            self::$confPaths[cUrlClientConfiguration::TIMEOUT_MS],
            self::$confPaths[cUrlClientConfiguration::CONNECT_TIMEOUT_MS],
            self::$confPaths[Conf::RETRY_REQUEST_IF_TIMEOUT],
        ];

        foreach ($pathToRemove as $path) {
            $this->configWriter->delete($path, $this->scope, $this->scopeId);
        }

        return true;
    }

    /**
     * Check if current scope (by scope id) is set in configuration,
     * In this way script could check if configuration exist for specific store id;
     * Additional check is by clientId
     *
     * @return bool
     */
    public function checkIfLoadAppropriateConfByScopeId()
    {
        $confCheckScopeId = (Conf::getInstance()->getCheckScopeId() === null)
            ?: (int)Conf::getInstance()->getCheckScopeId();

        if ($confCheckScopeId !== $this->scopeId) {
            return false;
        }

        $clientId = Conf::getInstance()->getClientId();

        return !empty($clientId);
    }

    /**
     * Saves owner list form Benhauer\Salesmanago\Helper\Conf instance to database
     */
    public function saveOwnersList()
    {
        //fixing return null from service config:
        $ownersList = empty(Conf::getInstance()->getOwnersList())
            ? []
            : Conf::getInstance()->getOwnersList();

        return $this->saveItem(
            self::SECTION_ACCOUNT_SETTINGS . self::GROUP_GENERAL . self::C_OWNERS_LIST,
            json_encode($ownersList)
        );
    }

    /**
     * Updating plugin version in db and in Conf entity
     */
    public function updatePluginVersion()
    {
        try {
            $composerJson = $this->fileSystemManager
                ->fileGetContents(__DIR__ . '/../composer.json');
        } catch (FileSystemException $exception) {
            $composerJson = false;
        }

        $pluginVersion = $composerJson
            ? json_decode($composerJson, true)['version']
            : null;

        $path = self::SECTION_PLATFORM_SETTING . self::GROUP_GENERAL . self::PLUGIN_VERSION;
        $this->saveItem($path, $pluginVersion);
        Conf::getInstance()->setVersionOfIntegration($pluginVersion);
    }

    /**
     * Saves particular configuration item to core_config_data table
     *
     * @param string $path - settings path;
     * @param mixed $value - settings value;
     * @return $this;
     */
    protected function saveItem($path, $value)
    {
        $this->configWriter->save(
            $path,
            $value,
            $this->scope,
            $this->scopeId
        );

        return $this;
    }

    /**
     * Get cached config item
     *
     * @param string $path
     * @return mixed
     */
    protected function getCachedItem($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            $this->scope,
            $this->scopeId
        );
    }

    /**
     * Get configuration item from data base apart from cache;
     *
     * @param string $path
     * @return mixed|null
     */
    protected function getNoCachedItem($path)
    {
        $settingItem = $this->collectionFactory
            ->create()
            ->addFieldToFilter('scope_id', ['eq' => $this->scopeId])
            ->addFieldToFilter('path', ['eq' => $path])
            ->getFirstItem()
            ->getData();

        return $settingItem['value'] ?? null;
    }

    /**
     * Generate callbacks links
     *
     * @param string|null $clientId
     * @return array;
     * @throws Exception
     * @todo Direct throw of generic Exception is discouraged. Use context specific instead.
     */
    protected function generateCallbacks($clientId = null)
    {
        try {
            //this is for url with enabled option "Add Store Code to Urls" and omitting adminhtml url for scopeId 0
            if ($this->scopeId != 0) {
                $storeDomain = $this->storeManager
                    ->getStore($this->scopeId)
                    ->getBaseUrl(UrlInterface::URL_TYPE_LINK);
            } else {
                $storeDomain = $this->storeManager
                    ->getStore()
                    ->getBaseUrl(UrlInterface::URL_TYPE_LINK);
            }

            return [
                self::C_CALLBACK_OPTIN =>
                    $storeDomain . "salesmanago/newsletter/in?email=\$email\$&key=sha1(\$email\${$clientId})",
                self::C_CALLBACK_OPTOUT =>
                    $storeDomain . "salesmanago/newsletter/out?email=\$email\$&key=sha1(\$email\${$clientId})"
            ];
        } catch (NoSuchEntityException $e) {
//@codingStandardsIgnoreStart
            throw new Exception($e->getMessage());
//@codingStandardsIgnoreEnd
        }
    }

    /**
     * Generate SALESmanago store location (store id) based on platform home url;
     *
     * @param string $storeDomain - base Home Url form platform
     * @param string $settingsPath - custom setting path
     * @return string|bool
     */
    protected function generateLocationId($storeDomain = null, $settingsPath = null)
    {
        if ($storeDomain == null) {
            return null;
        }

        return 'sm_' . hash('haval128,5', $storeDomain . $this->scopeId . $this->scope);
    }

    /**
     * Return store domain
     *
     * @return mixed
     * @throws Exception
     * @todo Direct throw of generic Exception is discouraged. Use context specific instead.
     */
    protected function getStoreDomain()
    {
        try {
            return $this->storeManager
                ->getStore($this->scopeId)
                ->getBaseUrl(UrlInterface::URL_TYPE_WEB);
        } catch (NoSuchEntityException  $e) {
//@codingStandardsIgnoreStart
            throw new Exception($e->getMessage());
//@codingStandardsIgnoreEnd
        }
    }

    /**
     * Parse configuration value to arr
     *
     * @param string $getType - cached or nocached configuration item
     * @return array
     */
    protected function getConfIgnoreDomainsToArray($getType = "cached")
    {
        switch ($getType) {
            case "nocached":
                $confEmailDomainsToIgnore = $this->getNoCachedItem(self::$confPaths[Conf::IGNORED_DOMAINS]);
                break;
            default:
                $confEmailDomainsToIgnore = $this->getCachedItem(self::$confPaths[Conf::IGNORED_DOMAINS]);
                break;
        }

        if (empty($confEmailDomainsToIgnore)) {
            return Conf::getInstance()->getIgnoredDomains();
        }

        if (strpos($confEmailDomainsToIgnore, ',') !== false) {
            return explode(',', $confEmailDomainsToIgnore);
        }

        return [$confEmailDomainsToIgnore];
    }

    /**
     * Parse arr configuration value to string
     *
     * @param array $arr
     * @return string
     */
    protected function getConfIgnoreDomainsToString($arr = [])
    {
        if (empty($arr)) {
            return '';
        }

        if (count($arr) > 1) {
            return implode(',', $arr);
        }

        return $arr[0];
    }

    /**
     * Get additional platform data for Configuration
     *
     * @throws Exception
     */
    protected function addPlatformDataToConfiguration()
    {
        Conf::getInstance()
            ->setPlatformDomain($this->getStoreDomain())
            ->setPlatformLang(
                $this->storeManager
                    ->getStore($this->scopeId)
                    ->getLocaleCode()
            )
            ->setPlatformName(self::PLATFORM_NAME)
            ->setPlatformVersion($this->magentoProductMeta->getVersion());
    }
}
