<?php


namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\EventsCollection;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;
use Benhauer\Salesmanago\Model\Exports\Export;

class Orders extends Export
{
    /**
     * @var OrderCollectionFactory
     */
    protected $orderFactory;

    /**
     * @var Event
     */
    protected $Event;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var EventsCollection
     */
    protected $eventsCollection;

    /**
     * Orders constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TimezoneInterface $timezone
     * @param OrderCollectionFactory $orderFactory
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     * @param ProductRepository $productRepository
     * @param EventsCollection $eventsCollection
     * @param MetaDataHelper $metaDataHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TimezoneInterface $timezone,
        OrderCollectionFactory $orderFactory,
        Event $Event,
        StoreManagerInterface $storeManager,
        ProductRepository $productRepository,
        EventsCollection $eventsCollection,
        MetaDataHelper $metaDataHelper
    ) {
        parent::__construct(
            $context,
            $registry,
            $timezone,
            $metaDataHelper
        );

        $this->orderFactory      = $orderFactory;
        $this->Event             = $Event;
        $this->storeManager      = $storeManager;
        $this->productRepository = $productRepository;
        $this->eventsCollection  = $eventsCollection;
    }

    /**
     * @var string[]
     */
    public static $exportOrderTypes = [
        'complete',
        'closed',
        'pending'
    ];

    /**
     * Setup basic of exports (date, scope/store id)
     *
     * @param mixed $dateFrom - date range od export from
     * @param mixed $dateTo - date range od export to
     * @param mixed $scopeId - scope id for store scope od exported data
     * @param mixed $ordersTypes - order types which will be exported
     *
     * @return $this
     */
    public function setUpExport(
        $dateFrom = null,
        $dateTo = null,
        $scopeId = 0,
        $ordersTypes = null
    ) {
        parent::setUpExport($dateFrom, $dateTo, $scopeId);
        $this->setOrdersTypesToExport($ordersTypes);
        return $this;
    }

    /**
     * Sets order types which must be exported
     *
     * @param mixed|string $param
     * @return $this;
     */
    protected function setOrdersTypesToExport($param = null)
    {
        if (empty($param)) {
            return $this;
        }

        self::$exportOrderTypes = explode(',', $param);
        return $this;
    }

    /**
     * Return order types which will be exported
     *
     * @return string[]
     */
    public function getOrdersTypesToExport()
    {
        return self::$exportOrderTypes;
    }

    /**
     * Get orders collection based on request params
     *
     * @param int $lastExportedPackage
     * @return OrderCollection
     */
    public function getOrdersCollection($lastExportedPackage = 0)
    {
        $lastExportedPackage = empty($lastExportedPackage)
            ? 0
            : (int) $lastExportedPackage;

        try {
            $orderFactory = $this->orderFactory
                ->create()
                ->addFieldToFilter(
                    'status',
                    [
                        'in' => self::$exportOrderTypes
                    ]
                )
                ->addFieldToSelect(['*'])
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->setCurPage($lastExportedPackage+1);

            $orderFactory->addFieldToFilter(
                'created_at',
                [
                    'from' => $this->dateRangeFrom,
                    'to' => $this->dateRangeTo,
                    'date' => true
                ]
            );

            if ($this->scopeId !== 0) {
                $orderFactory->addFieldToFilter(
                    'store_id',
                    [
                        'eq' => $this->scopeId
                    ]
                );
            }

            return $orderFactory;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Map data to sm events collection
     *
     * @param mixed $collection
     * @return EventsCollection
     * @throws NoSuchEntityException
     */
    public function prepareEventsToExport($collection)
    {
        foreach ($collection as $order) {
            $Event    = clone $this->Event;
            $products = $order->getAllVisibleItems();

            $store    = $this->storeManager->getStore();
            $storeUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_WEB);

            switch (Conf::getInstance()->getProductTypeId()) {
                case EventModel::$productIdDetail['sku']:
                    $mainProductId = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getSku();
                            },
                            $products
                        )
                    );
                    $detail3 = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getProductId();
                            },
                            $products
                        )
                    );
                    break;

                default:
                    $mainProductId = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getProductId();
                            },
                            $products
                        )
                    );
                    $detail3 = implode(
                        ",",
                        array_map(
                            function ($product) {
                                return $product->getSku();
                            },
                            $products
                        )
                    );
            }

            try {
                $categoriesIds = implode(
                    ",",
                    array_map(
                        function ($product) {
                            $categoryIds = $this->productRepository
                                ->get($product->getSku())
                                ->getCategoryIds();

                            return empty($categoryIds) ? 'ncat' : implode('/', $categoryIds);
                        },
                        $products
                    )
                );
            } catch (\Exception $e) {
                $categoriesIds = '';
            }

            try {
                $origData = $order->getOrigData()['shipping_description'];
                $shippingDescription = (empty($origData) || !isset($origData['shipping_description']))
                    ? ''
                    : $origData['shipping_description'];

                $metadata = [
                    'metadata' => [
                        'from'         => 'exportEventsPurchase',
                        'timeOfAction' => time()
                    ]
                ];

                if (Conf::getInstance()->getPurchaseValueAsBaseGrandTotal()) {
                    $value = (float) $order->getBaseGrandTotal();
                    $currencyCode = $order->getBaseCurrency()->getCode();
                } else {
                    $value = (float) $order->getGrandTotal();
                    $currencyCode = $order->getOrderCurrency()->getCode();
                }

                $this->eventsCollection->addItem(
                    $Event
                        ->setEmail($order->getCustomerEmail())
                        ->setContactExtEventType(Event::EVENT_TYPE_PURCHASE)
                        ->setProducts($mainProductId)
                        ->setDescription($order->getPayment()->getMethod())
                        ->setDate(strtotime($order->getData('created_at')))
                        ->setExternalId($order->getRealOrderId())
                        ->setLocation(Conf::getInstance()->getLocation())
                        ->setValue($value)
                        ->setShopDomain($storeUrl)
                        ->setDetails([
                            1 => implode(
                                ",",
                                array_map(
                                    function ($product) {
                                        return $product->getName();
                                    },
                                    $products
                                )
                            ),
                            2 => implode(
                                "/",
                                array_map(
                                    function ($product) {
                                        return (int)$product->getQtyOrdered();
                                    },
                                    $products
                                )
                            ),
                            3 => $detail3,
                            4 => $currencyCode,
                            5 => $shippingDescription,
                            6 => $order->getPayment()->getMethod(),
                            7 => ($order->getShippingAddress() == null)
                                ? ''
                                : $order->getShippingAddress()->getCountryId(),
                            8 => $categoriesIds,
                            9 => $this->getStoreLocale((int) $order->getStoreId()),
                            10 => json_encode($metadata)
                        ])
                );
            } catch (Exception $e) {
                $this->_logger->critical($e->getViewMessage());
            } catch (\Exception $e) {
                $this->_logger->critical($e->getMessage());
            }
        }

        return $this->eventsCollection;
    }

    /**
     * Returns total Orders to export
     *
     * @return int
     */
    public function getTotalOrdersToExport()
    {
        return $this->getOrdersCollection()->getSize();
    }

    /**
     * Count total packages to export
     *
     * @return int
     */
    public function countTotalPackagesToExport()
    {
        return (int) ceil($this->getTotalOrdersToExport() / self::EXP_PACKAGE_SIZE);
    }
}
