<?php


namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Newsletter\Model\ResourceModel\Subscriber\Collection as SubscriberCollection;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Newsletter\Model\Subscriber;

use SALESmanago\Entity\Contact\Contact;
use SALESmanago\Model\Collections\ContactsCollection;
use SALESmanago\Entity\Contact\Address;
use SALESmanago\Entity\Contact\Options;
use SALESmanago\Exception\Exception;

use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;
use Benhauer\Salesmanago\Model\Contact as ContactModel;
use Benhauer\Salesmanago\Helper\ProcessingData;

class Customers extends Export
{
    /**
     * @var SubscriberCollectionFactory
     */
    protected $subscriberFactory;

    /**
     * @var CustomerCollectionFactory
     */
    protected $customerFactory;

    /**
     * @var AddressFactory
     */
    protected $addressFactory;

    /**
     * @var Subscriber
     */
    protected $subscriber;

    /**
     * @var mixed
     */
    protected $customersCollection;

    /**
     * @var ContactsCollection
     */
    protected $contactsCollection;

    /**
     * @var MetaDataHelper
     */
    protected $metaDataHelper;

    /**
     * @var GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * @var Contact
     */
    protected $Contact;

    /**
     * @var Address
     */
    protected $Address;

    /**
     * @var Options
     */
    protected $Options;

    /**
     * @var ProcessingData
     */
    protected $processingDataHelper;

    /**
     * Customers constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TimezoneInterface $timezone
     * @param SubscriberCollectionFactory $subscriberFactory
     * @param CustomerCollectionFactory $customerFactory
     * @param AddressFactory $addressFactory
     * @param ContactsCollection $contactsCollection
     * @param Subscriber $subscriber
     * @param GroupRepositoryInterface $groupRepository
     * @param MetaDataHelper $metaDataHelper
     * @param Contact $Contact
     * @param Address $Address
     * @param Options $Options
     * @param ProcessingData $processingDataHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TimezoneInterface $timezone,
        SubscriberCollectionFactory $subscriberFactory,
        CustomerCollectionFactory $customerFactory,
        AddressFactory $addressFactory,
        ContactsCollection $contactsCollection,
        Subscriber $subscriber,
        GroupRepositoryInterface $groupRepository,
        MetaDataHelper $metaDataHelper,
        Contact $Contact,
        Address $Address,
        Options $Options,
        ProcessingData $processingDataHelper
    ) {
        parent::__construct(
            $context,
            $registry,
            $timezone,
            $metaDataHelper
        );
        $this->groupRepository      = $groupRepository;
        $this->customerFactory      = $customerFactory;
        $this->subscriberFactory    = $subscriberFactory;
        $this->addressFactory       = $addressFactory;
        $this->contactsCollection   = $contactsCollection;
        $this->subscriber           = $subscriber;
        $this->Contact              = $Contact;
        $this->Address              = $Address;
        $this->Options              = $Options;
        $this->processingDataHelper = $processingDataHelper;
    }

    /**
     * Get customer collection based on request parameters
     *
     * @param int $lastExportedPackage
     * @return Collection
     */
    public function getCustomersCollection($lastExportedPackage = 0)
    {
        $lastExportedPackage = empty($lastExportedPackage)
            ? 0
            : (int) $lastExportedPackage;

        try {
            $customerFactory = $this->customerFactory
                ->create()
                ->addFieldToSelect(['*'])
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->setCurPage($lastExportedPackage);

            $customerFactory->addFieldToFilter(
                'created_at',
                [
                    'from' => $this->dateRangeFrom,
                    'to' => $this->dateRangeTo,
                    'date' => true
                ]
            );

            if ($this->scopeId !== 0) {
                $customerFactory->addFieldToFilter(
                    'store_id',
                    ['eq' => $this->scopeId]
                );
            }

            return $customerFactory;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Return total of contacts on platform (customers and subscribers)
     *
     * @return int
     */
    public function getTotalCustomersToExport()
    {
        $CustomerCollection = $this->getCustomersCollection($this->scopeId);

        return $CustomerCollection->getSize();
    }

    /**
     * Count total customers packages to export
     *
     * @return int
     */
    public function getTotalCustomersPackagesToExport()
    {
        return (int) ceil($this->getTotalCustomersToExport() / self::EXP_PACKAGE_SIZE);
    }

    /**
     * Map data to sm contacts collection
     *
     * @param mixed $collection
     * @param mixed $exportTags
     * @return ContactsCollection
     * @throws Exception
     */
    public function prepareCustomersToExport($collection, $exportTags)
    {
        foreach ($collection as $customer) {
            $customerEmail      = $customer->getEmail();
            $billingAddressId   = $customer->getDefaultBilling();

            if ($billingAddressId) {
                $billingAddress = $this->addressFactory
                    ->create()
                    ->load($billingAddressId);
                $addr = $billingAddress->getData();
            } else {
                $addr = [];
            }

            $cSubscribed  = $this->subscriber->loadByEmail($customerEmail);
            $isSubscribed = !($customerEmail != $cSubscribed->getEmail()) && boolval($cSubscribed->isSubscribed());
            $exportTag    = explode(",", $exportTags);

            $customerBirthday = (!empty($customer->getDob()))
                ? date('Ymd', strtotime($customer->getDob()))
                : '';

            $Contact = clone $this->Contact;
            $Address = clone $this->Address;
            $Options = clone $this->Options;

            $Options
                ->setTags($exportTag)
                ->setIsSubscribed($isSubscribed)
                ->setCreatedOn(time());

            $Address
                ->setStreetAddress($addr ? $addr['street'] : '')
                ->setZipCode($addr ? $addr['postcode'] : '')
                ->setCity($addr ? $addr['city'] : '')
                ->setCountry($addr ? $this->processingDataHelper->getCountryAsISO3166($addr['country_id']) : '');

            $Contact
                ->setName($customer->getName())
                ->setEmail($customerEmail)
                ->setCompany($addr ? $addr['company'] : '')
                ->setExternalId($customer->getId())
                ->setPhone($addr ? $addr['telephone'] : '')
                ->setFax($addr ? $addr['fax'] : '')
                ->setBirthday($customerBirthday)
                ->setOptions($Options)
                ->setAddress($Address);

            $group = $this->groupRepository->getById($customer->getGroupId());

            try {
                $Contact->setProperties(
                    $Contact->getProperties()
                        ->setItems(
                        //@codingStandardsIgnoreStart
                            array_merge(
                                $this->metaDataHelper->getContactMeta('Customer_Export'),
                                [
                                    ContactModel::ID => $group->getId(),
                                    ContactModel::CODE => $group->getCode(),
                                    ContactModel::TAX_CLASS_ID => $group->getTaxClassId(),
                                    ContactModel::TAX_CLASS_NAME => $group->getTaxClassName()
                                ]
                            )
                        //@codingStandardsIgnoreEnd
                        )
                );
            } catch (Exception $err) {
                $this->_logger->warning($err->getLogMessage());
            }

            try {
                $this->contactsCollection->addItem($Contact);
            } catch (Exception $e) {
                $this->_logger->critical($e->getViewMessage());
            }
        }

        return $this->contactsCollection;
    }
}
