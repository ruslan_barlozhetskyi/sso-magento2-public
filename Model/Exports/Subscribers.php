<?php


namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Newsletter\Model\Subscriber;
use Magento\Newsletter\Model\ResourceModel\Subscriber\Collection as SubscriberCollection;

use SALESmanago\Entity\Contact\Address;
use SALESmanago\Entity\Contact\Contact;
use SALESmanago\Entity\Contact\Options;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\ContactsCollection;

use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;

class Subscribers extends Export
{
    /**
     * @var SubscriberCollectionFactory
     */
    protected $subscriberFactory;

    /**
     * @var Subscriber
     */
    protected $subscriber;

    /**
     * @var ContactsCollection
     */
    protected $contactsCollection;

    /**
     * @var Contact
     */
    protected $Contact;

    /**
     * @var Address
     */
    protected $Address;

    /**
     * @var Options
     */
    protected $Options;

    /**
     * Subscribers constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TimezoneInterface $timezone
     * @param SubscriberCollectionFactory $subscriberFactory
     * @param ContactsCollection $contactsCollection
     * @param Subscriber $subscriber
     * @param MetaDataHelper $metaDataHelper
     * @param Contact $Contact
     * @param Options $Options
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TimezoneInterface $timezone,
        SubscriberCollectionFactory $subscriberFactory,
        ContactsCollection $contactsCollection,
        Subscriber $subscriber,
        MetaDataHelper $metaDataHelper,
        Contact $Contact,
        Options $Options
    ) {
        parent::__construct(
            $context,
            $registry,
            $timezone,
            $metaDataHelper
        );

        $this->subscriberFactory  = $subscriberFactory;
        $this->contactsCollection = $contactsCollection;
        $this->subscriber         = $subscriber;
        $this->Contact            = $Contact;
        $this->Options            = $Options;
    }

    /**
     * Gets subscriber collection based on request
     *
     * @param int $lastExportedPackage
     * @return SubscriberCollection
     */
    public function getSubscribersCollection($lastExportedPackage = 0)
    {
        $lastExportedPackage = empty($lastExportedPackage)
            ? 0
            : (int) $lastExportedPackage;

        try {
            $subscriberFactory = $this->subscriberFactory->create();

            $subscriberFactory
                ->addFieldToSelect(['*'])
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->addFieldToFilter('customer_id', ['eq' => '0'])
                ->addFieldToFilter(
                    'change_status_at',
                    [
                        'from' => $this->dateRangeFrom,
                        'to' => $this->dateRangeTo,
                        'date' => true
                    ]
                )
                ->setCurPage($lastExportedPackage);

            if ($this->scopeId !== 0) {
                $subscriberFactory->addFieldToFilter(
                    'store_id',
                    ['eq' => $this->scopeId]
                );
            }

            return $subscriberFactory;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Map data to sm contacts collection
     *
     * @param mixed $collection
     * @param mixed $exportTags
     * @return ContactsCollection
     * @throws Exception
     */
    public function prepareSubscribersToExport($collection, $exportTags)
    {
        foreach ($collection as $item) {
            $customerEmail = $item->getEmail();

            $cSubscribed  = $this->subscriber->loadByEmail($customerEmail);
            $isSubscribed = !($customerEmail != $cSubscribed->getEmail()) && boolval($cSubscribed->isSubscribed());
            $exportTag    = explode(",", $exportTags);

            $Contact = clone $this->Contact;
            $Options = clone $this->Options;

            $Options
                ->setTags($exportTag)
                ->setIsSubscribed($isSubscribed)
                ->setCreatedOn(time());

            $Contact
                ->setName($item->getName())
                ->setEmail($customerEmail)
                ->setExternalId($item->getId())
                ->setOptions($Options);

            try {
                $Contact->setProperties(
                    $Contact->getProperties()
                        ->setItems(
                            $this->metaDataHelper->getContactMeta('Subscriber_Export')
                        )
                );
            } catch (Exception $err) {
                $this->_logger->warning($err->getLogMessage());
            }

            try {
                $this->contactsCollection->addItem($Contact);
            } catch (Exception $e) {
                $this->_logger->critical($e->getViewMessage());
            }
        }

        return $this->contactsCollection;
    }

    /**
     * Count total packages of subscribers to export
     *
     * @return int
     */
    public function getTotalSubscribersPackagesToExport()
    {
        return (int) ceil($this->getTotalSubscribersToExport() / self::EXP_PACKAGE_SIZE);
    }

    /**
     * Return total of subscribers on platform
     *
     * @return int
     */
    public function getTotalSubscribersToExport()
    {
        $SubscribersCollection = $this->getSubscribersCollection($this->scopeId);

        return $SubscribersCollection->getSize();
    }
}
