<?php


namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;

use SALESmanago\Entity\Contact\Address;
use SALESmanago\Entity\Contact\Contact;
use SALESmanago\Entity\Contact\Options;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\ContactsCollection;

use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;
use Benhauer\Salesmanago\Helper\ProcessingData;

class GuestCustomers extends Export
{
    /**
     * @var OrderCollectionFactory
     */
    protected $orderFactory;

    /**
     * @var ContactsCollection
     */
    protected $contactsCollection;

    /**
     * @var Subscriber
     */
    protected $subscriber;

    /**
     * @var Contact
     */
    protected $Contact;

    /**
     * @var Address
     */
    protected $Address;

    /**
     * @var Options
     */
    protected $Options;

    /**
     * @var ProcessingData
     */
    protected $processingDataHelper;

    /**
     * GuestCustomers constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TimezoneInterface $timezone
     * @param OrderCollectionFactory $orderFactory
     * @param Subscriber $subscriber
     * @param MetaDataHelper $metaDataHelper
     * @param Contact $Contact
     * @param Address $Address
     * @param Options $Options
     * @param ContactsCollection $contactsCollection
     * @param ProcessingData $processingDataHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TimezoneInterface $timezone,
        OrderCollectionFactory $orderFactory,
        Subscriber $subscriber,
        MetaDataHelper $metaDataHelper,
        Contact $Contact,
        Address $Address,
        Options $Options,
        ContactsCollection $contactsCollection,
        ProcessingData $processingDataHelper
    ) {
        parent::__construct(
            $context,
            $registry,
            $timezone,
            $metaDataHelper
        );

        $this->processingDataHelper = $processingDataHelper;
        $this->orderFactory         = $orderFactory;
        $this->subscriber           = $subscriber;
        $this->Contact              = $Contact;
        $this->Address              = $Address;
        $this->Options              = $Options;
        $this->contactsCollection   = $contactsCollection;
    }

    /**
     * Get filtered customers collection
     *
     * @param int $lastExportedPackage
     * @return OrderCollection
     */
    public function getGuestOrdersCollection($lastExportedPackage = 0)
    {
        $lastExportedPackage = empty($lastExportedPackage)
            ? 0
            : (int) $lastExportedPackage;

        $orderFactory = $this->orderFactory
            ->create()
            ->addFieldToSelect('*')
            ->setPageSize(self::EXP_PACKAGE_SIZE)
            ->setCurPage($lastExportedPackage);

        $orderFactory
            ->addAttributeToFilter(
                'customer_is_guest',
                [
                    'eq' => 1
                ]
            );

        $orderFactory->addFieldToFilter(
            'created_at',
            [
                'from' => $this->dateRangeFrom,
                'to' => $this->dateRangeTo,
                'date' => true
            ]
        );

        if ($this->scopeId !== 0) {
            $orderFactory->addFieldToFilter(
                'store_id',
                [
                    'eq' => $this->scopeId
                ]
            );
        }

        return $orderFactory;
    }

    /**
     * Build array for api
     *
     * @param OrderCollection $collection
     * @param string $exportTags strings separated by comma
     * @return ContactsCollection
     */
    public function prepareCustomersToExport(OrderCollection $collection, $exportTags = '')
    {
        foreach ($collection as $order) {
            $Contact = clone $this->Contact;
            $Address = clone $this->Address;
            $Options = clone $this->Options;

            $billingAddress = $order->getBillingAddress();

            $exportTag = explode(",", $exportTags);

            $Options
                ->setTags($exportTag)
                ->setIsSubscriptionStatusNoChange(true)
                ->setCreatedOn($order->getData('created_at'));

            $Address
                ->setStreetAddress($billingAddress->getData('street'))
                ->setZipCode($billingAddress->getData('postcode'))
                ->setCity($billingAddress->getData('city'))
                ->setCountry($this->processingDataHelper->getCountryAsISO3166($billingAddress->getData('country_id')));

            $Contact
                ->setName([$order->getData('customer_firstname'), $order->getData('customer_lastname')])
                ->setEmail($order->getData('customer_email'))
                ->setCompany($billingAddress->getData('company'))
                ->setPhone($billingAddress->getData('telephone'))
                ->setOptions($Options)
                ->setAddress($Address);

            try {
                $Contact->setProperties(
                    $Contact->getProperties()
                        ->setItems(
                            $this->metaDataHelper->getContactMeta('GuestCustomer_Export')
                        )
                );
            } catch (Exception $err) {
                $this->_logger->warning($err->getLogMessage());
            }

            try {
                $this->contactsCollection->addItem($Contact);
            } catch (Exception $e) {
                $this->_logger->critical($e->getViewMessage());
            }
        }

        return $this->contactsCollection;
    }

    /**
     * Count total packages to export
     *
     * @return int
     */
    public function getTotalGuestCustomersPackagesToExport()
    {
        return (int) ceil($this->countTotalGuestCustomersToExport() / self::EXP_PACKAGE_SIZE);
    }

    /**
     * Count total items to export
     *
     * @return int
     */
    public function countTotalGuestCustomersToExport()
    {
        $guestCustomerCollection = $this->getGuestOrdersCollection($this->scopeId);
        return $guestCustomerCollection->getSize();
    }
}
