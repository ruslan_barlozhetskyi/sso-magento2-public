<?php


namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Model\ResourceModel\Quote\Collection as QuoteCollection;
use Magento\Quote\Model\ResourceModel\Quote\Item\CollectionFactory as QuoteItemCollectionFactory;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Store\Model\StoreManagerInterface;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\EventsCollection;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Model\CartRecovery;
use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;

class Quotes extends Export
{
    /**
     * @var QuoteItemCollectionFactory
     */
    protected $quoteItemCollectionFactory;

    /**
     * @var QuoteCollectionFactory
     */
    public $quoteCollectionFactory;

    /**
     * @var CartRecovery
     */
    protected $cartRecoveryModel;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var Event
     */
    protected $Event;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var EventsCollection
     */
    protected $eventsCollection;

    /**
     * Quotes constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TimezoneInterface $timezone
     * @param QuoteItemCollectionFactory $quoteItemCollectionFactory
     * @param QuoteCollectionFactory $quoteCollectionFactory
     * @param ProductRepository $productRepository
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     * @param EventsCollection $eventsCollection
     * @param CartRecovery $cartRecoveryModel
     * @param MetaDataHelper $metaDataHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TimezoneInterface $timezone,
        QuoteItemCollectionFactory $quoteItemCollectionFactory,
        QuoteCollectionFactory $quoteCollectionFactory,
        ProductRepository $productRepository,
        Event $Event,
        StoreManagerInterface $storeManager,
        EventsCollection $eventsCollection,
        CartRecovery $cartRecoveryModel,
        MetaDataHelper $metaDataHelper
    ) {
        parent::__construct(
            $context,
            $registry,
            $timezone,
            $metaDataHelper
        );

        $this->quoteItemCollectionFactory = $quoteItemCollectionFactory;
        $this->quoteCollectionFactory     = $quoteCollectionFactory;
        $this->productRepository          = $productRepository;
        $this->Event                      = $Event;
        $this->storeManager               = $storeManager;
        $this->eventsCollection           = $eventsCollection;
        $this->cartRecoveryModel          = $cartRecoveryModel;
    }
    /**
     * @var string[]
     */
    public static $exportTypes = [
        'exportQuotes' => 'exportQuotes'
    ];

    /**
     * Get quotes data
     *
     * @param QuoteCollection $quotesCollection
     * @return array
     */
    public function getQuotesData(QuoteCollection $quotesCollection)
    {
        try {
            $quotesData = $quotesCollection->getData();

            foreach ($quotesData as $key => $quoteData) {
                $quotesData[$key]['items'] = [];
                $quotesData[$key]['items'] = $this->quoteItemCollectionFactory
                    ->create()
                    ->addFieldToFilter(
                        'quote_id',
                        [
                            'eq' => $quoteData['entity_id']
                        ]
                    )->addFieldToSelect([
                        'sku',
                        'product_id',
                        'qty',
                        'name'
                    ])
                    ->getData();
            }
            return $quotesData;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Get Quotes collection to export
     *
     * @param int $lastExportedPackage
     * @return QuoteCollection
     */
    public function getQuotesCollection($lastExportedPackage = 0)
    {
        try {
            $quoteCollection = $this->quoteCollectionFactory
                ->create()
                ->addFieldToSelect([
                    'customer_id',
                    'customer_email',
                    'customer_firstname',
                    'customer_middlename',
                    'customer_lastname',
                    'subtotal',
                    'items_qty',
                    'orig_order_id',
                    'entity_id',
                    'created_at',
                    'store_id',
                    'grand_total',
                    'base_grand_total',
                    'quote_currency_code',
                    'base_currency_code',
                ])
                ->addFieldToFilter(
                    'customer_email',
                    [
                        'notnull' => true
                    ]
                )
                ->setPageSize(self::EXP_PACKAGE_SIZE)
                ->setCurPage($lastExportedPackage+1);

            if ($this->scopeId !== 0) {
                $quoteCollection->addFieldToFilter(
                    'store_id',
                    [
                        'eq' => $this->scopeId
                    ]
                );
            }

            $quoteCollection->addFieldToFilter(
                'created_at',
                [
                    'from' => $this->dateRangeFrom,
                    'to'   => $this->dateRangeTo,
                    'date' => true
                ]
            );

            return $quoteCollection;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Map quotes to SM event
     *
     * @param array $quotesData
     * @return EventsCollection
     */
    public function prepareQuotesToExport($quotesData)
    {
        try {
            foreach ($quotesData as $quote) {
                $Event = clone $this->Event;

                switch (Conf::getInstance()->getProductTypeId()) {
                    case EventModel::$productIdDetail['sku']:
                        $mainProductId = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['sku'];
                        }, $quote['items']));
                        $detail3 = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['product_id'];
                        }, $quote['items']));
                        break;

                    default:
                        $mainProductId = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['product_id'];
                        }, $quote['items']));
                        $detail3 = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['sku'];
                        }, $quote['items']));
                }

                try {
                    $categoriesIds = implode(
                        ",",
                        array_map(
                            function ($quoteItem) {
                                $categoryIds = $this->productRepository
                                    ->get($quoteItem['sku'])
                                    ->getCategoryIds();

                                return empty($categoryIds) ? 'ncat' : implode('/', $categoryIds);
                            },
                            $quote['items']
                        )
                    );
                } catch (\Exception $e) {
                    $categoriesIds = '';
                }

                $shopDomain = $this->storeManager
                    ->getStore($quote['store_id'])
                    ->getBaseUrl(UrlInterface::URL_TYPE_WEB);

                $detail1 = implode(",", array_map(function ($quoteItem) {
                    return $quoteItem['name'] ?? '';
                }, $quote['items']));

                $detail2 = implode("/", array_map(function ($quoteItem) {
                    return isset($quoteItem['qty']) ? (int)$quoteItem['qty'] : '';
                }, $quote['items']));

                if (Conf::getInstance()->getPurchaseValueAsBaseGrandTotal()) {
                    $value = (float) $quote['base_grand_total'];
                    $detail4 = $quote['base_currency_code'] ?? '';
                } else {
                    $value = (float) $quote['grand_total'];
                    $detail4 = $quote['quote_currency_code'] ?? '';
                }

                $metadata = [
                    'metadata' => [
                        'from'         => 'exportQuotes',
                        'timeOfAction' => time()
                    ]
                ];

                $this->eventsCollection->addItem(
                    $Event
                        ->setDate(strtotime($quote['created_at'] ?? ''))
                        ->setLocation(Conf::getInstance()->getLocation())
                        ->setValue($value ?? '')
                        ->setExternalId($quote['entity_id'] ?? '')
                        ->setShopDomain($shopDomain)
                        ->setContactExtEventType(Event::EVENT_TYPE_CART)
                        ->setDetails([
                            1 => $detail1,
                            2 => $detail2,
                            3 => $detail3,
                            4 => $detail4,
                            //details 5,6,7 no needed but api-sso-util:3.0.5 slide off indexes id details not set
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => $categoriesIds,
                            9 => $this->getStoreLocale($quote['store_id'] ?? 0),
                            10 => json_encode($metadata)
                        ])
                        ->setEmail($quote['customer_email'])
                        ->setProducts($mainProductId)
                );

                $Event->setDescription($this->cartRecoveryModel->getRecoveryUrl($Event));
            }

            return $this->eventsCollection;
        } catch (Exception $e) {
            $this->_logger->critical($e->getViewMessage());
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Return total quotes to export
     *
     * @return int
     */
    public function getTotalQuotesToExport()
    {
        return $this->getQuotesCollection()->getSize();
    }

    /**
     * Count total packages to export
     *
     * @return int
     */
    public function countTotalPackagesToExport()
    {
        return (int) ceil($this->getTotalQuotesToExport() / self::EXP_PACKAGE_SIZE);
    }
}
