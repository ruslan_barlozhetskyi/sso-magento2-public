<?php

namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;
use Magento\Quote\Model\ResourceModel\Quote\Item\CollectionFactory as QuoteItemCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\CartRecovery;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Model\Export;

use SALESmanago\Entity\Contact\Address;
use SALESmanago\Entity\Contact\Contact;
use SALESmanago\Entity\Contact\Options;
use SALESmanago\Entity\Event\Event;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\ContactsCollection;
use SALESmanago\Model\Collections\EventsCollection;

/**
 * @deprecated since 4.1.2
 * @see Benhauer\Salesmanago\Model\Exports\Quotes
 */
class ExportQuotes extends Export
{
    /**
     * @var QuoteItemCollectionFactory
     */
    protected $quoteItemCollectionFactory;

    /**
     * @var CartRecovery
     */
    protected $cartRecoveryModel;

    /**
     * ExportQuotes constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param CustomerCollectionFactory $customerFactory
     * @param SubscriberCollectionFactory $subscriberFactory
     * @param OrderCollectionFactory $orderFactory
     * @param AddressFactory $addressFactory
     * @param TimezoneInterface $timezone
     * @param ContactsCollection $contactsCollection
     * @param EventsCollection $eventsCollection
     * @param Event $Event
     * @param Address $Address
     * @param Contact $Contact
     * @param Options $Options
     * @param QuoteItemCollectionFactory $quoteItemCollectionFactory
     * @param CartRecovery $cartRecoveryModel
     * @param QuoteCollectionFactory $quoteCollectionFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param ProductRepository $productRepository
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager,
        CustomerCollectionFactory $customerFactory,
        SubscriberCollectionFactory $subscriberFactory,
        OrderCollectionFactory $orderFactory,
        AddressFactory $addressFactory,
        TimezoneInterface $timezone,
        ContactsCollection $contactsCollection,
        EventsCollection $eventsCollection,
        Event $Event,
        Address $Address,
        Contact $Contact,
        Options $Options,
        QuoteItemCollectionFactory $quoteItemCollectionFactory,
        CartRecovery $cartRecoveryModel,
        QuoteCollectionFactory $quoteCollectionFactory,
        ScopeConfigInterface $scopeConfigInterface,
        ProductRepository $productRepository
    ) {
        parent::__construct(
            $context,
            $registry,
            $storeManager,
            $customerFactory,
            $subscriberFactory,
            $orderFactory,
            $addressFactory,
            $timezone,
            $contactsCollection,
            $eventsCollection,
            $Event,
            $Address,
            $Contact,
            $Options,
            $quoteCollectionFactory,
            $scopeConfigInterface,
            $productRepository
        );
        $this->quoteItemCollectionFactory = $quoteItemCollectionFactory;
        $this->cartRecoveryModel          = $cartRecoveryModel;
    }

    /**
     * @var string[]
     */
    public static $exportTypes = [
        'exportQuotes' => 'exportQuotes'
    ];

    /**
     * Get quotes data
     *
     * @param mixed $params
     * @param int $store_id
     * @return array
     */
    public function getQuotesData($params, $store_id = 0)
    {
        try {
            $quotesData = $this->getQuotesCollection($params, $store_id)->getData();

            foreach ($quotesData as $key => $quoteData) {
                $quotesData[$key]['items'] = [];
                $quotesData[$key]['items'] = $this->quoteItemCollectionFactory
                    ->create()
                    ->addFieldToFilter(
                        'quote_id',
                        ['eq' => $quoteData['entity_id']]
                    )->addFieldToSelect([
                        'sku',
                        'product_id',
                        'qty',
                        'name'
                    ])
                    ->getData();
            }
            return $quotesData;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Map quotes to SM event
     *
     * @param array $quotesData
     * @return EventsCollection
     */
    public function prepareQuotesToExport($quotesData)
    {
        try {
            foreach ($quotesData as $quote) {
                $Event = clone $this->Event;

                switch (Conf::getInstance()->getProductTypeId()) {
                    case EventModel::$productIdDetail['sku']:
                        $mainProductId = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['sku'];
                        }, $quote['items']));
                        $detail3 = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['product_id'];
                        }, $quote['items']));
                        break;

                    default:
                        $mainProductId = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['product_id'];
                        }, $quote['items']));
                        $detail3 = implode(",", array_map(function ($quoteItem) {
                            return $quoteItem['sku'];
                        }, $quote['items']));
                }

                try {
                    $categoriesIds = implode(
                        ",",
                        array_map(
                            function ($quoteItem) {
                                $categoryIds = $this->productRepository
                                    ->get($quoteItem['sku'])
                                    ->getCategoryIds();

                                return empty($categoryIds) ? 'ncat' : implode('/', $categoryIds);
                            },
                            $quote['items']
                        )
                    );
                } catch (\Exception $e) {
                    $categoriesIds = '';
                }

                $shopDomain = $this->storeManager
                    ->getStore($quote['store_id'])
                    ->getBaseUrl(UrlInterface::URL_TYPE_WEB);

                $detail1 = implode(",", array_map(function ($quoteItem) {
                    return $quoteItem['name'] ?? '';
                }, $quote['items']));

                $detail2 = implode("/", array_map(function ($quoteItem) {
                    return isset($quoteItem['qty']) ? (int)$quoteItem['qty'] : '';
                }, $quote['items']));

                $detail4 = $quote['quote_currency_code'] ?? '';

                $metadata = [
                    'metadata' => [
                        'from'         => 'exportQuotes',
                        'timeOfAction' => time()
                    ]
                ];

                $this->eventsCollection->addItem(
                    $Event
                        ->setDate(strtotime($quote['created_at'] ?? ''))
                        ->setLocation(Conf::getInstance()->getLocation())
                        ->setValue((float) $quote['grand_total'] ?? '')
                        ->setExternalId($quote['entity_id'] ?? '')
                        ->setShopDomain($shopDomain)
                        ->setContactExtEventType(Event::EVENT_TYPE_CART)
                        ->setDetails([
                            1 => $detail1,
                            2 => $detail2,
                            3 => $detail3,
                            4 => $detail4,
                            //details 5,6,7 no needed but api-sso-util:3.0.5 slide off indexes id details not set
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => $categoriesIds,
                            9 => $this->getStoreLocale($quote['store_id'] ?? 0),
                            10 => json_encode($metadata)
                        ])
                        ->setEmail($quote['customer_email'])
                        ->setProducts($mainProductId)
                );

                $Event->setDescription($this->cartRecoveryModel->getRecoveryUrl($Event));
            }

            return $this->eventsCollection;
        } catch (Exception $e) {
            $this->_logger->critical($e->getViewMessage());
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }
}
