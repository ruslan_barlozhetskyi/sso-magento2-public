<?php


namespace Benhauer\Salesmanago\Model\Exports;

use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;

class Export extends MagentoAbstractModel
{
    public const
        EXP_PACKAGE_SIZE = 300,
        DEFAULT_DATE_FROM = 946688461;

    /**
     * @var MetaDataHelper
     */
    protected $metaDataHelper;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var int
     */
    protected $dateRangeFrom;

    /**
     * @var int
     */
    protected $dateRangeTo;

    /**
     * @var int
     */
    protected $scopeId = 0;// by default in exports means to export from all scopes

    /**
     * Export constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TimezoneInterface $timezone
     * @param MetaDataHelper $metaDataHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TimezoneInterface $timezone,
        MetaDataHelper $metaDataHelper
    ) {
        parent::__construct(
            $context,
            $registry
        );

        $this->timezone = $timezone;
        $this->metaDataHelper = $metaDataHelper;
    }

    /**
     * Sets scope id
     *
     * @param int $param
     * @return $this
     */
    public function setScopeId($param)
    {
        $this->scopeId = $param;
        return $this;
    }

    /**
     * Sets data range to filter collections to exports
     *
     * @param string|null $from
     * @param string|null $to
     * @return array
     */
    public function setExportDateRange($from = null, $to = null)
    {
        if (empty($from)) {
            $from = self::DEFAULT_DATE_FROM; //default old date couldn't be 0
        } elseif (is_numeric($from)) {
            $from = (int) $from;
        }

        if (empty($to)) {
            $to = time();
        } elseif (is_numeric($to)) {
            $to = (int) $to;
        }

        $this->dateRangeFrom = $this->timezone->date($from)->format('Y-m-d H:i:s');
        $this->dateRangeTo = $this->timezone->date($to)->format('Y-m-d H:i:s');

        $this->dateRangeFrom = strtotime($this->dateRangeFrom);
        $this->dateRangeTo = strtotime($this->dateRangeTo) + 86400;

        return [
            'from' => $this->dateRangeFrom,
            'to' => $this->dateRangeTo
        ];
    }

    /**
     * Return export date range to
     *
     * @return int
     */
    public function getDateRangeTo()
    {
        if (!isset($this->dateRangeTo) || empty($this->dateRangeTo)) {
            $this->dateRangeTo = time();
            $this->dateRangeTo = $this->timezone->date($this->dateRangeTo)->format('Y-m-d H:i:s');
            $this->dateRangeTo = strtotime($this->dateRangeTo) + 86400;
        }

        return $this->dateRangeTo;
    }

    /**
     * Return export date range from
     *
     * @return int
     */
    public function getDateRangeFrom()
    {
        if (!isset($this->dateRangeFrom) || empty($this->dateRangeFrom)) {
            $this->dateRangeFrom = self::DEFAULT_DATE_FROM; //default old date couldn't be 0
            $this->dateRangeFrom = strtotime($this->dateRangeFrom);
        }

        return $this->dateRangeFrom;
    }

    /**
     * Setup basic of exports (date, scope/store id)
     *
     * @param mixed $dateFrom - date range od export from
     * @param mixed $dateTo - date range od export to
     * @param mixed $scopeId - scope id for store scope od exported data
     *
     * @return $this
     */
    public function setUpExport($dateFrom = null, $dateTo = null, $scopeId = 0)
    {
        $this->setExportDateRange($dateFrom, $dateTo);
        $this->setScopeId($scopeId);

        return $this;
    }
}
