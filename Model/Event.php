<?php

namespace Benhauer\Salesmanago\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\Locale\Resolver;
use Magento\SalesRule\Model\Coupon;
use Magento\SalesRule\Model\Rule;

use SALESmanago\Entity\Event\Event as EventEntity;

use Benhauer\Salesmanago\Helper\Conf;

class Event extends MagentoAbstractModel
{
    public const
        PRODUCT_TYPE_SIMPLE       = 'simple',
        PRODUCT_TYPE_CONFIGURABLE = 'configurable',
        PRODUCT_TYPE_VIRTUAL      = 'virtual',
        PRODUCT_TYPE_DOWNLOADABLE = 'downloadable';

    /**
     * @var string[]
     */
    public static $productIdDetail = [
        'id'         => 'id',
        'sku'        => 'sku',
        'variant_id' => 'variant_id'
    ];

    /**
     * @var StoreInterface
     */
    public $store;

    /**
     * @var mixed
     */
    public $model;

    /**
     * @var MagentoDateTime
     */
    public $objDate;

    /**
     * @var QuoteFactory
     */
    public $quoteFactory;

    /**
     * @var EventEntity
     */
    public $Event;

    /**
     * @var CartRecovery
     */
    protected $cartRecoveryModel;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var Resolver
     */
    protected $localeResolver;

    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * @var Rule
     */
    protected $saleRule;

    /**
     * Event constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param EventEntity $Event
     * @param StoreInterface $store
     * @param MagentoDateTime $objDate
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param QuoteFactory $quoteFactory
     * @param CartRecovery $cartRecoveryModel
     * @param Resolver $localeResolver
     * @param Coupon $coupon
     * @param Rule $saleRule
     */
    public function __construct(
        Context $context,
        Registry $registry,
        EventEntity $Event,
        StoreInterface $store,
        MagentoDateTime $objDate,
        CustomerRepositoryInterface $customerRepositoryInterface,
        QuoteFactory $quoteFactory,
        CartRecovery $cartRecoveryModel,
        Resolver $localeResolver,
        Coupon $coupon,
        Rule $saleRule
    ) {
        parent::__construct(
            $context,
            $registry
        );

        $this->Event                       = $Event;
        $this->store                       = $store;
        $this->objDate                     = $objDate;
        $this->quoteFactory                = $quoteFactory;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->cartRecoveryModel           = $cartRecoveryModel;
        $this->localeResolver              = $localeResolver;
        $this->coupon                      = $coupon;
        $this->saleRule                    = $saleRule;
    }

    /**
     * Gets event from order
     *
     * @param mixed $order
     * @return mixed
     */
    public function getEventFromOrder($order)
    {
        try {
            $dateTime = $this->objDate->date();

            $prods = [
                'ids'         => [],
                'names'       => [],
                'qty'         => [],
                'sku'         => [],
                'categoryIds' => []
            ];

            foreach ($order->getAllItems() as $item) {

                if ($this->duplicationCheck($item, $prods)) {
                    try {
                        $categoryIds = $item->getProduct()->getCategoryIds();
                    } catch (\Exception $e) {
                        $categoryIds = [];
                    }

                    //if product doesn't assign to any category 'ncat' is added as product category;
                    $categoryIds = (!empty($categoryIds)) ? implode('/', $categoryIds) : 'ncat';

                    array_push($prods['categoryIds'], $categoryIds);
                    array_push($prods['ids'], $item->getProductId());
                    array_push($prods['names'], $item->getName());
                    array_push($prods['qty'], (int) $item->getQtyOrdered());
                    array_push($prods['sku'], $item->getSku());
                }
            }

            try {
                $paymentMethod = $order->getPayment()->getMethod();
            } catch (\Exception $e) {
                $paymentMethod = '';
            }

            try {
                $shippingCountryId = ($order->getShippingAddress() == null)
                    ? ''
                    : $order->getShippingAddress()->getCountryId();
            } catch (\Exception $e) {
                $shippingCountryId = '';
            }

            $categoriesIds = (!empty($prods['categoryIds'])) ? implode(',', $prods['categoryIds']) : '';

            $this->setDetailsWithProductTypeId($prods);

            $origData = $order->getOrigData();
            $shippingDescription = (empty($origData) || !isset($origData['shipping_description']))
                ? ''
                : $origData['shipping_description'];

            $metadata = [
                'metadata' => [
                    'from'         => 'hookPurchaseEv',
                    'timeOfAction' => time()
                ]
            ];

            if (Conf::getInstance()->getPurchaseValueAsBaseGrandTotal()) {
                $value = (float) $order->getBaseGrandTotal();
                $currencyCode = $order->getBaseCurrency()->getCode();
            } else {
                $value = (float) $order->getGrandTotal();
                $currencyCode = $order->getOrderCurrency()->getCode();
            }

            $this->Event
                ->setDate($dateTime->format('c'))
                ->setLocation(Conf::getInstance()->getLocation())
                ->setValue($value)
                ->setExternalId($order->getIncrementId())
                ->setShopDomain($this->store->getBaseUrl(UrlInterface::URL_TYPE_WEB))
                ->setContactExtEventType('PURCHASE')
                ->setDetail($currencyCode, 4)
                ->setDetail($shippingDescription, 5)
                ->setDetail($paymentMethod, 6)
                ->setDetail($shippingCountryId, 7)
                ->setDetail($categoriesIds, 8)
                ->setDetail($this->getCurrentLocale(), 9)
                ->setDetail(json_encode($metadata), 10)
                ->setDetail($order->getCouponCode(), 11)
                ->setDetail($this->getDiscountIfExist($order->getCouponCode()), 12)
                ->setDetail($this->getRuleNameByCouponCode($order->getCouponCode()), 13)
                ->setDescription($order->getPayment()->getMethod())
                ->setForceOptIn(false);

            return $this->Event;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Return Event by cart
     *
     * @param mixed $observer
     * @param null|string $contactId
     * @return mixed
     */
    public function getEventByCartModified($observer, $contactId = null)
    {
        try {
            $cart = $observer->getCart();

            $prods = [
                'ids'         => [],
                'names'       => [],
                'qty'         => [],
                'sku'         => [],
                'categoryIds' => []
            ];

            $dateTime = $this->objDate->date();

            $items = $cart->getItems();

            //$items->getItems() is need for processing data for checkout_cart_save_after hook
            $items = is_array($items) ? $items : $items->getItems();

            foreach ($items as $item) {
                if ($this->duplicationCheck($item, $prods)) {
                    try {
                        $categoryIds = $item->getProduct()->getCategoryIds();
                    } catch (\Exception $e) {
                        $categoryIds = [];
                    }

                    //if product doesn't assign to any category 'ncat' is added as product category;
                    $categoryIds = (!empty($categoryIds)) ? implode('/', $categoryIds) : 'ncat';

                    array_push($prods['categoryIds'], $categoryIds);
                    array_push($prods['ids'], $item->getProductId());
                    array_push($prods['names'], $item->getName());
                    array_push($prods['qty'], $item->getQty());
                    array_push($prods['sku'], $item->getSku());
                }
            }

            $categoriesIds = (!empty($prods['categoryIds'])) ? implode(',', $prods['categoryIds']) : '';

            $this->setDetailsWithProductTypeId($prods);

            $metadata = [
                'metadata' => [
                    'from'         => ($observer->getEvent() ? $observer->getEvent()->getName() : ''),
                    'timeOfAction' => time()
                ]
            ];

            $quote = $cart->getQuote();

            if (Conf::getInstance()->getPurchaseValueAsBaseGrandTotal()) {
                $value = $quote ? $quote->getBaseGrandTotal() : $cart->getBaseGrandTotal();
                $currencyCode = $quote ? $quote->getBaseCurrencyCode() : $cart->getBaseCurrencyCode();
            } else {
                $value = $quote ? $quote->getGrandTotal() : $cart->getGrandTotal();
                $currencyCode = $quote ? $quote->getQuoteCurrencyCode() : $cart->getCurrencyCode();
            }

            $externalId = $quote ? $quote->getId() : $cart->getId();

            if (!empty($contactId)) {
                $this->Event->setContactId($contactId);
            }

            $this->Event
                ->setDate($dateTime->format('c'))
                ->setLocation(Conf::getInstance()->getLocation())
                ->setValue((float) $value)
                ->setExternalId($externalId)
                ->setShopDomain($this->store->getBaseUrl(UrlInterface::URL_TYPE_WEB))
                ->setContactExtEventType(EventEntity::EVENT_TYPE_CART)
                ->setDetail($currencyCode, 4)
                //details 5,6,7 no needed but api-sso-util:3.0.5 slide off indexes id details not set
                ->setDetail('', 5)
                ->setDetail('', 6)
                ->setDetail('', 7)
                ->setDetail($categoriesIds, 8)
                ->setDetail($this->getCurrentLocale(), 9)
                ->setDetail(json_encode($metadata), 10);

            $this->Event->setDescription($this->cartRecoveryModel->getRecoveryUrl($this->Event, $contactId));

            return $this->Event;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Switching types of primary id for products in SALESmanago Event
     *
     * @param array $prods
     */
    protected function setDetailsWithProductTypeId($prods)
    {
        switch (Conf::getInstance()->getProductTypeId()) {
            case self::$productIdDetail['sku']:
                $this->Event
                    ->setProducts($prods['sku'])
                    ->setDetail(implode(",", $prods['names']), 1)
                    ->setDetail(implode("/", $prods['qty']), 2)
                    ->setDetail(implode(",", $prods['ids']), 3);
                break;

            default:
                $this->Event
                    ->setProducts($prods['ids'])
                    ->setDetail(implode(",", $prods['names']), 1)
                    ->setDetail(implode("/", $prods['qty']), 2)
                    ->setDetail(implode(",", $prods['sku']), 3);
        }
    }

    /**
     * Get current store locale
     *
     * @return false|string
     */
    protected function getCurrentLocale()
    {
        try {
            $currentLocaleCode = $this->localeResolver->getLocale();
            return strstr($currentLocaleCode, '_', true);
        } catch (\Exception $e) {
            return strtoupper(strstr(Resolver::DEFAULT_LOCALE, '_', true));
        }
    }

    /**
     * Checking duplications of processing products.
     *
     * Basically used for exclude basics products which added to each configurable products;
     *
     * @param mixed $item
     * @param array $prods
     * @return bool
     */
    protected function duplicationCheck($item, $prods)
    {
        try {
            return (
                in_array(
                    $item->getProductType(),
                    [
                        self::PRODUCT_TYPE_SIMPLE,
                        self::PRODUCT_TYPE_VIRTUAL,
                        self::PRODUCT_TYPE_DOWNLOADABLE
                    ]
                )
                && !in_array($item->getSku(), $prods['sku'])
                && !in_array($item->getName(), $prods['names'])
                && !in_array($item->getProductId(), $prods['ids'])
            );
        } catch (\Exception $exception) {
            $this->_logger->error($exception->getTraceAsString());
            return false;
        }
    }

    /**
     * Return discount if exist
     *
     * @param string $couponCode
     * @return float
     */
    protected function getDiscountIfExist($couponCode = null)
    {
        if (empty($couponCode)) {
            return null;
        }

        $ruleId = $this->coupon->loadByCode($couponCode)->getRuleId();
        $rule = $this->saleRule->load($ruleId);
        $discountAmount = $rule->getDiscountAmount();
        return $discountAmount ? number_format($discountAmount, 2) : null;
    }

    /**
     * Return price rule name by coupon code
     *
     * @param string $couponCode
     * @return string|null
     */
    protected function getRuleNameByCouponCode($couponCode = null)
    {
        if (empty($couponCode)) {
            return null;
        }

        $ruleId = $this->coupon->loadByCode($couponCode)->getRuleId();
        $rule = $this->saleRule->load($ruleId);
        return $rule->getName();
    }
}
