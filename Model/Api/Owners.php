<?php

namespace Benhauer\Salesmanago\Model\Api;

use Psr\Log\LoggerInterface;
use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\Model\Context;
use \Magento\Framework\Registry;

use Benhauer\Salesmanago\Helper\Conf;

use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Exception\Exception;

class Owners extends MagentoAbstractModel
{
    /**
     * @var FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * Owners constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $registry
        );
        $this->factoryOrganizer = $factoryOrganizer;
        $this->logger = $logger;
    }

    /**
     * Returns list od owners from SM API
     *
     * @return array
     */
    public function getList()
    {
        try {
            if (empty(Conf::getInstance()->getClientId())) {
                return [];
            }

            $userc = $this->factoryOrganizer->getInst(
                FactoryOrganizer::USER_C,
                Conf::getInstance()
            );

            return $userc->getOwnersList();
        } catch (Exception $exception) {
            $this->logger->error($exception->getLogMessage());
        }
    }
}
