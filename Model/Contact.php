<?php

namespace Benhauer\Salesmanago\Model;

use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Framework\Locale\Resolver;
use Magento\Customer\Api\GroupRepositoryInterface;

use SALESmanago\Exception\Exception;
use SALESmanago\Entity\Contact\Contact as SmContact;

use Benhauer\Salesmanago\Helper\MetaData as MetaDataHelper;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Helper\ProcessingData;

class Contact extends MagentoAbstractModel
{
    public const
        ID             = 'id',
        CODE           = 'code',
        TAX_CLASS_ID   = 'taxClassId',
        TAX_CLASS_NAME = 'taxClassName';

    /**
     * @var SmContact
     */
    protected $Contact;

    /**
     * @var SmContact
     */
    protected $cContact;

    /**
     * @var Subscriber
     */
    public $subscriber;

    /**
     * @var CustomerRepositoryInterface
     */
    public $customerRepositoryInterface;

    /**
     * @var CountryInformationAcquirerInterface
     */
    public $countryInformation;

    /**
     * @var bool - flag to check if newsletter hook try to get data
     */
    protected $newsletterHook = false;

    /**
     * @var Resolver
     */
    private $localeResolver;

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @var MetaDataHelper
     */
    private $metaDataHelper;

    /**
     * @var ProcessingData
     */
    private $processingDataHelper;

    /**
     * Contact constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param SmContact $Contact
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param Subscriber $subscriber
     * @param CountryInformationAcquirerInterface $countryInformation
     * @param Resolver $localeResolver
     * @param GroupRepositoryInterface $groupRepository
     * @param MetaDataHelper $metaDataHelper
     * @param ProcessingData $processingDataHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        SmContact $Contact,
        CustomerRepositoryInterface $customerRepositoryInterface,
        Subscriber $subscriber,
        CountryInformationAcquirerInterface $countryInformation,
        Resolver $localeResolver,
        GroupRepositoryInterface $groupRepository,
        MetaDataHelper $metaDataHelper,
        ProcessingData $processingDataHelper
    ) {
        parent::__construct(
            $context,
            $registry
        );

        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->Contact              = $Contact;
        $this->cContact             = $Contact;
        $this->subscriber           = $subscriber;
        $this->countryInformation   = $countryInformation;
        $this->localeResolver       = $localeResolver;
        $this->groupRepository      = $groupRepository;
        $this->metaDataHelper       = $metaDataHelper;
        $this->processingDataHelper = $processingDataHelper;
    }

    /**
     * Get customer by id
     *
     * @param int $id
     * @param mixed|null $subscriber
     * @param bool $newsletterHook
     * @param string $hookName
     * @return SmContact
     */
    public function getContactByCustomerId(
        $id,
        $subscriber = null,
        $newsletterHook = false,
        $hookName = ''
    ) {
        try {
            $this->Contact = clone $this->cContact;
            if ($subscriber != null) {
                $this->subscriber = $subscriber;
            }

            $this->newsletterHook = $newsletterHook;

            $customer = $this->customerRepositoryInterface->getById($id);

            $phone      = method_exists($customer, 'getTelephone') ? $customer->getTelephone() : '';
            $fax        = method_exists($customer, 'getFax') ? $customer->getFax() : '';
            $company    = method_exists($customer, 'getCompany') ? $customer->getCompany() : '';
            $externalId = method_exists($customer, 'getId') ? $customer->getId() : '';

            if (method_exists($customer, 'getDefaultBillingAddress')) {
                $customerAddress    = $customer->getDefaultBillingAddress();
            } else {
                $customerAddresses  = $customer->getAddresses();
                $customerAddress    = end($customerAddresses);
            }

            $street   = '';
            $postcode = '';
            $city     = '';
            $country  = '';

            if (!empty($customerAddress)) {
                $street = method_exists($customerAddress, 'getStreet')
                    ? $customerAddress->getStreet()
                    : $street;
                $postcode = method_exists($customerAddress, 'getPostcode')
                    ? $customerAddress->getPostcode()
                    : $postcode;
                $city = method_exists($customerAddress, 'getCity')
                    ? $customerAddress->getCity()
                    : $city;
                $country = method_exists($customerAddress, 'getCountryId')
                        ? $this->processingDataHelper->getCountryAsISO3166($customerAddress->getCountryId())
                        : (method_exists($customerAddress, 'getCountry') ? $customerAddress->getCountry() : '');

                $phone = (empty($phone) && method_exists($customerAddress, 'getTelephone'))
                    ? $customerAddress->getTelephone()
                    : $phone;

                $fax = (empty($fax) && method_exists($customerAddress, 'getFax'))
                    ? $customerAddress->getFax()
                    : $fax;

                $company = (empty($company) && method_exists($customerAddress, 'getCompany'))
                    ? $customerAddress->getCompany()
                    : $company;
            }

            $dob = ($customer->getDob())
                ? $customer->getDob()
                : null;

            $createdOn = method_exists($customer, 'getCreatedAt')
                ? $customer->getCreatedAt()
                : time();

            $this->Contact->getAddress()
                ->setStreetAddress($street)
                ->setZipCode($postcode)
                ->setCity($city)
                ->setCountry($country)
                ->setProvince('');

            $this->Contact
                ->getOptions()
                ->setCreatedOn($createdOn)
                ->setLang($this->getCurrentLocale());

            $this->Contact
                ->setEmail($customer->getEmail())
                ->setName([
                    $customer->getFirstname(),
                    $customer->getMiddlename(),
                    $customer->getLastname()
                ])
                ->setPhone($phone)
                ->setFax($fax)
                ->setCompany($company)
                ->setExternalId($externalId)
                ->setBirthday($dob);

            $group = $this->groupRepository->getById($customer->getGroupId());
            $props = $this->Contact->getProperties();
            $this->Contact->setProperties(
                $props->setItems(
                    array_merge(
                        $this->metaDataHelper->getContactMeta($hookName),
                        [
                            self::ID => $group->getId(),
                            self::CODE => $group->getCode(),
                            self::TAX_CLASS_ID => $group->getTaxClassId(),
                            self::TAX_CLASS_NAME => $group->getTaxClassName()
                        ]
                    )
                )
            );

            $this->setContactOptStatus();
            return $this->Contact;

        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Get contact by customer billing address
     *
     * @param object $billingAddress magento billing addess
     * @param string $customerEmail - contact email
     * @param string $hookName
     * @return SmContact
     * @throws Exception
     */
    public function getContactByCustomerBillingAddress($billingAddress, $customerEmail, $hookName = '')
    {
        $this->Contact = clone $this->cContact;
         $this->Contact
            ->setName(
                [
                    $billingAddress->getFirstname(),
                    $billingAddress->getLastname()
                ]
            )->setEmail($customerEmail)
            ->setPhone($billingAddress->getTelephone())
            ->setFax($billingAddress->getFax())
            ->setCompany($billingAddress->getCompany());

        $this->Contact->getAddress()
            ->setStreetAddress($billingAddress->getStreet())
            ->setCountry($this->processingDataHelper->getCountryAsISO3166($billingAddress->getCountryId()))
            ->setProvince($billingAddress->getRegion())
            ->setZipCode($billingAddress->getPostCode())
            ->setCity($billingAddress->getCity());

        $this->Contact->getOptions()
            ->setCreatedOn(date('c'));

        $props = $this->Contact->getProperties();
        $this->Contact->setProperties($props->setItems($this->metaDataHelper->getContactMeta($hookName)));

        $this->setContactOptStatus();
        return $this->Contact;
    }

    /**
     * Get contact by subscriber
     *
     * @param mixed $Subscriber
     * @param bool $newsletterHook
     * @param string $hookName
     * @return SmContact
     * @throws Exception
     */
    public function getContactBySubscriber($Subscriber, $newsletterHook = false, $hookName = '')
    {
        $this->Contact        = clone $this->cContact;
        $this->newsletterHook = $newsletterHook;
        $this->subscriber     = $Subscriber;

        $this->Contact
            ->setEmail($Subscriber->getEmail());

        $createOn = method_exists($this->subscriber, 'getCreatedAt')
            ? date("F j, Y, g:i a", $this->subscriber->getCreatedAt())
            : date("F j, Y, g:i a", time());

        $this->Contact->getOptions()
            ->setCreatedOn($createOn)
            ->setLang($this->getCurrentLocale());

        $this->setContactOptStatus();

        $props = $this->Contact->getProperties();
        $this->Contact->setProperties($props->setItems($this->metaDataHelper->getContactMeta($hookName)));

        return $this->Contact;
    }

    /**
     * Get country name by code
     *
     * @deprecated since v4.1.7
     * @see \Benhauer\Salesmanago\Helper\ProcessingData::getCountryAsISO3166()
     *
     * @param string $countryId country short code such EN, PL, DE, etc.;
     * @return string full country name;
     * @log Exception to magento log file;
     */
    protected function getCountryNameByCode($countryId)
    {
        try {
            if ($countryId == null) {
                return '';
            }

            return $this->countryInformation
                ->getCountryInfo($countryId)
                ->getFullNameLocale();
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Return current store locale
     *
     * @return false|string
     */
    protected function getCurrentLocale()
    {
        $currentLocaleCode = $this->localeResolver->getLocale();
        return strstr($currentLocaleCode, '_', true);
    }

    /**
     * Sets Opt States for Contact object base on magento subscriber status
     *
     * @return object $this->Contact;
     */
    protected function setContactOptStatus()
    {
        try {
            if ($this->newsletterHook
                && boolval($this->subscriber->getEmail())
                && boolval($this->subscriber->getStatus())
                && $this->subscriber->getStatus() == Subscriber::STATUS_SUBSCRIBED
            ) {
                $this->Contact
                    ->getOptions()
                    ->setIsSubscribes(true)
                    ->setIsUnSubscribes(false)
                    ->setIsSubscriptionStatusNoChange(false);
            } elseif ($this->newsletterHook
                && boolval($this->subscriber->getEmail())
                && boolval($this->subscriber->getStatus())
                && $this->subscriber->getStatus() == Subscriber::STATUS_UNSUBSCRIBED
            ) {
                $this->Contact
                    ->getOptions()
                    ->setIsUnSubscribes(true)
                    ->setIsSubscribes(false)
                    ->setIsSubscriptionStatusNoChange(false);
            } else {
                $this->Contact
                    ->getOptions()
                    ->setIsSubscribes(false)
                    ->setIsUnSubscribes(false)
                    ->setIsSubscriptionStatusNoChange(true);
            }

            //next one must prevent to send apiDoubleOptIn in another hooks as newsletter;
            if (!$this->newsletterHook) {
                Conf::getInstance()
                    ->getApiDoubleOptIn()
                    ->setEnabled(false);
            }

            return $this->Contact;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }
}
