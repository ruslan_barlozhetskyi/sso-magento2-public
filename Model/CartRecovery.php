<?php

namespace Benhauer\Salesmanago\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Model\AbstractModel as MagentoAbstractModel;
use Magento\Framework\Registry;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

use SALESmanago\Entity\Event\Event as EventEntity;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Helper\Conf;

class CartRecovery extends MagentoAbstractModel
{
    public const
        BASE_URL = 'salesmanago/events/cartrecovery';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * CartRecovery constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct(
            $context,
            $registry
        );

        $this->storeManager = $storeManager;
    }

    /**
     * Builds recovery cart url.
     *
     * Indexes for specific data - s, i, q
     *
     * @param EventEntity $Event
     * @param string $contactId
     * @return null|string
     */
    public function getRecoveryUrl(EventEntity $Event, $contactId = null)
    {
        $link = null;

        switch (Conf::getInstance()->getProductTypeId()) {
            case EventModel::$productIdDetail['sku']:
                $linkQuery = [
                    's' => $Event->getProducts(),
                    'i' => $Event->getDetail(3),
                    'q' => $Event->getDetail(2)
                ];
                break;
            default:
                $linkQuery = [
                    's' => $Event->getDetail(3),
                    'i' => $Event->getProducts(),
                    'q' => $Event->getDetail(2)
                ];
                break;
        }

        try {
            $link = $this->storeManager->getStore()->getUrl(self::BASE_URL);
            $link.= '?key=' . base64_encode(json_encode($linkQuery));
            $link.= (!empty($contactId)) ? '&smclient='.$contactId : '';
        } catch (NoSuchEntityException $e) {
            $this->_logger->error($e->getLogMessage());
        }

        return $link;
    }

    /**
     * Recover carts from url
     *
     * @param string $urlKeyParam
     * @return array
     * @todo The use of function base64_decode() is discouraged
     */
    public function recoverCartDataFromUrl($urlKeyParam)
    {
//@codingStandardsIgnoreStart
        $queryParams = json_decode(base64_decode($urlKeyParam), true);
//@codingStandardsIgnoreEnd
        return [
            'sku' => isset($queryParams['s']) ? explode(',', $queryParams['s']) : null,
            'qty' => isset($queryParams['q']) ? explode('/', $queryParams['q']) : null,
            'id'  => isset($queryParams['i']) ? explode(',', $queryParams['i']) : null
        ];
    }
}
