<?php
/**
 * @author Salesmanago Team
 * @copyright Copyright (c) 2021 Salesmanago (http://www.salesmanago.com)
 * @package Benhauer_Salesmanago
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Benhauer_Salesmanago',
    __DIR__
);
