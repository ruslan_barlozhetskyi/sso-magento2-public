<?php

namespace Benhauer\Salesmanago\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Sales\Model\ResourceModel\Order\Status\Collection as OrderStatusCollection;
use Benhauer\Salesmanago\Model\Export as SmExportModel;

class CustomEventExportTab extends Template
{
    /**
     * @var OrderStatusCollection
     */
    private $orderStatusCollection;

    /**
     * CustomEventExportTab constructor.
     *
     * @param Template\Context $context
     * @param OrderStatusCollection $orderStatusCollection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        OrderStatusCollection $orderStatusCollection,
        array $data = []
    ) {
        $this->orderStatusCollection = $orderStatusCollection;
        parent::__construct($context, $data);
    }

    /**
     * Block template.
     *
     * @var string
     */
    protected $_template = 'custom_event_export_tab.phtml';

    /**
     * Get order statuses array for view
     *
     * @return array
     */
    public function getOrderStatusArray()
    {
        return $this->orderStatusCollection->toOptionArray();
    }

    /**
     * Return default export order type
     *
     * @return array
     */
    public function getDefaultSMOrderProcessingType()
    {
        return SmExportModel::$exportOrderTypes;
    }
}
