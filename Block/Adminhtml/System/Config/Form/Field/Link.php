<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Link extends Field
{
    /**
     * Overwrite _getElementHtml
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);

        return sprintf(
            '<a href ="%s">%s</a>',
            rtrim($this->_urlBuilder->getUrl('salesmanago/config/updateOwnersList', ['store' => $storeId]), '/'),
            __('Update owners')
        );
    }
}
