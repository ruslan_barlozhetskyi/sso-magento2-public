<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ExportEventsButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Overwrite getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Export Purchases'),
            'class' => 'save primary',
            'title' => 'Exports orders with a specific state as PURCHASE events to SALESmanago',
            'on_click' => 'ExportHelper.buttonInitAndRun("exportEvents")',
            'sort_order' => 90
        ];
    }
}
