<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class LogoutButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Overwrite getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Logout'),
            'class' => 'action-secondary',
            'on_click' => sprintf("location.href = '%s';", $this->getLogoutUrl()),
            'sort_order' => 30
        ];
    }

    /**
     * Return logout url
     *
     * @return string
     */
    public function getLogoutUrl()
    {
        $storeId = $this->request->getParam('store');

        if ($storeId) {
            return $this->getUrl('*/*/logout/store/'.$storeId);
        }

        return $this->getUrl('*/*/logout');
    }
}
