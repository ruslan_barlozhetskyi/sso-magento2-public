<?php


namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Config\Block\System\Config\Tabs;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Model\Config\Structure;
use Magento\Backend\Helper\Data;
use Magento\Framework\Api\Filter;

class IndexMenu extends Tabs
{
    /**
     * Used template file
     *
     * @var string
     */
    protected $_template = 'Benhauer_Salesmanago::plugin_menu.phtml';

    /**
     * @var mixed
     */
    protected $tabs;

    /**
     * Store or store view id
     *
     * @var int
     */
    protected $store = 0;

    /**
     * IndexMenu constructor.
     *
     * @param Context $context
     * @param Structure $configStructure
     * @param Data $backendHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Structure $configStructure,
        Data $backendHelper,
        array $data = []
    ) {
        $this->_backendHelper = $backendHelper;

        parent::__construct(
            $context,
            $configStructure,
            $backendHelper,
            $data
        );

        $this->setId('salesmanago_config_tabs');
        $this->setTitle(__('Configuration '));
    }

    /**
     * Overwrite getMenuItems
     *
     * @return array[]
     */
    public function getMenuItems()
    {
        return [
            [
                'item' => 'index',
                'class' => '',
                'url' => '*/*/index',
                'label' => 'Welcome',
                'active' => false
            ],
            [
                'item' => 'export',
                'class' => '',
                'url' => '*/*/exportdata',
                'label' => 'Export Data',
                'active' => false
            ],
            [
                'item' => 'systemdebug',
                'class' => '',
                'url' => '*/*/systemdebug',
                'label' => 'System Debug',
                'active' => false
            ]
        ];
    }

    /**
     * Overwrite addFilterMethod
     *
     * @param Filter $filter
     * @return null
     */
    public function addFilter(Filter $filter)
    {
        return null;
    }

    /**
     * Overwrite isItemActive method
     *
     * @param array $item
     * @return bool
     */
    public function isItemActive($item)
    {
        if (strpos($this->getCurrentUrl(), str_replace('*/*', '', $item['url'])) !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Overwrite getUrl method
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        if ($store = $this->_request->getParam('store')) {
            $params = array_merge($params, ['store' => $store]);
        }

        return $this->_urlBuilder->getUrl($route, $params);
    }
}
