<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Api\Filter;

use Benhauer\Salesmanago\Model\Config as ModelConfig;
use \Benhauer\Salesmanago\Helper\Conf;

class WelcomeForm extends Generic
{
    /**
     * @var Store
     */
    protected $_systemStore;

    /**
     * @var mixed|string
     */
    public $pluginVersion = 'x.x.x';

    /**
     * @var null|string
     */
    public $clientId = null;

    /**
     * @var null|string
     */
    public $endpoint = null;

    /**
     * WelcomeForm constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param ModelConfig $modelConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        ModelConfig $modelConfig,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $modelConfig->updatePluginVersion();
        $this->pluginVersion = Conf::getInstance()->getVersionOfIntegration();

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('welcome_form');
        $this->setTitle(__('Welcome'));
        $this->getAccountTypeData();
    }

    /**
     * Sets account data
     */
    public function getAccountTypeData()
    {
        $this->clientId = Conf::getInstance()->getClientId();
        $this->endpoint = Conf::getInstance()->getEndpoint();
    }

    /**
     * Overwrite filter method
     *
     * @param Filter $filter
     * @return null
     */
    public function addFilter(Filter $filter)
    {
        return null;
    }
}
