<?php
namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class ResetButton
 */
class LoginCreateButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Overwrite getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Go to Create Account'),
            'class' => 'reset',
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'sort_order' => 30
        ];
    }

    /**
     * Overwrite getBackUrl()
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/create');
    }
}
