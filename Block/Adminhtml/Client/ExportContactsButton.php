<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ExportContactsButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Overwrite getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Export Contacts'),
            'class' => 'save primary',
            'title' => 'Exports registered customers, subscribers, data from guest purchase as contacts to SALESmanago',
            'on_click' => 'ExportHelper.buttonInitAndRun("exportContacts")',
            'sort_order' => 90
        ];
    }
}
