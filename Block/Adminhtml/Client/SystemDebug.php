<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Backend\Block\Template;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Benhauer\Salesmanago\Helper\Data;
use \Exception;

class SystemDebug extends Template
{
    /**
     * @var File
     */
    private $driverFile;

    /**
     * @var DirectoryList
     */
    private $dir;

    /**
     * @var Data
     */
    private $data;

    /**
     * @param Template\Context $context
     * @param File $driverFile
     * @param DirectoryList $dir
     * @param Data $configurations
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        File $driverFile,
        DirectoryList $dir,
        Data $configurations,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );

        $this->driverFile = $driverFile;
        $this->dir = $dir;
        $this->data = $configurations;
    }

    /**
     * Return filesize for files in var/log/ folder by $fileName
     *
     * @param string $fileName
     * @return string
     */
    public function getFileSize($fileName)
    {
        try {
            $bytes = $this->driverFile->stat($this->dir->getPath('log') . '/' . $fileName)['size'];
            if ($bytes >= 1073741824) {
                $bytes = number_format($bytes / 1073741824, 2) . ' GB';
            } elseif ($bytes >= 1048576) {
                $bytes = number_format($bytes / 1048576, 2) . ' MB';
            } elseif ($bytes >= 1024) {
                $bytes = number_format($bytes / 1024, 2) . ' KB';
            } elseif ($bytes > 1) {
                $bytes = $bytes . ' bytes';
            } elseif ($bytes == 1) {
                $bytes = $bytes . ' byte';
            } else {
                $bytes = '0 bytes';
            }
            return $bytes;
        } catch (FileSystemException $exception) {
            $this->_logger->error($exception->getMessage());
            return '0 byte';
        } catch (\Exception $e) {
            $this->_logger->error($e->getTraceAsString());
            return '0 byte';
        }
    }

    /**
     * Return configurations for scopes od store
     *
     * @return string
     */
    public function getConfigurations()
    {
        return $this->data->getStoresWithConfigurations();
    }

    /**
     * Check if given filename exist
     *
     * @param string $fileName
     * @return bool
     */
    public function fileExistForProcessing($fileName)
    {
        try {
            $path = $this->dir->getPath('log') . '/' . $fileName;
            return $this->driverFile->isExists($path);
        } catch (FileSystemException $e) {
            $this->_logger->error($e->getTraceAsString());
            return false;
        }
    }

    /**
     * Returns all log files in log/ folder
     *
     * @return array
     */
    public function getLogFiles()
    {
        try {
            $filesPath = $this->driverFile->readDirectory($this->dir->getPath('log'));

            $files = [];
            foreach ($filesPath as $filePath) {
                $parts = explode('/', $filePath);
                $name = end($parts);

                $files[] = [
                    'name' => $name,
                    'size' => $this->getFileSize($name),
                    'type' => str_replace('.log', '', $name)
                ];
            }

            return $files;
        } catch (FileSystemException $e) {
            $this->_logger->error($e->getTraceAsString());
            return [];
        } catch (Exception $err) {
            $this->_logger->error($err->getTraceAsString());
            return [];
        }
    }
}
