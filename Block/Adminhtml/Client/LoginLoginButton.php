<?php
namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class LoginLoginButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Overwrite getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Login'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
