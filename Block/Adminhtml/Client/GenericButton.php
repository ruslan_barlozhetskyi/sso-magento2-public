<?php
namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Search\Controller\RegistryConstants;
use Magento\Framework\UrlInterface;
use Magento\Framework\Registry;
use Magento\Framework\App\Request\Http;
use Magento\Backend\Block\Widget\Context;

class GenericButton
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Http
     */
    protected $request;

    /**
     * GenericButton constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param Http $request
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Http $request
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
        $this->request = $request;
    }

    /**
     * Return the synonyms group Id.
     *
     * @return int|null
     */
    public function getId()
    {
        $contact = $this->registry->registry('contact');
        return $contact ? $contact->getId() : null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
