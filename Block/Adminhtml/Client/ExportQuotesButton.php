<?php

namespace Benhauer\Salesmanago\Block\Adminhtml\Client;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ExportQuotesButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Overwrite getButtonData
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Export Carts'),
            'class' => 'save primary',
            'title' => 'Exports quotes for registered customers as CARTs event to SALESmanago',
            'on_click' => 'ExportHelper.buttonInitAndRun("exportQuotes")',
            'sort_order' => 91
        ];
    }
}
