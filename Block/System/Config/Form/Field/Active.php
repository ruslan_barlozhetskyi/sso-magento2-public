<?php
namespace Benhauer\Salesmanago\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;
use Benhauer\Salesmanago\Model\Config;
use Benhauer\Salesmanago\Helper\Conf;

class Active extends Field
{
    /**
     * Overwrite _getElementHtml
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setData('readonly', 1);

        //this if is checks for active configuration:
        if (!empty($this->getConfigData(Config::$confPaths[Conf::CLIENT_ID]))
            || !empty($this->getConfigData(Config::$confPaths[Conf::LOCATION]))
        ) {
            $element->setData('value', 1);
        } else {
            $element->setData('value', 0);
        }

        return $element->getElementHtml();
    }
}
