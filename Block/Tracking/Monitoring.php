<?php

namespace Benhauer\Salesmanago\Block\Tracking;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Benhauer\Salesmanago\Helper\Conf;

use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Model\Config as ConfModel;

/**
 * Class Monitoring
 *
 * used for rendering monitoring code
 */
class Monitoring extends Template
{
    /**
     * @var string
     */
    public $trackingScript = '';

    /**
     * @var FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var ConfModel
     */
    public $confModel;

    /**
     * Monitoring constructor.
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param ConfModel $confModel
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        ConfModel $confModel
    ) {
        $data = [];
        parent::__construct($context, $data);
        $this->confModel = $confModel;
        $this->factoryOrganizer = $factoryOrganizer;
        $this->storeManager = $context->getStoreManager();
        $this->resolveStoreScopeForConfModel();
        $this->getMonitoringCode();
    }

    /**
     * Sets $this->trackingScript prepare monitoring script;
     *
     * @return string - monitoring scrip
     */
    public function getMonitoringCode()
    {
        if (Conf::getInstance()->getClientId()
            && !Conf::getInstance()->getDisableMonitoringCode()
        ) {
            $clientId = Conf::getInstance()
                ->getClientId();

            $endpoint = preg_replace("[(http|https)://]i", "", Conf::getInstance()->getEndpoint());

            $code = "<script>var _smid ='{$clientId}'; ";
            $code .= Conf::getInstance()->getBannersEnabled() ? " var _smbanners = true; " : "";
            $code .= Conf::getInstance()->getCustomJsEnabled() ? " var _smcustom = true; " : "";
            $code .= 'var _smclt = ' . Conf::getInstance()->getContactCookieTtl() . '; ';
            $code .= "
             (function(w, r, a, sm, s ) {
             w['SalesmanagoObject'] = r;
             w[r] = w[r] || function () {( w[r].q = w[r].q || [] ).push(arguments)};
             sm = document.createElement('script');
             sm.type = 'text/javascript'; sm.async = true; sm.src = a;
             s = document.getElementsByTagName('script')[0];
             s.parentNode.insertBefore(sm, s);
             })(window, 'sm', ('https:' == document.location.protocol ? 'https://' : 'http://')
             + '{$endpoint}/static/sm.js');</script>";

            $this->trackingScript = trim(preg_replace('/\s+/', ' ', $code));

            $this->trackingScript.= Conf::getInstance()->getPopupJsEnabled()
                ? "<script src='" . Conf::getInstance()->getEndpoint() . "/dynamic/{$clientId}/popups.js'></script>"
                : '';

            return $this->trackingScript;
        } else {
            return $this->trackingScript;
        }
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store
     */
    protected function resolveStoreScopeForConfModel()
    {
        try {
            $scopeId = 0;
            $scope = 'default';

            $scopeId = $storeId = $this->storeManager->getStore()->getId() ?? $scopeId;
            $scopeId = (int) $scopeId;
            $scope = $scopeId === 0 ? $scope : ScopeInterface::SCOPE_STORES;

            $this->confModel
                ->setScopeId($scopeId)
                ->setScope($scope)
                ->getCachedConf();

        } catch (NoSuchEntityException  $e) {
            $this->_logger->error($e->getMessage());
        } catch (Exception $e) {
            $this->_logger->critical($e->getLogMessage());
        }
    }
}
