require(["jquery", "prototype", "mage/backend/tabs"],
    function ($) {
        $(document).ready(function () {
            $(document).ajaxStop(function () {
                let toggleParent = function (el) {
                    if (!el.parentElement.parentElement.hasAttribute("style")) {
                        el.parentElement.parentElement.setAttribute("style", "visibility:hidden");
                    } else if (el.parentElement.parentElement.getAttribute("style") == "visibility:hidden") {
                        el.parentElement.parentElement.setAttribute("style", "visibility:visible");
                    } else {
                        el.parentElement.parentElement.setAttribute("style", "visibility:hidden");
                    }
                }

                toggleParent(document.querySelector('input[name="client[endpoint]"]'));

                let elEnableCustomEndpoint = document.querySelector('input[name="client[enableCustomEndpoint]"]');
                elEnableCustomEndpoint.addEventListener("click", function () {
                    toggleParent(document.querySelector('input[name="client[endpoint]"]'));
                });
            });
        });
    }
);
