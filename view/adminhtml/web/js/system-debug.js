require(["jquery", "prototype", "mage/backend/tabs"],
    function ($) {
        $(document).ready(function () {
            let fieldRows = [
                document.getElementById("row_plugin_configurations"),
            ];
            fieldRows.forEach(function(item, index, array) {
                let td = document.createElement('div');
                td.classList.add('copy_button');

                let copyButton = document.createElement('input');
                let copyButtonTarget = 'plugin_configurations';

                copyButton.setAttribute('type', 'button');
                copyButton.setAttribute('name', 'copy_button');
                copyButton.setAttribute('id', 'copy_button');
                copyButton.setAttribute('class', 'copy_button');
                copyButton.setAttribute('value', 'copy');
                copyButton.setAttribute('target', copyButtonTarget);
                item.appendChild(copyButton);
            });

            $(".copy_button").on("click", function(e){
                let copyText = document.getElementById(e.target.getAttribute('target'));
                copyText.focus();
                copyText.select();
                document.execCommand('copy');
            });

            $(document).ajaxStop(function () {
                //run after ajax stops
            });
        });
    }
);
