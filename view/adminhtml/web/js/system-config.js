require(["jquery", "prototype", "mage/backend/tabs"],
    function ($) {
        $(document).ready(function () {
            let active = document.getElementById('salesmanago_account_settings_general_active');
            let activeRow = document.getElementById('row_salesmanago_account_settings_general_active');

            if (active === null) {
                active = document.getElementById('salesmanago_platform_settings_callbacks_active');
                activeRow = document.getElementById('row_salesmanago_platform_settings_callbacks_active');
            }

            if (active !== null) {
                active.style.display = 'none';
                activeRow.style.display = 'none';

                if (!Boolean(parseInt(active.value))) {
                    let sections = document.getElementById('config-edit-form').getElementsByClassName('section-config');

                    for (let i = 0; i < sections.length; i++) {
                        if (sections[i].contains(document.getElementById('salesmanago_account_settings_smattention-state'))
                            || sections[i].contains(document.getElementById('salesmanago_platform_settings_smattention-state'))
                        ) {
                            console.log('fine');
                        } else {
                            sections[i].style.display = 'none';
                        }
                    }
                } else {
                    let sections = document.getElementById('config-edit-form').getElementsByClassName('section-config');

                    for (let i = 0; i < sections.length; i++) {
                        if (sections[i].contains(document.getElementById('salesmanago_account_settings_smattention-state'))
                            || sections[i].contains(document.getElementById('salesmanago_platform_settings_smattention-state'))
                        ) {
                            sections[i].style.display = 'none';
                        }
                    }
                }
            }

            //add copy buttons to callbacks
            let fieldRows = [
                document.getElementById("row_salesmanago_platform_settings_callbacks_callbackOptIn"),
                document.getElementById("row_salesmanago_platform_settings_callbacks_callbackOptOut")
            ];

            //create copy button foreach field rows
            fieldRows.forEach(function(item, index, array) {
                if (item != null) {
                    let td = document.createElement('td');
                    td.classList.add('copy_button');

                    let copyButton = document.createElement('input');
                    let copyButtonTarget = item.querySelector("td.value input").getAttribute('id');

                    copyButton.setAttribute('type', 'button');
                    copyButton.setAttribute('name', 'copy_button');
                    copyButton.setAttribute('id', 'copy_button');
                    copyButton.setAttribute('class', 'copy_button');
                    copyButton.setAttribute('value', 'copy');
                    copyButton.setAttribute('target', copyButtonTarget);

                    let tdValue = item.querySelector("td.value");
                    tdValue.appendChild(copyButton);
                }
            });

            $(".copy_button").on("click", function(e){
                var copyText = document.getElementById(e.target.getAttribute('target'));
                copyText.focus();
                copyText.select();
                document.execCommand('copy');
            });

            $(document).ajaxStop(function () {
                //run after ajax stops
            });
        });
    }
);
