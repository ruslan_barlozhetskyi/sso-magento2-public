let Conf = {};
let exportType = '';

//used for send request:
const RequestHelper = {
    getChangeTypeToUrl: (urlWithType) => {
        let type = urlWithType.substring(
            urlWithType.indexOf("{") + 1,
            urlWithType.lastIndexOf("}")
        );

        if (!(type in REST_URLs)) {
            console.warn(type + ' - is not in : REST_URLs');
        }

        if (type === '' || type === null || type === 'undefined') {
            return urlWithType;
        }

        urlWithType = urlWithType.replace('{' + type + '}', REST_URLs[type]);

        return urlWithType;
    },
    doGetRequest: function (urlWithType, callbackSuccess = function(url){}, callbackFail = function(url){}, async = true) {
        let url = this.getChangeTypeToUrl(urlWithType);
        url = new URL(url).toString();

        let xhr = new XMLHttpRequest();

        xhr.open('GET', url, async);
        xhr.onload = function () {
            if (xhr.status === 200) {
                try {
                    callbackSuccess(JSON.parse(this.responseText));
                } catch (error) {
                    console.error(error);
                    callbackFail(urlWithType);
                }
            } else if (xhr.status > 200 && xhr.status <= 500 ) {
                callbackFail(urlWithType);
            }
        }
        xhr.send();
    },
    addGetParameter: function(key, value) {
        return '&'+key+'='+value;
    }
};

//used for queue request for exports:
const Queue = {
    elements: {},
    head: 0,
    tail: 0,
    addElements: function (elements) {
        this.reset();// always reset before add new data
        this.elements = elements;
        this.tail = elements.length;
        return this;
    },
    enqueue: function (element) {
        this.elements[this.tail] = element;
        this.tail++;
    },
    dequeue: function () {
        const item = this.elements[this.head];
        delete this.elements[this.head];
        this.head++;
        this.save();
        return item;
    },
    dequeueMany: function (nrOfElements = 1) {
        let items = [];

        if (this.getLength() === 0) {
            return items;
        }

        if (this.getLength() < nrOfElements) {
            nrOfElements = this.getLength();
        }

        while(nrOfElements > 0) {
            if (nrOfElements > this.getLength()) {
                nrOfElements = this.getLength()
            }

            nrOfElements--;
            let item = this.dequeue();

            if (item) {
                items.push(item);
            }
        }

        return items;
    },
    peek: function() {
        return this.elements[this.head];
    },
    getLength: function () {
        return this.tail - this.head;
    },
    isEmpty: function () {
        return this.length === 0;
    },
    reset: function () {
        this.elements = {};
        this.head = 0;
        this.tail = 0;
    },
    save: function () {
        let filteredElements = [];

        for (let i = 0; i < this.elements.length; i++) {
            let el = this.elements[i];

            if (el !== null && el !== 'undefined' && typeof el === 'object') {
                filteredElements.push(el);
            }
        }

        localStorage.setItem('smRequestsQueue', JSON.stringify(filteredElements));
        return this;
    },
    removeSave: () => {
        localStorage.setItem('smRequestsQueue', null);
    },
    getFromSave: () => {
        let localstorageVar = localStorage.getItem('smRequestsQueue');



        if (localstorageVar === null || localstorageVar === 'undefined') {
            return null;
        }

        this.elements = JSON.parse(localstorageVar);

        if (!this.elements.length) {
            return null;
        }

        return this.elements;
    }
};

//used for keep requests after get setup response from backend:
const RequestsBuffer = {
    storage: [],
    failedStorage: [],
    save: function () {
        localStorage.setItem('smRequestsBuffer', JSON.stringify(this.storage));
    },
    addRequest: function (url, async = true) {
        this.storage.push({
            url: url.toString(),
            async: async
        });
    },
    addBatchRequests: (queueElements) => {
        localStorage.setItem('smRequestsBuffer', JSON.stringify(queueElements));
        return this;
    },
    getStorage: function () {
        return this.storage;
    },
    loadFromLocalStorage: function () {
        try {
            this.storage = JSON.parse(localStorage.getItem('smRequestsBuffer'));
        } catch (error) {
            console.error(error);
            this.storage = [];
        }
        return this;
    },
    clearStorage: function () {
        this.storage = [];
    },
    clearLocalStorage: function () {
        localStorage.setItem('smRequestsBuffer', null);
    },
    clearAll: function() {
        this.clearStorage();
        this.clearLocalStorage();
    }
};

const ExportHelper = {
    resetClearAll: function() {
        Conf = {};
        exportType = '';
        View.packagesToExport(0);
        document.querySelector('#sm_export').value = 0;
        RequestsBuffer.clearAll();
        Queue.reset();
    },
    buttonRetryFailed: function () {
        BufferExport.setup().run();
        View.exportFailedForm.hide();
        View.exportFailedForm.reset();
    },
    buttonDiscardFailed: function() {
        this.resetClearAll();
        View.exportFailedForm.hide();
        View.pluginButtons.enable();
    },
    buttonRetryAbandoned: () => {
        BufferExport.setupFromQueueSave().run();
        View.retryAbandonedExport.hide();
        View.exportStatus.show();
        View.retryAbandonedExport.reset();
    },
    buttonDiscardAbandoned: () => {
        View.retryAbandonedExport.hide();
        View.pluginButtons.enable();
    },
    buttonInitAndRun: function (type) {
        this.resetClearAll();
        exportType = type;
        let urlWithQuery = REST_URLs.setupAjaxExport;

        const dates = View.getDates();

        Conf = {
            exportType: type,
            exportTag: View.exportTag(),
            dateFrom: dates.from,
            dateTo: dates.to,
            scopeId: View.scopeId(),
            orderTypes: View.getOrderTypesForExport()
        }

        urlWithQuery += Object.keys(Conf).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(Conf[k])
        }).join('&');

        RequestHelper.doGetRequest(
            urlWithQuery,
            this.callbackAfterSuccessExportSetup,
            this.callbackAfterFailedExportSetup
        );

        return false;
    },
    addRequestToBuffer: function (exportType, totalItemPackages, callbackUpdateLastExportedPackage = function() {}) {
        let totalPackages = totalItemPackages;

        while (totalPackages) {
            //need to regenerate url in case of changing lastExportedPackages for exported item
            let queryParams = Object.keys(Conf).map(function(k) {
                return encodeURIComponent(k) + '=' + encodeURIComponent(Conf[k])
            }).join('&');

            let requestUrl = '{'+exportType+'}' + queryParams;

            RequestsBuffer.addRequest(requestUrl);
            callbackUpdateLastExportedPackage();
            totalPackages--;
        }

        return false;
    },
    exportContacts: function () {
        const updateLastExportedPackageCustomers = function () {
            return Conf.lastExportedCustomersPackage++;
        };

        const updateLastExportedPackageGuestCustomers = function () {
            return Conf.lastExportedGuestCustomersPackage++;
        };

        const updateLastExportedPackageSubscribers = function () {
            return Conf.lastExportedSubscribersPackage++;
        };

        if (Conf.customersLoops) {
            ExportHelper.addRequestToBuffer(
                'customers',
                Conf.customersLoops,
                updateLastExportedPackageCustomers
            );
        }

        if (Conf.guestCustomersLoops) {
            ExportHelper.addRequestToBuffer(
                'guestCustomers',
                Conf.guestCustomersLoops,
                updateLastExportedPackageGuestCustomers
            );
        }

        if (Conf.subscriberLoops) {
            ExportHelper.addRequestToBuffer(
                'subscribers',
                Conf.subscriberLoops,
                updateLastExportedPackageSubscribers
            );
        }

        RequestsBuffer.save();
        BufferExport.setup().run();
        return true;
    },
    exportQuotes: function () {
        const updateLastExportedPackage = function () {
            Conf.lastExportedPackage++;
        };

        if (Conf.loops) {
            ExportHelper.addRequestToBuffer(
                'quotes',
                Conf.loops,
                updateLastExportedPackage
            );

            RequestsBuffer.save();
            BufferExport.setup().run();
            return true;
        }

        return false;
    },
    exportEvents: function () {
        const updateLastExportedPackage = function () {
            Conf.lastExportedPackage++;
        };

        if (Conf.loops) {
            ExportHelper.addRequestToBuffer(
                'orders',
                Conf.loops,
                updateLastExportedPackage
            );

            RequestsBuffer.save();
            BufferExport.setup().run();
            return true;
        }
        return false;
    },
    callbackAfterSuccessExportSetup: function (response) {
        Conf = response;

        View.resetNotifications();
        View.pluginButtons.toggle();
        View.resetExportedPackages(0);
        View.toggleExportBar().packagesToExport(Conf.loops);

        if (Conf.totalToExport === 0 && Conf.loops === 0) {
            View.toggleItems(true, 0);
            return true;
        }

        switch (exportType) {
            case 'exportContacts':
                ExportHelper.exportContacts();
                break;
            case 'exportQuotes':
                ExportHelper.exportQuotes();
                break;
            case 'exportEvents':
                ExportHelper.exportEvents();
                break;
        }

        return true;
    },
    callbackAfterFailedExportSetup: function () {
        console.warn('Failed Export Setup');
    },
    callbackAfterSuccessPackageExport: function (response) {
        Conf.loops--;

        View.increaseExportedPackages();

        if (Conf.loops === 0) {
            //all export loops have been finished
            View.toggleItems(true);
        }
    },
    callbackAfterFailedPackageExport: function () {
        console.error('Failed package export');
    }
};

const BufferExport = {
    defaultPackageSize: 2,
    successRequestsInPackage: 0,
    packagesToProcess: null,
    packagesProcessed: 0,
    failedUrls: 0,
    callbackSuccessExportOfOneRequest: function (response) {
        this.successRequestsInPackage++;
        ExportHelper.callbackAfterSuccessPackageExport(response);

        if (this.successRequestsInPackage === this.defaultPackageSize) {
            this.successRequestsInPackage = 0;
            this.run();
        }
    },
    callbackFailExportOfOneRequest: function (url) {
        this.failedUrls++;
        console.warn('Bad url: ' + url);

        RequestsBuffer.addRequest(url);
        RequestsBuffer.save()

        ExportHelper.callbackAfterFailedPackageExport();

        this.successRequestsInPackage++;
        if (this.successRequestsInPackage === this.defaultPackageSize) {
            this.successRequestsInPackage = 0;
            this.run();
        }
    },
    exportPackageOfRequests: function (packageOfRequests) {
        packageOfRequests.forEach(function (request) {
            RequestHelper.doGetRequest(
                request.url,
                function (response) {
                    BufferExport.callbackSuccessExportOfOneRequest(response);
                },
                function (url) {
                    BufferExport.callbackFailExportOfOneRequest(url);
                },
            );
        });
    },
    setup: function () {
        const requestsToProcess = RequestsBuffer.loadFromLocalStorage().getStorage();

        if (requestsToProcess == null) {
            console.warn('No requests in buffer');
            return false;
        }

        //add elements from buffer to queue:
        Queue.addElements(requestsToProcess);

        //clear buffer, now we will be working only with queue:
        RequestsBuffer.clearAll();

        this.packagesToProcess = Math.ceil(Queue.getLength() / this.defaultPackageSize);
        this.successRequestsInPackage = 0;
        this.packagesProcessed = 0;

        return this;
    },
    setupFromQueueSave: function () {
        const requestsToProcess = Queue.getFromSave();

        Conf = {
            "lastExportedCustomersPackage":0,
            "lastExportedSubscribersPackage":0,
            "lastExportedGuestCustomersPackage":0,
            "packageSize":1,
            "subscriberLoops":17,
            "guestCustomersLoops":11,
            "customersLoops":1,
            "dateFrom":946670461,
            "dateTo":1652148520,
            "loops":requestsToProcess.length,
            "startDate":"09.05.2022 06:08",
            "totalToExport":requestsToProcess.length,
            "lastExportedPackage":0,
            "scopeId":0,
            "exportTag":"MAGENTO_EXPORT"
        };

        if (requestsToProcess == null) {
            console.warn('No requests in Queue save');
            return false;
        }

        //add elements from buffer to queue:
        Queue.addElements(requestsToProcess);

        //clear buffer, now we will be working only with queue:
        RequestsBuffer.clearAll();

        this.packagesToProcess = Queue.getLength();
        this.successRequestsInPackage = 0;
        this.packagesProcessed = 0;
        View.packagesToExport(this.packagesToProcess);

        return this;
    },
    run: function () {
        if (this.packagesProcessed === this.packagesToProcess) {
            this.packagesProcessed = 0;
            return false;
        }

        this.exportPackageOfRequests(Queue.dequeueMany(this.defaultPackageSize));

        if (Queue.getLength() === 0 && this.failedUrls) {
            View.exportFailedForm.show(this.failedUrls);
            console.warn('You\'ve got ' + this.failedUrls + ' packages ');
        }

        this.packagesProcessed++;
    },
};

const View = {
    exportTag: function () {
        return document.querySelector('input[name="exportData[tagToExport]"]').value;
    },
    dateFrom: function () {
        return document.querySelector('input[name="exportData[exportDateFrom]"]').value;
    },
    dateTo: function () {
        return document.querySelector('input[name="exportData[exportDateTo]"]').value;
    },
    scopeId: function () {
        return document.querySelector('#store_switcher').value;
    },
    getDates: function () {
        let dateFrom = document.querySelector('input[name="exportData[exportDateFrom]"]').value;
        let dateTo = document.querySelector('input[name="exportData[exportDateTo]"]').value;

        if (dateFrom != "" && dateTo != "") {
            if (Date.parse(dateFrom) > Date.parse(dateTo)) {
                document.querySelector('input[name="exportData[exportDateFrom]"]').value = dateTo;
                document.querySelector('input[name="exportData[exportDateTo]"]').value = dateFrom;
                return {from: dateTo, to: dateFrom};
            }
        }

        return {from: dateFrom, to: dateTo};
    },
    toggleItems: function (isSuccess = false, emptyExport = 1) {
        const buttons = document.querySelectorAll('.page-actions-buttons button');
        const fieldset = document.querySelectorAll('.admin__fieldset');

        for (let i = 0; i < buttons.length; i++) {
            buttons[i].classList.toggle('disabled');
        }
        for (let i = 0; i < fieldset.length; i++) {
            fieldset[i].classList.toggle('disabled');
        }

        this.exportStatusNothing.hide();

        if (!isSuccess) {
            this.exportStatus.hide();
        } else if (emptyExport <= 0) {
            this.exportStatusNothing.show();
            this.exportStatus.hide();
        } else {
            this.exportStatus.succeed();
        }
    },
    toggleExportBar: function () {
        this.exportStatus.toggle();
        return this;
    },
    pluginButtons: {
        toggle: function () {
            const elements = document.querySelectorAll('.page-actions-buttons button, .admin__fieldset');
            for (let i = 0; i < elements.length; i++) {
                elements[i].classList.toggle('disabled');
            }
        },
        disable: function () {
            const elements = document.querySelectorAll('.page-actions-buttons button, .admin__fieldset');

            for (let i = 0; i < elements.length; i++) {
                elements[i].classList.add('disabled');
            }
        },
        enable: function () {
            const elements = document.querySelectorAll('.page-actions-buttons button, .admin__fieldset');

            for (let i = 0; i < elements.length; i++) {
                elements[i].classList.remove('disabled');
            }
        }
    },
    exportStatus: {
        toggle: function () {
            document.querySelector('.exportStatus').classList.toggle('hidden');
        },
        show: function () {
            document.querySelector('.exportStatus').classList.remove('hidden');
        },
        hide: function () {
            document.querySelector('.exportStatus').classList.add('hidden');
        },
        succeed: function () {
            document.querySelector('.exportStatus').classList.add('succeed');
        }
    },
    exportFailedForm: {
        show: function (numberOfFailed) {
            document.querySelector('.exportStatus').classList.toggle('hidden');
            document.querySelector('.exportFailedForm h2.notification .failed-export-packages-number').innerHTML = numberOfFailed;
            document.querySelector('.exportFailedForm').classList.remove('hidden');
            return this;
        },
        hide: function() {
            document.querySelector('.exportFailedForm').classList.add('hidden');
        },
        reset: function () {
            document.querySelector('.exportFailedForm h2.notification .failed-export-packages-number').innerHTML = 0;
        },
    },
    exportStatusNothing: {
        show: function () {
            document.querySelector('.exportStatusNothing').classList.remove('hidden');
        },
        hide: function () {
            document.querySelector('.exportStatusNothing').classList.add('hidden');
        }
    },
    retryAbandonedExport: {
        show: function (numberOfPackagesToExport) {
            document.getElementById('abandonedExport').classList.toggle('hidden');
            document.querySelector('.abandonedExport h2.notification .aborted-to-export-packages-number').innerHTML = numberOfPackagesToExport;
            document.getElementById('abandonedExport').classList.remove('hidden');
            return this;
        },
        hide: function() {
            document.getElementById('abandonedExport').classList.add('hidden');
        },
        reset: function () {
        },
    },
    packagesToExport: function(value = 0) {
        document.querySelector('[data-max]').innerHTML = value.toString();
        document.querySelector('#sm_export').max = value.toString();
        return this;
    },
    increaseExportedPackages: function () {
        let exportedPackages = parseInt(document.querySelector('[data-current]').innerHTML);
        exportedPackages++;

        document.querySelector('[data-current]').innerHTML = exportedPackages.toString();
        document.querySelector('#sm_export').value = exportedPackages.toString();
    },
    decreaseExportedPackages: function () {
        let exportedPackages = parseInt(document.querySelector('[data-current]').innerHTML);
        exportedPackages--;

        document.querySelector('[data-current]').innerHTML = exportedPackages.toString();
        document.querySelector('#sm_export').value = exportedPackages.toString();
    },
    resetExportedPackages: function () {
        let exportedPackages = 0;
        document.querySelector('[data-current]').innerHTML = exportedPackages.toString();
    },
    getOrderTypesForExport: function () {
        let orderTypesGetParams = [];

        document.querySelectorAll('.order-types-input.checked').forEach(function(el){
            orderTypesGetParams.push(el.value);
        });

        const customOrderTypes = document.querySelector('.order-types-custom-list');

        if (customOrderTypes != null) {
            orderTypesGetParams.push(customOrderTypes.value);
        }

        return orderTypesGetParams.join(',');
    },
    resetNotifications: function() {
        document.querySelector('.exportStatus').classList.add('hidden');
        document.querySelector('.exportStatusNothing').classList.add('hidden');
        document.querySelector('.abandonedExport').classList.add('hidden');
        document.querySelector('.exportStatus').classList.remove('succeed');
    },
};

(function() {
    const AbandonedExportCheck = () => {
        let savedItems = Queue.getFromSave();
        if (savedItems === null || savedItems.length === 0) {
            return false;
        }

        View.pluginButtons.disable();
        View.retryAbandonedExport.show(savedItems.length);
    }
    document.addEventListener("DOMContentLoaded", AbandonedExportCheck);
})();
