require([
        'jquery',
        'mage/translate',
        'jquery/validate'],
    function($){
        $.validator.addMethod(
            'validate-greater-than-0',
            function (v) {
                return (v > 0);
            }, $.mage.__('Field must be greater than 0')
        );

        $.validator.addMethod(
            'validate-less-than-8760',
            function (v) {
                return (v <= 8760);
            }, $.mage.__('Field must be less than 8760')
        );

        $.validator.addMethod(
            'validate-sm-location',
            function (v) {
                const reg = /^[a-zA-Z].[0-9a-zA-Z:./_-]+$/;
                return reg.test(v);
            }, $.mage.__('The field must be at least 3 characters long, start with a letter, and have a maximum length of 13 characters')
        );

        $.validator.addMethod(
            'validate-sm-double-optin-additional-fields',
            function (v) {
                let adoiFields = {
                    templateId: document.getElementById('salesmanago_account_settings_apiDoubleOptIn_apiDoubleOptInEmailTemplateId').value,
                    accountId: document.getElementById('salesmanago_account_settings_apiDoubleOptIn_apiDoubleOptInEmailAccountId').value,
                    emailSubject: document.getElementById('salesmanago_account_settings_apiDoubleOptIn_apiDoubleOptInEmailSubject').value
                }

                return ((adoiFields.templateId !== ''
                    && adoiFields.accountId !== ''
                    && adoiFields.emailSubject !== ''
                ) || (adoiFields.templateId === ''
                    && adoiFields.accountId === ''
                    && adoiFields.emailSubject === ''
                ));

            }, $.mage.__('Please fill in all or none of the additional fields in API Double Opt-in configuration')
        );
    }
);
