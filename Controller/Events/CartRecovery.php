<?php

namespace Benhauer\Salesmanago\Controller\Events;

use Psr\Log\LoggerInterface;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Store\Model\Store;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;

use Benhauer\Salesmanago\Helper\CookieManager;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Config;
use Benhauer\Salesmanago\Model\CartRecovery as CartRecoveryModel;

use SALESmanago\Exception\Exception;

class CartRecovery extends Action
{
    public const
        PAGE_CART_URL     = '/checkout/cart/',
        PAGE_PURCHASE_URL = '/checkout/';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepositoryInterface;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var CartManagementInterface
     */
    protected $quoteManagement;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var CartRecoveryModel
     */
    protected $cartRecoveryModel;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var CookieManager
     */
    protected $cookieManagerHelper;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Configurable
     */
    protected $configurableProduct;

    /**
     * @var StockItemRepository;
     */
    protected $stockItemRepository;

    /**
     * CartRecovery constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ProductRepositoryInterface $productRepository
     * @param CartRepositoryInterface $cartRepositoryInterface
     * @param Store $store
     * @param Session $session
     * @param ProductFactory $productFactory
     * @param CartManagementInterface $quoteManagement
     * @param CustomerSession $customerSession
     * @param CartRecoveryModel $cartRecoveryModel
     * @param LoggerInterface $logger
     * @param CookieManager $cookieManagerHelper
     * @param FormKey $formKey
     * @param Cart $cart
     * @param Product $product
     * @param Configurable $configurableProduct
     * @param StockItemRepository $stockItemRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ProductRepositoryInterface $productRepository,
        CartRepositoryInterface $cartRepositoryInterface,
        Store $store,
        Session $session,
        ProductFactory $productFactory,
        CartManagementInterface $quoteManagement,
        CustomerSession $customerSession,
        CartRecoveryModel $cartRecoveryModel,
        LoggerInterface $logger,
        CookieManager $cookieManagerHelper,
        FormKey $formKey,
        Cart $cart,
        Product $product,
        Configurable $configurableProduct,
        StockItemRepository $stockItemRepository
    ) {
        parent::__construct($context);
        $this->resultPageFactory       = $resultPageFactory;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->store                   = $store;
        $this->session                 = $session;
        $this->productFactory          = $productFactory;
        $this->quoteManagement         = $quoteManagement;
        $this->customerSession         = $customerSession;
        $this->cartRecoveryModel       = $cartRecoveryModel;
        $this->logger                  = $logger;
        $this->productRepository       = $productRepository;
        $this->cookieManagerHelper     = $cookieManagerHelper;
        $this->product                 = $product;
        $this->cart                    = $cart;
        $this->formKey                 = $formKey;
        $this->configurableProduct     = $configurableProduct;
        $this->stockItemRepository     = $stockItemRepository;
    }

    /**
     * Overwrite execute method
     *
     * @return ResultInterface|void|bool
     * @throws LocalizedException
     */
    public function execute()
    {
        try {
            $query = $this->getRequest()->getParam('key');

            $cartData = $this->cartRecoveryModel->recoverCartDataFromUrl($query);

            if (empty($cartData['sku'])) {
                return false;
            }

            if ($this->customerSession->isLoggedIn()) {
                $customer = $this->customerSession->getCustomer();
                $quoteId  = $this->quoteManagement->createEmptyCartForCustomer($customer->getId());
                $quote    = $this->cartRepositoryInterface->get($quoteId);

                $this->addProductsToCart($cartData);
            } else {
                $quote = $this->session->getQuote();
                $quote->setStore($this->store);
                $quote->setCurrency();

                $this->addProductsToCart($cartData);
                $quote->save();
            }
            $quote->collectTotals();
            $this->cartRepositoryInterface->save($quote);

            if (!empty($this->getRequest()->getParam('smclient'))) {
                $this->cookieManagerHelper->setCookie(
                    CookieManager::CLIENT_COOKIE,
                    $this->getRequest()->getParam('smclient')
                );
            }

            $redirectTo = self::PAGE_PURCHASE_URL;
            switch (Conf::getInstance()->getCartRecoveryRedirectTo()) {
                case Config::TO_CART_PAGE:
                    $redirectTo = self::PAGE_CART_URL;
                    break;
                case Config::TO_PURCHASE_SUMMARY:
                    $redirectTo = self::PAGE_PURCHASE_URL;
                    break;
            }

            $this->getResponse()->setRedirect($redirectTo);

        } catch (Exception $e) {
            $this->logger->critical($e->getLogMessage());
        }
    }

    /**
     * Method add products to cart based on cart data from event CART
     *
     * @param array $cartData
     * @return bool|void
     * @throws LocalizedException
     */
    protected function addProductsToCart($cartData)
    {
        foreach ($cartData['id'] as $key => $id) {
            $params = [
                'form_key' => $this->formKey->getFormKey(),
                'product' => $id,
                'qty' => $cartData['qty'][$key]
            ];

            if (!$this->checkItemAvailability($id, $cartData['qty'][$key])) {
                continue;
            }

            $product = $this->productFactory->create()->load($id);

            $prodData = $this->convertProductToEntity($product, $params);

            if ($prodData) {
                try {
                    $this->cart->addProduct($prodData['product'], $prodData['params']);
                } catch (LocalizedException $exception) {
                    $this->logger->alert($exception->getTraceAsString());
                    return false;
                }
            }
        }

        $this->cart->save();
    }

    /**
     * Return parent product object if exist for configurable product type;
     *
     * @param int $childProductId
     * @return null
     */
    public function getParentProductIfExist($childProductId)
    {
        $product = $this->configurableProduct->getParentIdsByChild($childProductId);

        if (!isset($product[0])) {
            return null;
        }

        return $this->productFactory
                ->create()
                ->load($product[0]);
    }

    /**
     * Converts product to entity with params
     *
     * @param mixed $product
     * @param mixed $params
     * @return array
     */
    protected function convertProductToEntity($product, $params)
    {
        $parentProduct = $this->getParentProductIfExist($product->getId());

        if ($parentProduct != null) {
            $options = [];
            $productAttributeOptions = $parentProduct
                    ->getTypeInstance(true)
                    ->getConfigurableAttributesAsArray($parentProduct);

            foreach ($productAttributeOptions as $option) {
                $options[$option['attribute_id']] =  $product->getData($option['attribute_code']);
            }

            $product = $parentProduct;//we need to show parent product with child product attributes
            $params['super_attribute'] = $options;
        }

        return [
            'product' => $product,
            'params' => $params
        ];
    }

    /**
     * Check if item could by added to cart by availability
     *
     * @param int $productId
     * @param int $qtyToRecover
     * @return false
     */
    protected function checkItemAvailability($productId, $qtyToRecover)
    {
        try {
            $stockItem = $this->stockItemRepository->get($productId);
            $actualCartItemQty = $this->getActualCartItemQtyByItemId($productId);

            $futureCartItemQty = $actualCartItemQty + $qtyToRecover;
            $futureInStockQty = $stockItem->getQty() - $futureCartItemQty;

            if ($stockItem->getQty() > 0
                && $stockItem->getIsInStock()
                && $stockItem->getMaxSaleQty() >= $futureCartItemQty
                && $futureInStockQty >= 0
            ) {
                return true;
            }
        } catch (NoSuchEntityException $e) {
            return false;
        }

        return false;
    }

    /**
     * Return product from cart if product exist
     *
     * @param int $productId
     * @return int|float
     */
    protected function getActualCartItemQtyByItemId($productId)
    {
        $cartItems = $this->cart->getItems();
        $cartItemData = [];

        if (empty($cartItems)) {
            return 0;
        }

        $cartItemsData = $cartItems->getData();

        if (empty($cartItemsData)) {
            return 0;
        }

        array_walk($cartItemsData, function ($value, $key) use ($productId, &$cartItemData) {
            if ($value['product_id']) {
                $cartItemData = $value;
            }
        });

        if (!empty($cartItemData)) {
            return $cartItemData['qty'] ?? 0;
        }

        return 0;
    }
}
