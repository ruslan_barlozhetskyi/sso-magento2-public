<?php

namespace Benhauer\Salesmanago\Controller\Newsletter;

use Magento\Newsletter\Model\Subscriber;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

use Benhauer\Salesmanago\Controller\Newsletter\Callback;

class In extends Callback
{
    /**
     * Set contact status to subscribed;
     *
     * @return ResponseInterface|Json|ResultInterface|void
     * @throws \Exception
     * @todo fix $this->_redirect()
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

        if ($this->isItTestCallback($params)) {
            return $this->response('optIn', true);
        }

        if (!$this->parametersChecker($params)) {
            return $this->response('', false, 'No check passed.');
        }

        $checkSubscriber = $this->subscriber->loadByEmail($params['email']);

        if (in_array(
            $checkSubscriber->getStatus(),
            [
                Subscriber::STATUS_NOT_ACTIVE,
                Subscriber::STATUS_UNCONFIRMED,
                Subscriber::STATUS_UNSUBSCRIBED
            ]
        )
        ) {
            $this->session->setProgrammaticallyNewsletterStatus(hash('haval128,5', $checkSubscriber->getEmail()));
            $checkSubscriber->setStatus(Subscriber::STATUS_SUBSCRIBED)->save();
            return $this->response('optIn', true);
        } elseif ($checkSubscriber->getEmail() == null) {
            return $this->response('', false, 'No subscriber.');
        }
//      @codingStandardsIgnoreStart
        $this->_redirect('/');
//      @codingStandardsIgnoreEnd
    }
}
