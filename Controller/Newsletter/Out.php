<?php

namespace Benhauer\Salesmanago\Controller\Newsletter;

class Out extends Callback
{
    /**
     * Set contact status to unsubscribed
     *
     * @todo fix $this->_redirect()
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

        if ($this->isItTestCallback($params)) {
            return $this->response('optOut', true);
        }

        if (!$this->parametersChecker($params)) {
            return $this->response('', false, 'No check passed.');
        }

        $checkSubscriber = $this->subscriber->loadByEmail($params['email']);

        if ($checkSubscriber->isSubscribed() || $checkSubscriber->getStatus() == 4) {
            $this->session->setProgrammaticallyNewsletterStatus(hash('haval128,5', $checkSubscriber->getEmail()));
            $checkSubscriber->unsubscribe();

            return $this->response('optOut', true);
        } elseif ($checkSubscriber->getEmail() == null) {
            return $this->response('', false, 'No subscriber.');
        }
//      @codingStandardsIgnoreStart
        $this->_redirect('/');
//      @codingStandardsIgnoreEnd
    }
}
