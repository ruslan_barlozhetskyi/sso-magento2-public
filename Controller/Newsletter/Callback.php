<?php

namespace Benhauer\Salesmanago\Controller\Newsletter;

use \Psr\Log\LoggerInterface;

use Magento\Checkout\Model\Session;
use Magento\Newsletter\Model\Subscriber;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

use Benhauer\Salesmanago\Model\Config;
use SALESmanago\Exception\Exception;
use Benhauer\Salesmanago\Helper\Conf;

abstract class Callback extends Action
{
    /**
     * @var PageFactory
     */
    public $pageFactory;

    /**
     * @var Subscriber
     */
    public $subscriber;

    /**
     * @var Config
     */
    public $confModel;

    /**
     * @var Json
     */
    public $rJsonFactory;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var int
     */
    protected $scopeId = 0;

    /**
     * @var string
     */
    protected $scope = 'default';

    public const
        TEST_MAIL = 'callback.test@salesmanago.com';

    /**
     * Callback constructor.
     *
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param JsonFactory $resultJsonFactory
     * @param Subscriber $subscriber
     * @param Session $session
     * @param StoreManagerInterface $storeManager
     * @param Config $confModel
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        JsonFactory $resultJsonFactory,
        Subscriber $subscriber,
        Session $session,
        StoreManagerInterface $storeManager,
        Config $confModel,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->pageFactory  = $pageFactory;
        $this->subscriber   = $subscriber;
        $this->rJsonFactory = $resultJsonFactory->create();
        $this->session      = $session;
        $this->storeManager = $storeManager;
        $this->confModel    = $confModel;
        $this->logger       = $logger;
    }

    /**
     * Abstract class to implement
     *
     * @return ResponseInterface|ResultInterface|void
     */
    abstract public function execute();

    /**
     * Check if request is valid
     *
     * @param array $params
     * @param int|null $scopeId
     * @return bool
     */
    protected function parametersChecker($params, $scopeId = null)
    {
        try {
            if (!isset($params['email']) && empty($params['email'])) {
                return false;
            }

            if (!isset($params['key']) || empty($params['email'])) {
                return false;
            }

            $this->resolveStoreScope($scopeId);
            $this->confModel->getCachedConf();

            $clientId = Conf::getInstance()->getClientId();
            $localSha = sha1($params['email'] . $clientId);

            if (($localSha != $params['key']) && $this->scopeId !== 0) {
                return $this->parametersChecker($params, 0);
            } elseif ($localSha == $params['key']) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->critical($e->getLogMessage());
        }
    }

    /**
     * Check if client send test callback
     *
     * @param array $params - request
     * @return boolean
     */
    protected function isItTestCallback($params)
    {
        if ($params['email'] == self::TEST_MAIL
            && $this->parametersChecker($params)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Custom callback response
     *
     * @param mixed $inOut
     * @param bool $success
     * @param mixed|null $info
     * @return Json
     */
    protected function response($inOut, $success = false, $info = null)
    {
        return $this->rJsonFactory->setData(
            [
                'success' => $success,
                'message' => ($success) ? __("Contact is {$inOut}") : $info
            ]
        );
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store
     *
     * @throws Exception
     * @param int|null $forceToScopeId - force change scope to chosen scope id
     */
    protected function resolveStoreScope($forceToScopeId = null)
    {
        try {
            $this->scopeId = $forceToScopeId ?? $this->scopeId;

            //set store id by top select option chosen
            $this->scopeId = $this->storeManager->getStore()->getId() ?? $this->scopeId;
            $this->scopeId = ($forceToScopeId !== null) ? $forceToScopeId : $this->scopeId;
            $this->scopeId = (int) $this->scopeId;
            $this->scope = $this->scopeId === 0 ? 'default' : ScopeInterface::SCOPE_STORES;

            $this->confModel
                ->setScopeId($this->scopeId)
                ->setScope($this->scope)
                ->getCachedConf();
        } catch (NoSuchEntityException $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
