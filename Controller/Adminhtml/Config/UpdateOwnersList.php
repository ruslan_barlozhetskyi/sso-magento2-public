<?php


namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Magento\Backend\Model\View\Result\Redirect;
use Psr\Log\LoggerInterface;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Api\Owners;
use Benhauer\Salesmanago\Helper\Conf;

use SALESmanago\Entity\User;
use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;

class UpdateOwnersList extends AbstractController
{

    /**
     * @var Owners
     */
    protected $apiOwnersModel;

    /**
     * UpdateOwnersList constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Owners $apiOwnersModel
     * @param Redirect $redirect
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Owners $apiOwnersModel,
        Redirect $redirect
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect
        );

        $this->apiOwnersModel = $apiOwnersModel;
    }

    /**
     * Overwrite execute method
     *
     * @return ResponseInterface|ResultInterface|void
     * @todo fix $this->_redirect(..)
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();

            Conf::getInstance()->setOwnersList(
                $this->apiOwnersModel->getList()
            );

            $this->confModel->saveOwnersList();

            $this->cacheManager
                ->clean($this->cacheManager->getAvailableTypes());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getViewMessage());
            $this->logger->critical($e->getTraceAsString());
        }
//      @codingStandardsIgnoreStart
        $this->_redirect('adminhtml/system_config/edit/index/smconfiguration');
//      @codingStandardsIgnoreEnd
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store
     *
     * @throws Exception
     */
    protected function resolveStoreScope()
    {
        //set store id by top select option chosen
        $scopeId = (int) $this->getRequest()->getParam('store');
        $scope = $scopeId === 0
            ? 'default'
            : ScopeInterface::SCOPE_STORES;

        $this->confModel
            ->setScopeId($scopeId)
            ->setScope($scope)
            ->getCachedConf();
    }
}
