<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

use SALESmanago\Exception\Exception;

class SystemDebug extends AbstractController
{
    /**
     * Overwrite execute
     *
     * @return ResponseInterface|ResultInterface
     * @todo fix $this->_view->loadLayout(), $this->_view->renderLayout()
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();

            if (!$this->checkIfConfExist() || !$this->confModel->checkIfLoadAppropriateConfByScopeId()) {
                return $this->redirectTo('login');
            }

//      @codingStandardsIgnoreStart
            $this->_view->loadLayout();
            $this->_view->renderLayout();
//      @codingStandardsIgnoreEnd
        } catch (Exception $e) {
            $this->logger->warning($e->getLogMessage());
        }
    }
}
