<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Benhauer\Salesmanago\Controller\Adminhtml\Exports\AbstractExport;
use Magento\Backend\Model\View\Result\Redirect;
use \Psr\Log\LoggerInterface;

use Magento\Backend\App\Action\Context;
use Magento\Newsletter\Model\Subscriber;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\App\ResponseInterface;

use SALESmanago\Controller\ExportController;
use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Entity\User;

use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Model\Export;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Exports\ExportQuotes as ExportQuotesModel;

/**
 * @deprecated - will be removed in v4.1.2 use Adminhtml/Exports controller instead
 * @see AbstractExport
 */
class ExportData extends AbstractController
{
    /**
     * @var FactoryOrganizer
     */
    protected $factoryOrganizer;

    /**
     * @var Subscriber
     */
    protected $subscriber;

    /**
     * @var CustomerCollectionFactory
     */
    protected $customerFactory;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderFactory;

    /**
     * @var ExportQuotesModel
     */
    protected $exportQuotesModel;

    /**
     * @var SubscriberCollectionFactory
     */
    public $subscriberFactory;

    /**
     * @var Export
     */
    public $exportModel;

    /**
     * @var Data
     */
    public $jsonHelper;

    /**
     * @var string
     */
    protected $exportType;

    /**
     * ExportData constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param User $user
     * @param JsonFactory $resultJsonFactory
     * @param Subscriber $subscriber
     * @param CustomerCollectionFactory $customerFactory
     * @param SubscriberCollectionFactory $subscriberFactory
     * @param OrderCollectionFactory $orderFactory
     * @param Export $exportModel
     * @param LoggerInterface $logger
     * @param Data $jsonHelper
     * @param ExportQuotesModel $exportQuotesModel
     * @param Redirect $redirect
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        User $user,
        JsonFactory $resultJsonFactory,
        Subscriber $subscriber,
        CustomerCollectionFactory $customerFactory,
        SubscriberCollectionFactory $subscriberFactory,
        OrderCollectionFactory $orderFactory,
        Export $exportModel,
        LoggerInterface $logger,
        Data $jsonHelper,
        ExportQuotesModel $exportQuotesModel,
        Redirect $redirect
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $user,
            $confModel,
            $cacheManager,
            $logger,
            $redirect
        );
        $this->factoryOrganizer  = $factoryOrganizer;
        $this->subscriber        = $subscriber;
        $this->customerFactory   = $customerFactory;
        $this->orderFactory      = $orderFactory;
        $this->logger            = $logger;
        $this->exportModel       = $exportModel;
        $this->subscriberFactory = $subscriberFactory;
        $this->jsonHelper        = $jsonHelper;
        $this->exportQuotesModel = $exportQuotesModel;
    }

    /**
     * Overwrite execute method
     *
     * @return ResponseInterface|ResultInterface|void
     * @throws Exception
     */
    public function execute()
    {
        $this->resolveStoreScope();

        if (!$this->checkIfConfExist()) {
            return $this->redirectTo('login');
        }

        $getData          = $this->getRequest()->getParam('getData');
        $exportStatus     = $this->getRequest()->getParam('exportStatus');
        $scopeId          = (int) $this->getRequest()->getParam('scopeId');
        $this->exportType = $this->getRequest()->getParam('exportType');

        if ($exportStatus) {
            try {
                return $this->exportData($this->exportType, $scopeId);
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        } elseif ($getData) {
            $this->exportModel->setOrdersTypesToExport($this->getRequest()->getParam('orderTypes'));

            $prepareDataToLocalStorage = $this->exportModel->setDataToExport(
                $this->getRequest()->getParams(),
                $scopeId
            );

            try {
                return $this->jsonResponse($prepareDataToLocalStorage);
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        } else {
            $this->renderView();
        }
    }

    /**
     * Export data base on export type
     *
     * @param string $type
     * @param int $scopeId
     * @return ResultInterface|void
     */
    public function exportData($type, $scopeId)
    {
        $currentPackageDetails = $this->getRequest()->getParams();

        switch ($type) {
            case Export::$exportTypes['exportContacts']:
                $dataToExportRaw = $this->exportModel->getCustomersCollection(
                    $currentPackageDetails,
                    $scopeId
                );
                $dataToExport = $this->exportModel->prepareContactsToExport(
                    $dataToExportRaw,
                    $this->subscriber,
                    $currentPackageDetails['exportTag']
                );
                break;

            case Export::$exportTypes['exportEvents']:
                $this->exportModel->setOrdersTypesToExport($this->getRequest()->getParam('orderTypes'));

                $dataToExportRaw = $this->exportModel->getOrdersCollection(
                    $currentPackageDetails,
                    $scopeId
                );
                $dataToExport = $this->exportModel->prepareEventsToExport(
                    $dataToExportRaw,
                    $this->subscriber
                );
                break;

            case ExportQuotesModel::$exportTypes['exportQuotes']:
                $quotesData = $this->exportQuotesModel->getQuotesData(
                    $currentPackageDetails,
                    $scopeId
                );
                $dataToExport = $this->exportQuotesModel->prepareQuotesToExport($quotesData);
                break;

            case Export::$exportTypes['exportSubscribers']:
                $dataToExportRaw = $this->exportModel->getSubscribersCollection(
                    $currentPackageDetails,
                    $scopeId
                );
                $dataToExport = $this->exportModel->prepareContactsToExport(
                    $dataToExportRaw,
                    $this->subscriber,
                    $currentPackageDetails['exportTag']
                );
                break;

            default:
                $this->logger->critical('Undefined export type');
                return;
        }

        $conf = Conf::getInstance();
        $exportController = new ExportController($conf);

        try {
            $response = $exportController->export($dataToExport);
        } catch (Exception $e) {
            $this->logger->critical($e->getViewMessage());
        }

        $this->getRequest()->setParams(['sm_response' => $response->getFields()]);
        return $this->jsonResponse($this->getRequest()->getParams());
    }

    /**
     * Rendering Magento view
     *
     * @todo fix $this->_view->loadLayout() and $this->_view->renderLayout()
     */
    protected function renderView()
    {
//      @codingStandardsIgnoreStart
        $this->_view->loadLayout();
        $this->_view->renderLayout();
//      @codingStandardsIgnoreEnd
    }

    /**
     * Create json response
     *
     * @param string $response
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
