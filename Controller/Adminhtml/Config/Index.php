<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

use SALESmanago\Entity\Configuration;
use SALESmanago\Exception\Exception;
use SALESmanago\Factories\ReportFactory;

class Index extends AbstractController
{
    /**
     * Overwrite execute
     *
     * @return ResponseInterface|ResultInterface
     * @todo fix $this->_view->loadLayout(), $this->_view->renderLayout()
     */
    public function execute()
    {
        try {
            if ($this->getRequest()->getParam('smconfiguration') == "true") {
//      @codingStandardsIgnoreStart
                return $this->_redirect('adminhtml/system_config/edit/section/salesmanago_account_settings');
//      @codingStandardsIgnoreEnd
            }

            $this->resolveStoreScope();

            if (!$this->checkIfConfExist()
                || !$this->confModel->checkIfLoadAppropriateConfByScopeId()
            ) {
                return $this->redirectTo('login');
            }

//      @codingStandardsIgnoreStart
            $this->_view->loadLayout();
            $this->_view->renderLayout();
//      @codingStandardsIgnoreEnd
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getLogMessage());
            $this->logger->warning($e->getLogMessage());
        }
    }
}
