<?php


namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Psr\Log\LoggerInterface;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Store\Model\ScopeInterface;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Entity\User;
use SALESmanago\Exception\Exception;

abstract class AbstractController extends Action
{
    /**
     * @var FactoryOrganizer
     */
    protected $factoryOrganizer;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var int
     */
    protected $scopeId = 0;

    /**
     * @var string
     */
    protected $scope = 'default';

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var ConfigModel
     */
    protected $confModel;

    /**
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * @var User
     */
    protected $User;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Redirect
     */
    protected $redirect;

    /**
     * AbstractController constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     */
    public function __construct(
        Action\Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect
    ) {
        parent::__construct($context);

        $this->factoryOrganizer = $factoryOrganizer;
        $this->messageManager   = $context->getMessageManager();
        $this->User             = $User;
        $this->confModel        = $confModel;
        $this->cacheManager     = $cacheManager;
        $this->logger           = $logger;
        $this->redirect         = $redirect;
    }

    /**
     * Set scopeId and Store Scope name to confModel
     *
     * @param bool
     * @return bool
     */
    protected function checkIfConfExist()
    {
        if (($this->scopeId || $this->scopeId === 0)
            && $this->confModel->checkIfLoadAppropriateConfByScopeId()
        ) {
            return true;
        }

        return false;
    }

    /**
     *  Set redirects to link
     *
     * @param string $redirectTo
     * @return ResponseInterface
     */
    protected function redirectTo($redirectTo = 'index')
    {
        $store = '';
        if (!empty($this->scopeId)) {
            $store = '/store/'.$this->scopeId ;
        }
//@codingStandardsIgnoreStart
        return $this->_redirect('*/*/'.$redirectTo.$store);
//@codingStandardsIgnoreEnd
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store to get configuration for particular scope
     *
     * @throws Exception
     */
    protected function resolveStoreScope()
    {
        //set store id by top select option chosen
        $this->scopeId = $this->getRequest()->getParam('store') ?? $this->scopeId;
        $this->scopeId = (int) $this->scopeId;
        $this->scope = $this->scopeId === 0 ? 'default' : ScopeInterface::SCOPE_STORES;

        $this->confModel
            ->setScopeId($this->scopeId)
            ->setScope($this->scope)
            ->getCachedConf();
    }
}
