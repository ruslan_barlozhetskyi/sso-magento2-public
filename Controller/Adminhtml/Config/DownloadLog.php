<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

use Magento\Framework\Filesystem\Driver\File;
use Psr\Log\LoggerInterface;
use SALESmanago\Entity\User;
use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList as AppFilesystemDirectoryList;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Exception\FileSystemException;

class DownloadLog extends AbstractController
{
    /**
     * @var File
     */
    private $driverFile;

    /**
     * @var DirectoryList
     */
    private $dir;

    /**
     * @var RawFactory
     */
    private $resultRawFactory;

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param RawFactory $resultRawFactory
     * @param FileFactory $fileFactory
     * @param File $driverFile
     * @param DirectoryList $dir
     */
    public function __construct(
        Context          $context,
        FactoryOrganizer $factoryOrganizer,
        User             $User,
        ConfigModel      $confModel,
        CacheManager     $cacheManager,
        LoggerInterface  $logger,
        Redirect         $redirect,
        RawFactory       $resultRawFactory,
        FileFactory      $fileFactory,
        File             $driverFile,
        DirectoryList    $dir
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect
        );

        $this->resultRawFactory = $resultRawFactory;
        $this->fileFactory = $fileFactory;
        $this->driverFile = $driverFile;
        $this->dir = $dir;
    }

    /**
     * Basic method
     *
     * @return ResponseInterface|Raw|ResultInterface|void
     * @throws FileSystemException
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();

            if (!$this->checkIfConfExist() || !$this->confModel->checkIfLoadAppropriateConfByScopeId()) {
                return $this->redirectTo('login');
            }

            $fileName = $this->_request->getParam('logType').'.log';

            $this->fileFactory->create(
                $fileName,
                null,
                AppFilesystemDirectoryList::MEDIA,
                'application/octet-stream',
                ''
            );

            $resultRaw = $this->resultRawFactory->create();

            $this->driverFile->isWritable($this->dir->getPath('log') . '/' . $fileName);

            $fileResource = $this->driverFile->fileOpen(
                $this->dir->getPath('log') . '/' . $fileName,
                'r'
            );

            $this->driverFile->fileSeek($fileResource, 1, SEEK_END);
            $endPos = $this->driverFile->fileTell($fileResource);
            $startPos = $endPos - 100000;
            $startPos = ($startPos < 0) ? 0 : $startPos;
            $length = $endPos - $startPos;
            $this->driverFile->fileSeek($fileResource, $startPos);
            $contents = $this->driverFile->fileRead($fileResource, $length);

            $searchFor = 'salesmanago';

            $pattern = preg_quote($searchFor, '/');
            $pattern = "/^.*$pattern.*\$/m";

            $grepedContent = '';
            if (preg_match_all($pattern, $contents, $matches)) {
                $grepedContent = implode("\n", $matches[0]);
            }

            $resultRaw->setContents($grepedContent);

            return $resultRaw;
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getLogMessage());
            $this->logger->warning($e->getLogMessage());
        } catch (\Exception $err) {
            $this->messageManager->addErrorMessage($err->getMessage());
            $this->logger->warning($err->getTraceAsString());
        }
    }
}
