<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use SALESmanago\Services\Report\ReportService;
use SALESmanago\Model\Report\ReportModel;

use SALESmanago\Exception\Exception;

class Logout extends AbstractController
{
    /**
     * Overwrite execute method
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();

            try {
                ReportService::getInstance(
                    $this->confModel->getCachedConf()
                )->reportAction(ReportModel::ACT_LOGOUT);
            } catch (\Exception $e) {
                $this->logger->warning($e->getMessage());
            }

            if ($this->confModel->removeConf()) {
                $this->cacheManager
                    ->clean($this->cacheManager->getAvailableTypes());

                $this->messageManager->addSuccessMessage(__("Success"));
                return $this->redirectTo('login');
            }

            $this->messageManager->addErrorMessage(__("Something went wrong"));
            $this->logger->warning('Something went wrong');
            return $this->redirectTo('index');
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getViewMessage());
            $this->logger->warning($exception->getLogMessage());
            $this->redirectTo('index');
        }
    }
}
