<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Config;

use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;

use \Benhauer\Salesmanago\Helper\Conf;

use \SALESmanago\Factories\FactoryOrganizer;
use \SALESmanago\Entity\User;
use \SALESmanago\Exception\Exception;

class Login extends AbstractController
{
    /**
     * Overwrite execute method
     *
     * @return bool|ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();

            if ($this->checkIfConfExist()) {
                return $this->redirectTo('index');
            }

            $client = $this->getRequest()->getParam('client');

            if (isset($client['username']) && isset($client['password'])) {
                $this->User
                    ->setEmail($client['username'])
                    ->setPass($client['password']);
            }
            
            if (!empty($client['endpoint'])) {
                Conf::getInstance()
                    ->setEndpoint($client['endpoint']);
            }

            if (boolval($this->User->getEmail())) {
                return $this->loginUser($this->User);
            }
//      @codingStandardsIgnoreStart
            $this->_view->loadLayout();
            $this->_view->renderLayout();
//      @codingStandardsIgnoreEnd            
        } catch (Exception $e) {
            $this->logger->warning($e->getLogMessage());
        }
    }

    /**
     * Action method of user login
     *
     * @param User $User
     * @return ResponseInterface
     * @todo fix $this->_redirect(..)
     */
    public function loginUser($User)
    {
        try {
            $loginAccount = $this->factoryOrganizer->getInst(
                FactoryOrganizer::LOGIN_C,
                Conf::getInstance()
            );

            $Response = $loginAccount->login($User);

            if ($Response->isSuccess()) {
                $this->confModel->saveConfAfterLogin();

                $this->cacheManager
                    ->clean($this->cacheManager->getAvailableTypes());

                $this->messageManager->addSuccessMessage(__("Success"));
                return $this->redirectTo('index');
            }
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getViewMessage());
            $this->logger->warning($e->getLogMessage());
//      @codingStandardsIgnoreStart            
            return $this->_redirect('*/*/login');
//      @codingStandardsIgnoreEnd            
        }

        $this->messageManager->addErrorMessage(__("Unknown error"));
        $this->logger->warning("Unknown error");
//      @codingStandardsIgnoreStart        
        return $this->_redirect('*/*/login');
//      @codingStandardsIgnoreEnd
    }
}
