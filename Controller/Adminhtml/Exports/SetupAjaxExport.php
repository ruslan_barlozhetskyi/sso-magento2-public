<?php


namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Psr\Log\LoggerInterface;

use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Controller\ResultInterface;

use SALESmanago\Entity\User;
use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Model\Exports\Export;
use Benhauer\Salesmanago\Controller\Adminhtml\AbstractJsonResponseController;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Exports\Customers as ModelExportsCustomers;
use Benhauer\Salesmanago\Model\Exports\Orders as ModelExportsOrders;
use Benhauer\Salesmanago\Model\Exports\Quotes as ModelExportsQuotes;
use Benhauer\Salesmanago\Model\Exports\GuestCustomers as ModelGuestCustomers;
use Benhauer\Salesmanago\Model\Exports\Subscribers as ModelExportsSubscribers;
use SALESmanago\Model\Report\ReportModel;
use SALESmanago\Services\Report\ReportService;

class SetupAjaxExport extends AbstractJsonResponseController
{
    /**
     * @var string[]
     */
    public static $exportTypes = [
        'exportSubscribers' => 'exportSubscribers',
        'exportContacts'    => 'exportContacts',
        'exportEvents'      => 'exportEvents',
        'exportQuotes'      => 'exportQuotes'
    ];

    /**
     * @var mixed
     */
    protected $exportType;

    /**
     * @var ModelExportsQuotes
     */
    protected $modelExportsQuotes;

    /**
     * @var ModelExportsOrders
     */
    protected $modelExportsOrders;

    /**
     * @var ModelExportsCustomers
     */
    protected $modelExportsCustomers;

    /**
     * @var ModelGuestCustomers
     */
    protected $modelExportsGuestCustomers;

    /**
     * @var ModelExportsSubscribers
     */
    protected $modelExportsSubscribers;

    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * SetupAjaxExport constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param ModelExportsCustomers $modelExportsCustomers
     * @param Data $jsonHelper
     * @param TimezoneInterface $timezone
     * @param ModelExportsOrders $modelExportsOrders
     * @param ModelExportsQuotes $modelExportsQuotes
     * @param ModelGuestCustomers $modelExportsGuestCustomers
     * @param ModelExportsSubscribers $modelExportsSubscribers
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        ModelExportsCustomers $modelExportsCustomers,
        Data $jsonHelper,
        TimezoneInterface $timezone,
        ModelExportsOrders $modelExportsOrders,
        ModelExportsQuotes $modelExportsQuotes,
        ModelGuestCustomers $modelExportsGuestCustomers,
        ModelExportsSubscribers $modelExportsSubscribers
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect,
            $jsonHelper
        );
        $this->timezone   = $timezone;
        $this->jsonHelper = $jsonHelper;

        $this->modelExportsCustomers      = $modelExportsCustomers;
        $this->modelExportsOrders         = $modelExportsOrders;
        $this->modelExportsQuotes         = $modelExportsQuotes;
        $this->modelExportsGuestCustomers = $modelExportsGuestCustomers;
        $this->modelExportsSubscribers    = $modelExportsSubscribers;
    }

    /**
     * Overwrite parent function
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $totalToExport         = 0;
        $totalPackagesToExport = 0;

        $scopeId          = (int) $this->getRequest()->getParam('scopeId');
        $this->exportType = $this->getRequest()->getParam('exportType');

        switch ($this->getRequest()->getParam('exportType')) {

            case self::$exportTypes['exportContacts']:
                $this->modelExportsCustomers->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    $scopeId
                );

                $this->modelExportsGuestCustomers->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    $scopeId
                );

                $this->modelExportsSubscribers->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    $scopeId
                );

                //get total items:
                $totalToExport = $this->modelExportsCustomers->getTotalCustomersToExport();
                $totalToExport+= $this->modelExportsSubscribers->getTotalSubscribersToExport();
                $totalToExport+= $this->modelExportsGuestCustomers->countTotalGuestCustomersToExport();

                //get total packages:
                $totalPackagesToExport = $this->modelExportsCustomers->getTotalCustomersPackagesToExport();
                $totalPackagesToExport+= $this->modelExportsSubscribers->getTotalSubscribersPackagesToExport();
                $totalPackagesToExport+= $this->modelExportsGuestCustomers->getTotalGuestCustomersPackagesToExport();

                $data['lastExportedCustomersPackage'] = 0;
                $data['lastExportedSubscribersPackage'] = 0;
                $data['lastExportedGuestCustomersPackage'] = 0;

                //doesn't matter until not changes in specific model:
                $data['packageSize']         = Export::EXP_PACKAGE_SIZE;
                $data['subscriberLoops']     = $this->modelExportsSubscribers->getTotalSubscribersPackagesToExport();
                $data['guestCustomersLoops'] = $this->modelExportsGuestCustomers
                    ->getTotalGuestCustomersPackagesToExport();
                $data['customersLoops']      = $this->modelExportsCustomers->getTotalCustomersPackagesToExport();

                //here doesn't matter which model will return date they all must have the same parent method
                $data['dateFrom']            = $this->modelExportsCustomers->getDateRangeFrom();
                $data['dateTo']              = $this->modelExportsCustomers->getDateRangeTo();
                break;

            case self::$exportTypes['exportEvents']:
                $this->modelExportsOrders->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    $scopeId,
                    $this->getRequest()->getParam('orderTypes')
                );

                $totalToExport         = $this->modelExportsOrders->getTotalOrdersToExport();
                $totalPackagesToExport = $this->modelExportsOrders->countTotalPackagesToExport();

                $data['packageSize'] = ModelExportsOrders::EXP_PACKAGE_SIZE;
                $data['dateFrom']    = $this->modelExportsOrders->getDateRangeFrom();
                $data['dateTo']      = $this->modelExportsOrders->getDateRangeTo();
                $data['orderTypes']  = (!empty($this->modelExportsOrders->getOrdersTypesToExport()))
                    ? implode(',', $this->modelExportsOrders->getOrdersTypesToExport())
                    : '';
                break;

            case self::$exportTypes['exportQuotes']:
                $this->modelExportsQuotes->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    $scopeId
                );

                $totalToExport         = $this->modelExportsQuotes->getTotalQuotesToExport();
                $totalPackagesToExport = $this->modelExportsQuotes->countTotalPackagesToExport();

                $data['packageSize'] = ModelExportsQuotes::EXP_PACKAGE_SIZE;
                $data['dateFrom']    = $this->modelExportsQuotes->getDateRangeFrom();
                $data['dateTo']      = $this->modelExportsQuotes->getDateRangeTo();
                break;

            default:
                $this->logger->critical('Undefined export type');
        }

        $data['loops']     = $totalPackagesToExport;
        $data['startDate'] = date('d.m.Y H:i', time());

        $data['totalToExport']       = $totalToExport;
        $data['lastExportedPackage'] = 0;
        $data['scopeId']             = $scopeId;
        $data['exportTag']           = $this->getRequest()->getParam('exportTag');

        try {
            ReportService::getInstance(
                $this->confModel->getCachedConf()
            )->reportAction(ReportModel::ACT_EXPORT, $data);
        } catch (\Exception $e) {
            $this->logger->warning($e->getMessage());
        }

        try {
            return $this->jsonResponse($data);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
