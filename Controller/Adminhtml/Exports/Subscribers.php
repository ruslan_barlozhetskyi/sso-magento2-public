<?php


namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Exports\Subscribers as ModelExportsSubscribers;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Json\Helper\Data;
use Psr\Log\LoggerInterface;
use SALESmanago\Entity\User;
use SALESmanago\Factories\FactoryOrganizer;

class Subscribers extends AbstractExport
{
    /**
     * @var ModelExportsSubscribers
     */
    protected $modelExportsSubscribers;

    /**
     * Customers constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $user
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param Data $jsonHelper
     * @param ModelExportsSubscribers $modelExportsSubscribers
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $user,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        Data $jsonHelper,
        ModelExportsSubscribers $modelExportsSubscribers
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $user,
            $confModel,
            $cacheManager,
            $logger,
            $redirect,
            $jsonHelper
        );

        $this->modelExportsSubscribers = $modelExportsSubscribers;
    }

    /**
     * Overwrite parent method
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $this->resolveStoreScope();//used to get right plugin/api configuration for scope

        $SubscribersCollection = $this->modelExportsSubscribers
            ->setUpExport(
                $this->getRequest()->getParam('dateFrom'),
                $this->getRequest()->getParam('dateTo'),
                (int)$this->getRequest()->getParam('scopeId')
            )->getSubscribersCollection(
                (int) $this->getRequest()->getParam('lastExportedSubscribersPackage')
            );

        $dataToExport = $this->modelExportsSubscribers->prepareSubscribersToExport(
            $SubscribersCollection,
            $this->getRequest()
                ->getParam('exportTag')
        );

        return $this->export($dataToExport);
    }
}
