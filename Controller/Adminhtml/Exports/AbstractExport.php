<?php


namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Magento\Framework\Controller\ResultInterface;

use Benhauer\Salesmanago\Controller\Adminhtml\AbstractJsonResponseController;
use Benhauer\Salesmanago\Helper\Conf;

use SALESmanago\Controller\ExportController;
use SALESmanago\Exception\Exception;
use SALESmanago\Model\Collections\Collection;

abstract class AbstractExport extends AbstractJsonResponseController
{
    /**
     * Overwrite parent method
     *
     * @param Collection $dataToExport
     * @return ResultInterface
     */
    public function export($dataToExport)
    {
        $exportController = new ExportController(
            Conf::getInstance()
        );

        try {
            $apiResponse = $exportController->export($dataToExport);

            $requestParams = $this->getRequest()->getParams();
            $requestParams['sm_response'] = $apiResponse->getFields();

            $this->getRequest()->setParams($requestParams);
        } catch (Exception $e) {
            $this->logger->critical($e->getViewMessage());
        }

        return $this->jsonResponse($this->getRequest()->getParams());
    }
}
