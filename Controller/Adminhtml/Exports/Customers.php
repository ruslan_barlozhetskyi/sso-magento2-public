<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Psr\Log\LoggerInterface;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Exports\Customers as ModelExportsCustomers;

use SALESmanago\Entity\User;
use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;

class Customers extends AbstractExport
{
    /**
     * @var ModelExportsCustomers
     */
    protected $modelExportsContacts;

    /**
     * Customers constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $user
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param Data $jsonHelper
     * @param ModelExportsCustomers $modelExportsContacts
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $user,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        Data $jsonHelper,
        ModelExportsCustomers $modelExportsContacts
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $user,
            $confModel,
            $cacheManager,
            $logger,
            $redirect,
            $jsonHelper
        );

        $this->modelExportsContacts = $modelExportsContacts;
    }

    /**
     * Overwrite basic controller method
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();//used to get right plugin/api configuration for scope

            $CustomersCollection = $this->modelExportsContacts
                ->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    (int) $this->getRequest()->getParam('scopeId')
                )->getCustomersCollection(
                    (int) $this->getRequest()->getParam('lastExportedCustomersPackage')
                );

            $dataToExport = $this->modelExportsContacts->prepareCustomersToExport(
                $CustomersCollection,
                $this->getRequest()
                    ->getParam('exportTag')
            );

            return $this->export($dataToExport);
        } catch (Exception $e) {
            $this->logger->error($e->getLogMessage());
            return $this->errorResponse($e->getTraceAsString());
        }
    }
}
