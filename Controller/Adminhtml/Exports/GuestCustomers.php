<?php


namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use SALESmanago\Exception\Exception;

use Psr\Log\LoggerInterface;

use SALESmanago\Entity\User;
use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Model\Exports\GuestCustomers as ModelGuestCustomers;

class GuestCustomers extends AbstractExport
{
    /**
     * @var ModelGuestCustomers
     */
    protected $modelExportsGuestContacts;

    /**
     * GuestCustomers constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param Data $jsonHelper
     * @param ModelGuestCustomers $modelExportsGuestContacts
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        Data $jsonHelper,
        ModelGuestCustomers $modelExportsGuestContacts
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect,
            $jsonHelper
        );

        $this->modelExportsGuestContacts = $modelExportsGuestContacts;
    }

    /**
     * Overwrite basic controller method
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {
            $this->resolveStoreScope();//used to get right plugin/api configuration for scope

            $GuestOrderCollection = $this->modelExportsGuestContacts
                ->setUpExport(
                    $this->getRequest()->getParam('dateFrom'),
                    $this->getRequest()->getParam('dateTo'),
                    (int)$this->getRequest()->getParam('scopeId')
                )->getGuestOrdersCollection(
                    (int) $this->getRequest()->getParam('lastExportedGuestCustomersPackage')
                );

            $dataToExport = $this->modelExportsGuestContacts->prepareCustomersToExport(
                $GuestOrderCollection,
                $this->getRequest()
                    ->getParam('exportTag')
            );

            return $this->export($dataToExport);
        } catch (Exception $e) {
            $this->logger->error($e->getLogMessage());
            return $this->errorResponse($e->getTraceAsString());
        }
    }
}
