<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Psr\Log\LoggerInterface;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Json\Helper\Data;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Exports\Quotes as ModelExportsQuotes;

use SALESmanago\Entity\User;
use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;

class Quotes extends AbstractExport
{
    /**
     * @var ModelExportsQuotes
     */
    protected $modelExportsQuotes;

    /**
     * Quotes constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param Data $jsonHelper
     * @param ModelExportsQuotes $modelExportsQuotes
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        Data $jsonHelper,
        ModelExportsQuotes $modelExportsQuotes
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect,
            $jsonHelper
        );

        $this->modelExportsQuotes = $modelExportsQuotes;
    }

    /**
     * Overwrite basic controller action method
     *
     * @return ResponseInterface|ResultInterface
     * @throws Exception
     */
    public function execute()
    {
        $this->resolveStoreScope();//used to get right plugin/api configuration for scope

        $quotesCollection = $this->modelExportsQuotes
            ->setUpExport(
                $this->getRequest()->getParam('dateFrom'),
                $this->getRequest()->getParam('dateTo'),
                (int) $this->getRequest()->getParam('scopeId')
            )->getQuotesCollection(
                (int) $this->getRequest()->getParam('lastExportedPackage')
            );

        $dataToExport = $this->modelExportsQuotes->prepareQuotesToExport(
            $this->modelExportsQuotes->getQuotesData($quotesCollection)
        );

        return $this->export($dataToExport);
    }
}
