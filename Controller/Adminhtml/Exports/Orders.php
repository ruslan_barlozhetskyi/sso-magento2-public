<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml\Exports;

use Psr\Log\LoggerInterface;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

use SALESmanago\Entity\User;
use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Exception\Exception;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Exports\Orders as ModelExportsOrders;

/**
 * Class Purchase
 */
class Orders extends AbstractExport
{
    /**
     * @var ModelExportsOrders
     */
    protected $modelExportsOrders;

    /**
     * Orders constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param Data $jsonHelper
     * @param ModelExportsOrders $modelExportsOrders
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        Data $jsonHelper,
        ModelExportsOrders $modelExportsOrders
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect,
            $jsonHelper
        );

        $this->modelExportsOrders = $modelExportsOrders;
    }

    /**
     * Overwrite basic controller action method
     *
     * @return ResponseInterface|ResultInterface
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute()
    {
        $this->resolveStoreScope();//used to get right plugin/api configuration for scope

        $OrdersCollection = $this->modelExportsOrders
            ->setUpExport(
                $this->getRequest()->getParam('dateFrom'),
                $this->getRequest()->getParam('dateTo'),
                (int) $this->getRequest()->getParam('scopeId'),
                $this->getRequest()->getParam('orderTypes')
            )->getOrdersCollection(
                (int) $this->getRequest()->getParam('lastExportedPackage')
            );

        $dataToExport = $this->modelExportsOrders->prepareEventsToExport($OrdersCollection);

        return $this->export($dataToExport);
    }
}
