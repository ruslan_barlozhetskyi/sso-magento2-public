<?php

namespace Benhauer\Salesmanago\Controller\Adminhtml;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Json\Helper\Data;

use Benhauer\Salesmanago\Controller\Adminhtml\Config\AbstractController;
use Psr\Log\LoggerInterface;
use SALESmanago\Entity\User;
use SALESmanago\Factories\FactoryOrganizer;

abstract class AbstractJsonResponseController extends AbstractController
{
    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * AbstractJsonResponseController constructor.
     *
     * @param Context $context
     * @param FactoryOrganizer $factoryOrganizer
     * @param User $User
     * @param ConfigModel $confModel
     * @param CacheManager $cacheManager
     * @param LoggerInterface $logger
     * @param Redirect $redirect
     * @param Data $jsonHelper
     */
    public function __construct(
        Context $context,
        FactoryOrganizer $factoryOrganizer,
        User $User,
        ConfigModel $confModel,
        CacheManager $cacheManager,
        LoggerInterface $logger,
        Redirect $redirect,
        Data $jsonHelper
    ) {
        parent::__construct(
            $context,
            $factoryOrganizer,
            $User,
            $confModel,
            $cacheManager,
            $logger,
            $redirect
        );

        $this->jsonHelper = $jsonHelper;
    }

    /**
     * Create json response
     *
     * @param array $response
     * @return ResultInterface
     */
    public function jsonResponse($response = [])
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    /**
     * Return error response
     *
     * @param string $error
     * @return ResultInterface
     */
    public function errorResponse($error = '')
    {
        return $this->jsonResponse(
            [
                'success' => false,
                'error' => $error
            ]
        );
    }
}
