<?php

namespace Benhauer\Salesmanago\Helper;

use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * This class is for keeping functions for processing data, for example changes locale code to specific type,
 * filtering strings, escapes specific characters, etc.
 */
class ProcessingData extends AbstractHelper
{
    /**
     * @var CountryInformationAcquirerInterface
     */
    protected $countryInformation;

    /**
     * @param Context $context
     * @param CountryInformationAcquirerInterface $countryInformation
     */
    public function __construct(
        Context $context,
        CountryInformationAcquirerInterface $countryInformation
    ) {
        $this->countryInformation = $countryInformation;
        parent::__construct($context);
    }

    /**
     * Get country in ISO3166 standard
     *
     * @param string $countryId
     * @return string - country code
     */
    public function getCountryAsISO3166($countryId)
    {
        try {
            if ($countryId == null) {
                return '';
            }

            return $this->countryInformation
                ->getCountryInfo($countryId)
                ->getTwoLetterAbbreviation();
        } catch (NoSuchEntityException $e) {
            $this->_logger->warning($e->getMessage());
            return '';
        }
    }
}
