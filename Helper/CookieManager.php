<?php

namespace Benhauer\Salesmanago\Helper;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;

use SALESmanago\Adapter\CookieManagerAdapter;

class CookieManager extends AbstractHelper implements CookieManagerAdapter
{
    public const ONE_DAY_IN_UNIX_TIMESTAMP = 86400;

    /**
     * @var PhpCookieManager
     */
    protected $cookieManager;

    /**
     * @var PublicCookieMetadata
     */
    protected $publicCookieMetadata;

    /**
     * @var Session
     */
    protected $session;

    /**
     * CookieManager constructor.
     *
     * @param Context $context
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $publicCookieMetadata
     * @param Session $session
     */
    public function __construct(
        Context $context,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $publicCookieMetadata,
        Session $session
    ) {
        parent::__construct($context);
        $this->cookieManager = $cookieManager;
        $this->publicCookieMetadata = $publicCookieMetadata;
        $this->session = $session;
    }

    /**
     * Set cookie method
     *
     * @param string $name
     * @param string $value
     * @param int $expiryInSeconds - seconds to expire od cookie
     * @param bool $httpOnly
     * @param string $path
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @todo replace  setcookie() with native magento2 methods
     */
    public function setCookie($name, $value, $expiryInSeconds = null, $httpOnly = false, $path = '/')
    {
        $expiry = ($expiryInSeconds == null) ? (3600 * self::ONE_DAY_IN_UNIX_TIMESTAMP) : $expiryInSeconds;

        $sessionMethod = $this->generateSessionMethodName($name, 'set');
        $this->session->$sessionMethod($value);
//@codingStandardsIgnoreStart
        //native methods got conflicts with salesmanago requirements
        setcookie($name, $value, time() + $expiry, $path);
//@codingStandardsIgnoreEnd
    }

    /**
     * Delete cookie method
     *
     * @param string $name
     * @return bool|void
     * @throws FailureToSendException
     * @throws InputException
     * @todo replce setcookie with native magento2 methods
     */
    public function deleteCookie($name)
    {
        $sessionMethod = $this->generateSessionMethodName($name, 'uns');
        $this->session->$sessionMethod($name);

        $this->cookieManager->deleteCookie(
            $name,
            $this->publicCookieMetadata
                ->setPath('/')
        );
    }

    /**
     * Get Cookie method
     *
     * @param string $name
     * @return mixed|string|null
     */
    public function getCookie($name)
    {
        return $this->cookieManager->getCookie($name);
    }

    /**
     * Method dynamicly ganerate set/get method for \Magento\Checkout\Model\Session;
     *
     * @param string $valueName
     * @param string $type - set, get, uns, has - all from Magento\Framework\Session\sessionManager::__call
     * @return string
     */
    protected function generateSessionMethodName($valueName, $type = 'set')
    {
        $valueName = ucfirst($valueName);
        return $type.$valueName;
    }
}
