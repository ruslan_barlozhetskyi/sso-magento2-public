<?php

namespace Benhauer\Salesmanago\Helper;

use SALESmanago\Entity\Configuration;

class Conf extends Configuration
{
    public const
        SCHEMA_VER                    = 'schemaVersion',
        OWNERS_LIST                   = 'ownersList',
        PRODUCT_TYPE_ID               = 'productTypeId',
        LOCATION                      = 'location',
        TAGS_NEWSLETTER               = 'tagsNewsletter',
        TAGS_REGISTER                 = 'tagsRegistration',
        TAGS_LOGIN                    = 'tagsLogin',
        TAGS_GUEST_PURCHASE           = 'tagsGuestPurchase',
        TAGS_PURCHASE                 = 'tagsPurchase',
        DISABLE_MONIT_CODE            = 'disableMonitoringCode',
        BANNERS_ENABLED               = 'bannersEnabled',
        CUSTOM_JS_ENABLED             = 'customJsEnabled',
        POPUP_JS_ENABLED              = 'PopupJsEnabled',
        STORE_DOMAIN                  = 'storeDomain',
        ACTIVE_SYNC                   = 'activeSynchronization',
        /*ADOI - Api Double OptIn:*/
        ADOI_ENABLED                  = 'ApiDoubleOptInEnabled',
        ADOI_SUBJECT                  = 'ApiDoubleOptInSubject',
        ADOI_TEMPLATE_ID              = 'ApiDoubleOptInTemplateId',
        ADOI_ACCOUNT_ID               = 'ApiDoubleOptInAccountId',
        CHECK_SCOPE_ID                = 'checkScopeId',
        CART_RECOVERY_REDIRECT_TO     = 'cartRecoveryRedirectTo',
        PURCHASE_VALUE_AS_GRAND_TOTAL = 'purchaseValueAsBaseGrandTotal',
        VERSION_OF_INTEGRATION        = 'versionOfIntegration',
        CALLBACK_OPTIN                = 'callbackOptIn',
        CALLBACK_OPTOUT               = 'callbackOptOut',
        PURCHASE_PROCESSING_TYPE      = 'purchaseProcessingType',
        PURCHASE_PROCESSING_EVENT     = 'purchaseProcessingEvent',
        PURCHASE_PROCESSING_TYPE_CRON = 'purchaseProcessingTypeCron',
        ATTENTION                     = 'attention';

    /**
     * Could be changed if switch will be existed
     * @var bool
     */
    protected $activeReporting = true;

    /**
     * @var string
     */
    protected $newsletterTags = 'magento2_newsletter';

    /**
     * @var string
     */
    protected $registrationTags = 'magento2_register';

    /**
     * @var string
     */
    protected $purchaseTags = 'magento2_purchase';

    /**
     * @var string
     */
    protected $guestPurchaseTags = 'magento2_guest_purchase';

    /**
     * @var string
     */
    protected $loginTags = 'magento2_login';

    /**
     * @var string
     */
    protected $storeDomain = '';

    /**
     * @var null
     */
    protected $setCheckScopeId = null;

    /**
     * @var bool
     */
    protected $bannersEnabled = false;

    /**
     * @var bool
     */
    protected $customJsEnabled = false;

    /**
     * @var bool
     */
    protected $popupJsEnabled = false;

    /**
     * @var string
     */
    protected $productTypeId = 'id';

    /**
     * @var bool
     */
    protected $disableMonitoringCode = false;

    /**
     * @var string
     */
    protected $purchaseProcessingType = '';

    /**
     * @var string
     */
    protected $purchaseProcessingTypeSystemEvents = 'checkout_onepage_controller_success_action';

    /**
     * @var int
     */
    protected $cookieTtl = 3653 * CookieManager::ONE_DAY_IN_UNIX_TIMESTAMP;//days * hours * min * sec

    /**
     * @var int -
     */
    protected $contactCookieTtl = 3653 * CookieManager::ONE_DAY_IN_UNIX_TIMESTAMP;//days

    /**
     * @var int
     */
    protected $eventCookieTtl = 12;//hours

    /**
     * @var int default cron cycle
     */
    protected $purchaseProcessingTypeCron = 3;

    /**
     * @var string 'purchase_summary' or 'cart'
     */
    protected $cartRecoveryRedirectTo = 'purchase_summary';

    /**
     * In purchase event set BaseGrandTotal as value
     *
     * @var bool
     */
    protected $purchaseValueAsBaseGrandTotal = false;

    /**
     * Sets cookie time to live
     *
     * @param int $param
     * @return Conf
     */
    public function setCookieTtl($param)
    {
        $param = (int) $param;
        return parent::setCookieTtl($param);
    }

    /**
     * Sets ttls for all contact cookies except smevent
     *
     * @param int $days
     * @return Conf
     */
    public function setContactCookieTtl($days)
    {
        $this->contactCookieTtl = $days * CookieManager::ONE_DAY_IN_UNIX_TIMESTAMP;
        return $this;
    }

    /**
     * Sets event cookie time to live
     *
     * @param int $hours
     * @return Conf
     */
    public function setEventCookieTtl($hours)
    {
        $this->eventCookieTtl = $hours * 3600;
        return $this;
    }

    /**
     * Sets redirect to after cart recovery
     *
     * @param string $param
     * @return $this;
     */
    public function setCartRecoveryRedirectTo($param): Conf
    {
        $this->cartRecoveryRedirectTo = $param;
        return $this;
    }

    /**
     * Return previously set cart redirection link
     *
     * @return string;
     */
    public function getCartRecoveryRedirectTo()
    {
        return $this->cartRecoveryRedirectTo;
    }

    /**
     * Set tags for newsletter
     *
     * @param string|null $param
     * @return $this;
     */
    public function setNewsletterTags($param): Conf
    {
        $this->newsletterTags = $param;
        return $this;
    }

    /**
     * Return newsletter tags
     *
     * @return string|null
     */
    public function getNewsletterTags()
    {
        return $this->newsletterTags;
    }

    /**
     * Set registration tags
     *
     * @param mixed $params
     * @return $this
     */
    public function setRegistrationTags($params): Conf
    {
        $this->registrationTags = $params;
        return $this;
    }

    /**
     * Return registration tags
     *
     * @return string|null
     */
    public function getRegistrationTags()
    {
        return $this->registrationTags;
    }

    /**
     * Return registration tags
     *
     * @return string|null
     */
    public function getLoginTags()
    {
        return $this->loginTags;
    }

    /**
     * Set registration tags
     *
     * @param mixed $params
     * @return $this
     */
    public function setLoginTags($params): Conf
    {
        $this->loginTags = $params;
        return $this;
    }

    /**
     * Set purchase tags
     *
     * @param string|null $param
     * @return $this
     */
    public function setPurchaseTags($param)
    {
        $this->purchaseTags = $param;
        return $this;
    }

    /**
     * Return purchase tags
     *
     * @return string|null
     */
    public function getPurchaseTags()
    {
        return $this->purchaseTags;
    }

    /**
     * Set guest tags
     *
     * @param string|null $param
     * @return $this
     */
    public function setGuestPurchaseTags($param): Conf
    {
        $this->guestPurchaseTags = $param;
        return $this;
    }

    /**
     * Return guest tags
     *
     * @return string|null
     */
    public function getGuestPurchaseTags()
    {
        return $this->guestPurchaseTags;
    }

    /**
     * Set store domain
     *
     * @param string|null $param
     * @return $this
     */
    public function setStoreDomain($param): Conf
    {
        $this->storeDomain = $param;
        return $this;
    }

    /**
     * Set scope id to check
     *
     * @param mixed $param
     * @return Conf
     */
    public function setCheckScopeId($param) : Conf
    {
        $this->setCheckScopeId = $param;
        return $this;
    }

    /**
     * Get checked scope id
     *
     * @return null|int
     */
    public function getCheckScopeId()
    {
        return $this->setCheckScopeId;
    }

    /**
     * Set banners enabled flag
     *
     * @param bool $param
     * @return $this
     */
    public function setBannersEnabled($param)
    {
        $this->bannersEnabled = $param;
        return $this;
    }

    /**
     * Return value for banners enabled flag
     *
     * @return bool
     */
    public function getBannersEnabled()
    {
        return $this->bannersEnabled;
    }

    /**
     * Set custom js enabled flag
     *
     * @param bool $param
     * @return $this
     */
    public function setCustomJsEnabled($param)
    {
        $this->customJsEnabled = $param;
        return $this;
    }

    /**
     * Get custom js enabled flag
     *
     * @return mixed
     */
    public function getCustomJsEnabled()
    {
        return $this->customJsEnabled;
    }

    /**
     * Set custom js enabled flag
     *
     * @param bool $param
     * @return $this
     */
    public function setPopupJsEnabled($param)
    {
        $this->popupJsEnabled = $param;
        return $this;
    }

    /**
     * Get custom js enabled flag
     *
     * @return mixed
     */
    public function getPopupJsEnabled()
    {
        return $this->popupJsEnabled;
    }

    /**
     * Set product type id
     *
     * @param bool $param
     * @return $this
     */
    public function setProductTypeId($param)
    {
        $this->productTypeId = $param;
        return $this;
    }

    /**
     * Get product type id
     *
     * @return mixed
     */
    public function getProductTypeId()
    {
        return $this->productTypeId;
    }

    /**
     * Set purchase processing type
     *
     * @param mixed $param
     * @return $this
     */
    public function setPurchaseProcessingType($param)
    {
        $this->purchaseProcessingType = $param;
        return $this;
    }

    /**
     * Get purchase processing type
     *
     * @return string
     */
    public function getPurchaseProcessingType()
    {
        return $this->purchaseProcessingType;
    }

    /**
     * Set purchase processing type system events
     *
     * @param mixed $param
     * @return $this
     */
    public function setPurchaseProcessingTypeSystemEvents($param)
    {
        $this->purchaseProcessingTypeSystemEvents = $param;
        return $this;
    }

    /**
     * Get purchase processing types system events
     *
     * @return string
     */
    public function getPurchaseProcessingTypeSystemEvents()
    {
        return $this->purchaseProcessingTypeSystemEvents;
    }

    /**
     * Set purchase processing type cron
     *
     * @param mixed $param
     * @return $this
     */
    public function setPurchaseProcessingTypeCron($param)
    {
        $this->purchaseProcessingTypeCron = $param;
        return $this;
    }

    /**
     * Get purchase processing type cron
     *
     * @return int
     */
    public function getPurchaseProcessingTypeCron()
    {
        return $this->purchaseProcessingTypeCron;
    }

    /**
     * Set disable monitoring code
     *
     * @param mixed $param
     * @return $this
     */
    public function setDisableMonitoringCode($param)
    {
        $this->disableMonitoringCode = (bool) $param;
        return $this;
    }

    /**
     * Get disable monitoring code
     *
     * @return bool
     */
    public function getDisableMonitoringCode()
    {
        return $this->disableMonitoringCode;
    }

    /**
     * Get Purchase Value As Base GrandTotal
     *
     * @param bool $bool
     * @return $this
     */
    public function setPurchaseValueAsBaseGrandTotal($bool)
    {
        $this->purchaseValueAsBaseGrandTotal = $bool;
        return $this;
    }

    /**
     * Get Purchase Value As Base GrandTotal
     *
     * @return bool
     */
    public function getPurchaseValueAsBaseGrandTotal()
    {
        return $this->purchaseValueAsBaseGrandTotal;
    }
}
