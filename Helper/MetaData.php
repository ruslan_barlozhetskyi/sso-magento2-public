<?php


namespace Benhauer\Salesmanago\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManager\ContextInterface;

class MetaData extends AbstractHelper
{

    /**
     * @var ContextInterface
     */
    public $context;

    /**
     * MetaData constructor.
     *
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
        $this->context = $context;
    }

    /**
     * Returns array for contact properties description
     *
     * @param string $wayOfUpsert - place from which contact is added;
     * @return array
     */
    public function getContactMeta($wayOfUpsert = 'default')
    {
        return [
            'lastUpsertTime' => time(),
            'wayOfUpsert'    => $wayOfUpsert
        ];
    }

    /**
     * Returns metadata for events
     *
     * @param string $eventPalace - place from which event is added;
     * @return array
     */
    public function getEventMetaData($eventPalace = 'default')
    {
        return [
            'metadata' => [
                'from' => 'exportEventsPurchase',
                'timeOfAction' => time()
            ]
        ];
    }
}
