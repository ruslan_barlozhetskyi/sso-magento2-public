<?php

namespace Benhauer\Salesmanago\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Framework\App\ProductMetadataInterface;

class Data extends AbstractHelper
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @param Context $context
     * @param ResourceConnection $resourceConnection
     * @param StoreRepositoryInterface $storeRepository
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        Context $context,
        ResourceConnection $resourceConnection,
        StoreRepositoryInterface $storeRepository,
        ProductMetadataInterface $productMetadata
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->storeRepository = $storeRepository;
        $this->productMetadata = $productMetadata;
        parent::__construct($context);
    }

    /**
     * Return plugin configuration items for specific scope id or for all scope ids if $scopeId==null
     *
     * @param null|int $scopeId
     * @return array
     */
    public function getAllConfigurations($scopeId = null)
    {
        $connection = $this->resourceConnection->getConnection();
        $table = $connection->getTableName('core_config_data');
//@codingStandardsIgnoreStart
        $query = "SELECT * FROM " . $table . " WHERE `path` LIKE '%salesmanago%' ";
//@codingStandardsIgnoreEnd
        if ($scopeId != null) {
            $query .= "AND `scope_id` = " . $scopeId;
        }

        return $connection->fetchAssoc($query);
    }

    /**
     * Gets platform information with configuration for salesmanago module
     *
     * @return false|string
     */
    public function getStoresWithConfigurations()
    {
        $stores = $this->storeRepository->getList();

        $result = [
            'basic-info' => [
                'php-version' => phpversion(),
                'platform' => $this->productMetadata->getName(),
                'platform-edition' => $this->productMetadata->getEdition(),
                'platform-version' => $this->productMetadata->getVersion()
            ],
            'stores' => []
        ];

        foreach ($stores as $store) {
            $result['stores'][] = [
                'store' => $store->getCode(),
                'storeData' => $store->getData(),
                'configuration' => $this->getAllConfigurations($store->getId())
            ];
        }

        return json_encode($result);
    }
}
