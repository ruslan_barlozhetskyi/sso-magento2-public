<?php

namespace Benhauer\Salesmanago\Observer;

use Benhauer\Salesmanago\Helper\Conf;
use Magento\Framework\Event\Observer;

use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;

use Salesmanago\Exception\Exception;

class CustomerLogin extends SalesmanagoAbstractObserver
{
    /**
     * Overwrite hookAct()
     *
     * @param Observer $observer
     * @return void
     */
    public function hookAct(Observer $observer)
    {
        try {
            $customerId = $observer->getCustomer()->getId();
            $Contact = $this->modelContact->getContactByCustomerId(
                $customerId,
                '',
                false,
                ($observer->getEvent() ? $observer->getEvent()->getName() : '')
            );

            $Contact->getOptions()
                ->setTags(Conf::getInstance()->getLoginTags())
                ->setTagScoring(false);

            $this->transferContact($Contact);
        } catch (Exception $e) {
            $this->logger->critical($e->getLogMessage());
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }
}
