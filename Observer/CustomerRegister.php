<?php

namespace Benhauer\Salesmanago\Observer;

use Benhauer\Salesmanago\Helper\CookieManager;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Contact as ContactModel;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Magento\Checkout\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Newsletter\Model\Subscriber;

use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;
use Benhauer\Salesmanago\Helper\Conf;

use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use SALESmanago\Entity\Event\Event;
use SALESmanago\Entity\Response;
use SALESmanago\Factories\FactoryOrganizer;
use Magento\Framework\Filesystem\Driver\File;

class CustomerRegister extends SalesmanagoAbstractObserver
{
    /**
     * Overwrite hookAct()
     *
     * @param Observer $observer
     * @return Observer|mixed|void
     */
    public function hookAct(Observer $observer)
    {
        try {
            $Contact = $this->modelContact->getContactByCustomerId(
                $observer->getCustomer()->getId(),
                '',
                false,
                ($observer->getEvent() ? $observer->getEvent()->getName() : '')
            );

            $Contact->getOptions()
                ->setTags(Conf::getInstance()->getRegistrationTags())
                ->setTagScoring(false);

            $Response = $this->transferContact($Contact);
            $this->synchronizeContactFromSm($Contact->getEmail(), $Response);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * Subscribe contact to magento newsletter if "syncRule" & optIn state constact in SALESmanago
     *
     * @param string $email
     * @param Response $Response
     * @return bool
     */
    protected function synchronizeContactFromSm($email, $Response)
    {
        try {
            $conf = $Response->getField('conf'); // $conf could be replaced with Conf::getInstance()
            $reqSync = $conf->getRequireSynchronization();

            if (!$reqSync) {
                return true;
            }

            $checkSubscriber = $this->subscriber->loadByEmail($email);

            if ($this->session->getLastSyncEmail()
                && ($this->session->getLastSyncEmail() == $email)
            ) {
                return true;
            }

            if ($checkSubscriber->isSubscribed()) {
                return true;
            }

            $this->session->setProgrammaticallyNewsletterStatus(hash('haval128,5', $checkSubscriber->getEmail()));

            if ($checkSubscriber->getOrigData() == null) {
                //next one subscribing customer which don't exist in subscribers:
                $checkSubscriber->subscribe($email);
                $checkSubscriber->setStatus(Subscriber::STATUS_SUBSCRIBED)->save();
            } else {
                //next one change status:
                $checkSubscriber->setStatus(Subscriber::STATUS_SUBSCRIBED)->save();
            }

            $this->session->setLastSyncEmail($email);
            return true;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
