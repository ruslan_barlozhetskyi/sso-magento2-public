<?php

namespace Benhauer\Salesmanago\Observer\Events\Purchase;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Checkout\Model\Session;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Exception\Exception;

use Benhauer\Salesmanago\Model\Contact;
use Benhauer\Salesmanago\Model\Config\PurchasesConfig;
use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Helper\CookieManager as CookieManagerHelper;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Helper\CookieManager;

abstract class AbstractPurchaseObserver extends SalesmanagoAbstractObserver
{
    /**
     * @var PurchasesConfig
     */
    public $purchasesConfigModel;

    /**
     * @var OrderRepositoryInterface
     */
    public $orderRepository;

    /**
     * @var QuoteFactory
     */
    public $quoteFactory;

    /**
     * @var CookieMetadataFactory
     */
    public $cookieMetadataFactory;

    /**
     * @var SessionManagerInterface
     */
    public $sessionManager;

    public const ORDER_ID_COOKIE_NAME = 'smorid';

    /**
     * @var int
     */
    public static $cookieExpiredTime = 30960000;

    /**
     * AbstractPurchaseObserver constructor.
     *
     * @param Session $session
     * @param StoreInterface $store
     * @param Subscriber $subscriber
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CookieManagerInterface $cookieManager
     * @param CookieManager $cookieManagerHelper
     * @param MagentoDateTime $objDate
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     * @param Contact $modelContact
     * @param EventModel $modelEvent
     * @param ConfigModel $confModel
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     * @param PurchasesConfig $purchasesConfigModel
     * @param OrderRepositoryInterface $orderRepository
     * @param QuoteFactory $quoteFactory
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Session $session,
        StoreInterface $store,
        Subscriber $subscriber,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CookieManagerInterface $cookieManager,
        CookieManagerHelper $cookieManagerHelper,
        MagentoDateTime $objDate,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger,
        Contact $modelContact,
        EventModel $modelEvent,
        ConfigModel $confModel,
        Event $Event,
        StoreManagerInterface $storeManager,
        PurchasesConfig $purchasesConfigModel,
        OrderRepositoryInterface $orderRepository,
        QuoteFactory $quoteFactory,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager
    ) {
        parent::__construct(
            $session,
            $store,
            $subscriber,
            $customerRepositoryInterface,
            $cookieManager,
            $cookieManagerHelper,
            $objDate,
            $factoryOrganizer,
            $logger,
            $modelContact,
            $modelEvent,
            $confModel,
            $Event,
            $storeManager
        );
        $this->purchasesConfigModel = $purchasesConfigModel;
        $this->orderRepository = $orderRepository;
        $this->quoteFactory = $quoteFactory;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
    }

    /**
     * Overwrite execute method
     *
     * @param Observer $observer
     * @return bool|void
     * @throws InputException
     * @throws FailureToSendException
     */
    public function execute(Observer $observer)
    {
        try {
            $this->resolveStoreScope();

            if ($this->purchasesConfigModel->getType() == PurchasesConfig::P_T_CRON) {
                $this->cookieManagerHelper->deleteCookie(CookieManagerHelper::EVENT_COOKIE);
            }

            if (!$this->purchasesConfigModel->isHookActive($observer->getEvent()->getName())) {
                return false;
            }

            $this->resolveStoreScope();

            if (Conf::getInstance()->getClientId() == null
                || Conf::getInstance()->isActive() == false
            ) {
                return true;
            }

            $this->hookAct($observer);
        } catch (Exception $exception) {
            $this->logger->critical($exception->getLogMessage());
        }
    }

    /**
     * Main method
     *
     * @param Observer $observer
     * @return mixed
     */
    public function hookAct(Observer $observer)
    {
        return $observer;
    }

    /**
     * Send PURCHASE event, remove smevent cookie, send contact
     *
     * @param mixed|null $order
     * @return bool
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     * @todo Direct throw of generic Exception is discouraged. Use context specific instead.
     * @todo Exceptions must not be handled in the same function where they are thrown.
     */
    public function executePurchase($order = null)
    {
        try {
            if (empty($order)) {
                return false;
            }

            $billingAddress = $order->getBillingAddress();
            $customerEmail = $billingAddress->getEmail();
            $smClientCookie = $this->cookieManagerHelper->getCookie(CookieManagerHelper::CLIENT_COOKIE);

            if ($order->getCustomerId() == null
                && $order->getCustomerIsGuest()
            ) {
                $Contact = $this->modelContact->getContactByCustomerBillingAddress($billingAddress, $customerEmail);

                $Contact->getOptions()
                    ->setTagScoring(true)
                    ->setTags(
                        [
                            Conf::getInstance()->getGuestPurchaseTags(),
                            Conf::getInstance()->getPurchaseTags()
                        ]
                    );

            } elseif ($order->getCustomerId() != null) {
                $customerId = $order->getCustomerId();
                $Contact = $this->modelContact->getContactByCustomerId($customerId);

                $Contact->getOptions()
                    ->setTagScoring(true)
                    ->setTags(Conf::getInstance()->getPurchaseTags());
            }

            $contactEmail = $Contact->getEmail();
            $Event = $this->modelEvent->getEventFromOrder($order);

            if (!empty($contactEmail)) {
                $Event->setEmail($Contact->getEmail());
            } elseif ($smClientCookie != null) {
                $Event->setContactId($smClientCookie);
            } else {
//@codingStandardsIgnoreStart  
                throw new Exception('No contact identificator for extEvent ');
//@codingStandardsIgnoreEnd                
            }

            $response = $this->transferBoth($Contact, $Event);

            $this->cookieManagerHelper->setCookie(
                CookieManagerHelper::CLIENT_COOKIE,
                $response->getField('contactId')
            );

            $this->cookieManagerHelper->deleteCookie(CookieManagerHelper::EVENT_COOKIE);

            return true;
//@codingStandardsIgnoreStart            
        } catch (Exception $e) {
                $this->logger->critical($e->getMessage());
        }
//@codingStandardsIgnoreEnd        
    }

    /**
     * Set order Id to cookie;
     *
     * @param string $value
     * @throws FailureToSendException
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     */
    protected function setOrderIdToCookie($value)
    {
        $this->session->setSmOrderId($value);

        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration(self::$cookieExpiredTime)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie(self::ORDER_ID_COOKIE_NAME, $value, $metadata);
    }

    /**
     * Gets order Id from cookie
     *
     * @return string|null
     */
    protected function getOrderIdFromCookie()
    {
        $sessionOrderId = $this->session->getSmOrderId();
        $cookieOrderId = $this->cookieManager->getCookie(self::ORDER_ID_COOKIE_NAME);

        return empty($sessionOrderId) ? $cookieOrderId : $sessionOrderId;
    }
}
