<?php

namespace Benhauer\Salesmanago\Observer\Events;

use Psr\Log\LoggerInterface;

use Magento\Checkout\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Framework\Exception\LocalizedException;
use Magento\Newsletter\Model\Subscriber;

use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\State;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Contact as ContactModel;
use Benhauer\Salesmanago\Model\Event as EventModel;
use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Helper\CookieManager;

class OrderStatusChanged extends SalesmanagoAbstractObserver
{
    /**
     * @var State
     */
    protected $state;

    /**
     * @var array
     */
    protected $processingOrdersStatus = [
        'canceled' => Event::EVENT_TYPE_CANCELLATION
    ];

    /**
     * OrderStatusChanged constructor.
     *
     * @param Session $session
     * @param StoreInterface $store
     * @param Subscriber $subscriber
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CookieManagerInterface $cookieManager
     * @param CookieManager $cookieManagerHelper
     * @param MagentoDateTime $objDate
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     * @param ContactModel $modelContact
     * @param EventModel $modelEvent
     * @param ConfigModel $confModel
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     * @param State $state
     */
    public function __construct(
        Session $session,
        StoreInterface $store,
        Subscriber $subscriber,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CookieManagerInterface $cookieManager,
        CookieManager $cookieManagerHelper,
        MagentoDateTime $objDate,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger,
        ContactModel $modelContact,
        EventModel $modelEvent,
        ConfigModel $confModel,
        Event $Event,
        StoreManagerInterface $storeManager,
        State $state
    ) {
        parent::__construct(
            $session,
            $store,
            $subscriber,
            $customerRepositoryInterface,
            $cookieManager,
            $cookieManagerHelper,
            $objDate,
            $factoryOrganizer,
            $logger,
            $modelContact,
            $modelEvent,
            $confModel,
            $Event,
            $storeManager
        );

        $this->state = $state;
    }

    /**
     * Overwrite hookAct
     *
     * @param Observer $observer
     * @return bool
     * @throws LocalizedException
     */
    public function hookAct(Observer $observer)
    {
        $order = $observer->getOrder();

        //prevent double purchase
        if ($this->state->getAreaCode() != 'adminhtml'
            && in_array($order->getStatus(), ['pending'])
        ) {
            return false;
        }

        // Only trigger when an order enters processing state.
        if ($eventType = $this->checkOrderStatusToProcess($order->getStatus())) {
            $this->sendExternalEventFromOrder($order, $eventType);
            return true;
        }

        return false;
    }

    /**
     * Sends external event based on order
     *
     * @param mixed $order - Magento Order
     * @param string $eventType - SALESmanago event type
     * @return bool|void
     */
    public function sendExternalEventFromOrder($order, $eventType = 'OTHER')
    {
        try {
            $billingAddress = $order->getBillingAddress();
            $customerEmail = $billingAddress->getEmail();

            if ($order->getCustomerId() == null
                && $order->getCustomerIsGuest()
            ) {
                $Contact = $this->modelContact->getContactByCustomerBillingAddress($billingAddress, $customerEmail);
                $Contact->getOptions()
                    ->setTagScoring(true)
                    ->setTags(
                        [
                            Conf::getInstance()->getPurchaseTags(),
                            Conf::getInstance()->getGuestPurchaseTags()
                        ]
                    );
            } elseif ($order->getCustomerId() != null) {
                $Contact = $this->modelContact->getContactByCustomerId($order->getCustomerId());

                $Contact->getOptions()
                    ->setTagScoring(true)
                    ->setTags(Conf::getInstance()->getPurchaseTags());
            }

            if (!isset($Contact)) {
                return true;
            }

            $Event = $this->modelEvent->getEventFromOrder($order);

            if ($Event instanceof Event) {
                $Event
                    ->setEmail($Contact->getEmail())
                    ->setContactId($Contact->getContactId())
                    ->setContactExtEventType($eventType);

                $this->transferBoth($Contact, $Event);

                if ($Event->getContactExtEventType() !== Event::EVENT_TYPE_OTHER) {
                    $this->setSentEvents($Event->getContactExtEventType(), $Event->getExternalId());
                }

                $this->cookieManagerHelper->deleteCookie(CookieManager::EVENT_COOKIE);
            } else {
                $this->transferContact($Contact);
                $this->logger->notice('Event for contact '. $Contact->getEmail() . ' was null');
            }

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * Sets to session check variable;
     *
     * @param string $eventType - SALESmanago Event Type
     * @param string $externalId - platform object id
     */
    protected function setSentEvents($eventType, $externalId)
    {
        $availableEvents = $this->session->getSentEvents();

        if ($availableEvents
            && array_key_exists($eventType, $availableEvents)
            && !in_array($externalId, $availableEvents)
        ) {
            array_push($availableEvents, $externalId);
        } elseif ($availableEvents
            && !array_key_exists($eventType, $availableEvents)
        ) {
            $availableEvents[$eventType] = $externalId;
        } else {
            $availableEvents = [$eventType => $externalId];
        }

        $this->session->setSentEvents($availableEvents);
    }

    /**
     * Checking processing order statuses for SALESmanago.
     * Returned false if SALESmanago don't processes order with $orderStatus.
     * Returned event type in case when SALESmanago processes that order type;
     *
     * @param string $orderStatus - Magento order status;
     * @return bool|mixed
     */
    protected function checkOrderStatusToProcess($orderStatus)
    {
        return (array_key_exists($orderStatus, $this->processingOrdersStatus))
            ? $this->processingOrdersStatus[$orderStatus]
            : false;
    }

    /**
     * Checks if particular event with sm event type from platform object was send;
     *
     * @param string $eventType - SALESmanago Event Type
     * @param mixed $externalId - platform object id
     * @return bool
     */
    protected function checkIfEventWasSent($eventType, $externalId)
    {
        $availableEvents = $this->session->getSentEvents();

        if ($availableEvents
            && array_key_exists($eventType, $availableEvents)
            && in_array($externalId, $availableEvents)
        ) {
            return true;
        }

        return false;
    }
}
