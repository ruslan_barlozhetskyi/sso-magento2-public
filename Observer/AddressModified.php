<?php

namespace Benhauer\Salesmanago\Observer;

use Magento\Framework\Event\Observer;
use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;

class AddressModified extends SalesmanagoAbstractObserver
{
    /**
     * Overwrite hook method
     *
     * @param Observer $observer
     * @return Observer|mixed|void
     */
    public function hookAct(Observer $observer)
    {
        try {
            $Contact = $this->modelContact->getContactByCustomerBillingAddress(
                $observer->getCustomerAddress(),
                $observer->getCustomerAddress()->getCustomer()->getEmail(),
                $observer->getEventName()
            );

            $this->transferContact($Contact);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
