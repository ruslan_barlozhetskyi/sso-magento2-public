<?php

namespace Benhauer\Salesmanago\Observer;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Framework\Event\Observer;
use Magento\Newsletter\Model\Subscriber;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerModelSession;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Model\Contact;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Helper\CookieManager as CookieManagerHelper;
use Benhauer\Salesmanago\Helper\CookieManager;
use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;
use Benhauer\Salesmanago\Model\Event as EventModel;

class CartModified extends SalesmanagoAbstractObserver
{
    /**
     * @var CustomerModelSession
     */
    public $customerSession;

    /**
     * @var CookieManagerInterface
     */
    public $cookieManager;

    /**
     * CartModified constructor.
     *
     * @param Session $session
     * @param StoreInterface $store
     * @param Subscriber $subscriber
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CustomerModelSession $customerSession
     * @param CookieManagerInterface $cookieManager
     * @param CookieManager $cookieManagerHelper
     * @param MagentoDateTime $objDate
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     * @param Contact $modelContact
     * @param EventModel $modelEvent
     * @param ConfigModel $confModel
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Session $session,
        StoreInterface $store,
        Subscriber $subscriber,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CustomerModelSession $customerSession,
        CookieManagerInterface $cookieManager,
        CookieManager $cookieManagerHelper,
        MagentoDateTime $objDate,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger,
        Contact $modelContact,
        EventModel $modelEvent,
        ConfigModel $confModel,
        Event $Event,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct(
            $session,
            $store,
            $subscriber,
            $customerRepositoryInterface,
            $cookieManager,
            $cookieManagerHelper,
            $objDate,
            $factoryOrganizer,
            $logger,
            $modelContact,
            $modelEvent,
            $confModel,
            $Event,
            $storeManager
        );
        $this->customerSession = $customerSession;
    }

    /**
     * Overwrite hookAct()
     *
     * @param Observer $observer
     * @return Observer|mixed
     */
    public function hookAct(Observer $observer)
    {
        try {
            $eventId = $this->cookieManagerHelper->getCookie(CookieManagerHelper::EVENT_COOKIE);
            $contactId = ($this->cookieManagerHelper->getCookie(CookieManagerHelper::CLIENT_COOKIE))
                ? ['contactId' => $this->cookieManagerHelper->getCookie(CookieManagerHelper::CLIENT_COOKIE)]
                : '';

            $Event = $this->modelEvent->getEventByCartModified(
                $observer,
                isset($contactId['contactId'])
                    ? $contactId['contactId']
                    : null
            );

            if (!$eventId) {
                $eventId = $this->session->getSmEventId();
            }

            if (isset($eventId)
                && !empty($eventId)
            ) {
                $Event->setEventId($eventId);
                $Response = $this->transferEvent($Event);
                return $Response->isSuccess();
            } else {
                $customerSession = $this->customerSession;

                if ($customerSession->isLoggedIn()) {
                    $Event->setEmail($customerSession->getCustomer()->getEmail());
                } else {
                    if (!empty($contactId)
                    ) {
                        $contactId = is_array($contactId)
                            ? $contactId['contactId']
                            : $contactId;

                        $Event->setContactId($contactId);
                    }
                }

                if ($Event->getEmail() || $Event->getContactId()) {
                    if ($Event->getEmail()) {
                        $Event->setForceOptIn(false);
                    }

                    $Response = $this->transferEvent($Event);

                    $this->cookieManagerHelper->setCookie(
                        CookieManager::EVENT_COOKIE,
                        $Response->getField('eventId'),
                        Conf::getInstance()->getEventCookieTtl()
                    );
                }
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
