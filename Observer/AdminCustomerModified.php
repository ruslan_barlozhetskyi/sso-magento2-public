<?php

namespace Benhauer\Salesmanago\Observer;

use Magento\Framework\Event\Observer;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;

use Benhauer\Salesmanago\Helper\Conf;

use SALESmanago\Exception\Exception;

class AdminCustomerModified extends CustomerModified
{
    /**
     * @var mixed
     */
    protected $customer;

    /**
     * Overwrite execute()
     *
     * @param Observer $observer
     * @return bool|void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->getCustomer($observer);
            $this->resolveStoreScope();

            if (empty(Conf::getInstance()->getClientId())
                && $this->scopeId === 0
            ) {
                return true;
            } elseif (empty(Conf::getInstance()->getClientId())) {
                //change config to get from 0
                $this->resolveStoreScope(0);
            }

            if ($observer->getEvent()->getName() != 'customer_register_success'
                && Conf::getInstance()->getActiveSynchronization()
            ) {
                Conf::getInstance()->setActiveSynchronization(false);
            }

            $this->hookAct($observer);
        } catch (Exception $e) {
            $this->logger->critical($e->getLogMessage());
        }
    }

    /**
     * Get customer from observer
     *
     * Resets scope by customer store id
     *
     * @param mixed $observer
     * @return CustomerInterface
     * @throws Exception
     */
    public function getCustomer($observer) : CustomerInterface
    {
        $this->customer = $observer->getCustomer();
        $this->resolveStoreScope();
        return $this->customer;
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store
     *
     * @param int $forceTo - scope id, force resole config by passed scope id;
     * @throws Exception
     */
    protected function resolveStoreScope($forceTo = null)
    {
        try {
            if ($forceTo !== null) {
                $this->scopeId = $forceTo;
            } else {
                //set store id by top select option chosen
                $this->scopeId = $this->customer->getStoreId() ?? $this->scopeId;
            }

            $this->scopeId = (int) $this->scopeId;
            $this->scope = $this->scopeId === 0
                ? 'default'
                : ScopeInterface::SCOPE_STORES;

            $this->confModel
                ->setScopeId($this->scopeId)
                ->setScope($this->scope)
                ->getCachedConf();

        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
