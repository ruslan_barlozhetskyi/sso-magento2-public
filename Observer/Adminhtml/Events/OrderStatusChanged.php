<?php

namespace Benhauer\Salesmanago\Observer\Adminhtml\Events;

use Magento\Framework\App\State;

use SALESmanago\Entity\Event\Event;

use Benhauer\Salesmanago\Observer\Events\OrderStatusChanged as FrontOrderStatusChanged;

class OrderStatusChanged extends FrontOrderStatusChanged
{

    /**
     * Here we just add pending to catch purchase event creation on admin side
     *
     * @var array
     */
    protected $processingOrdersStatus = [
        'canceled' => Event::EVENT_TYPE_CANCELLATION,
        'pending'  => Event::EVENT_TYPE_PURCHASE,
    ];
}
