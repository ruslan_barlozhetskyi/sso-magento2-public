<?php

namespace Benhauer\Salesmanago\Observer;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\UrlInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session as CustomerSession;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Entity\Contact\Contact;

use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Contact as ContactModel;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;
use Benhauer\Salesmanago\Helper\CookieManager;
use Benhauer\Salesmanago\Model\Event as EventModel;

class NewsletterStatusModified extends SalesmanagoAbstractObserver
{
    /**
     * @var CustomerSession
     */
    public $CustomerSession;

    /**
     * @var int
     */
    public static $counter = 0;

    /**
     * NewsletterStatusModified constructor.
     *
     * @param Session $session
     * @param StoreInterface $store
     * @param Subscriber $subscriber
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CookieManagerInterface $cookieManager
     * @param CookieManager $cookieManagerHelper
     * @param MagentoDateTime $objDate
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     * @param ContactModel $modelContact
     * @param ConfigModel $confModel
     * @param EventModel $modelEvent
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     * @param CustomerSession $CustomerSession
     */
    public function __construct(
        Session $session,
        StoreInterface $store,
        Subscriber $subscriber,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CookieManagerInterface $cookieManager,
        CookieManager $cookieManagerHelper,
        MagentoDateTime $objDate,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger,
        ContactModel $modelContact,
        ConfigModel $confModel,
        EventModel $modelEvent,
        Event $Event,
        StoreManagerInterface $storeManager,
        CustomerSession $CustomerSession
    ) {
        parent::__construct(
            $session,
            $store,
            $subscriber,
            $customerRepositoryInterface,
            $cookieManager,
            $cookieManagerHelper,
            $objDate,
            $factoryOrganizer,
            $logger,
            $modelContact,
            $modelEvent,
            $confModel,
            $Event,
            $storeManager
        );
        $this->CustomerSession = $CustomerSession;
    }

    /**
     * Overwrite hookAct()
     *
     * @param Observer $observer
     * @return bool|Observer|mixed
     */
    public function hookAct(Observer $observer)
    {
        try {
            if ($this->checkPreventUpsertsWithDoubleOptIn($observer->getSubscriber()->getStatus())) {
                //Status 4 is set while double optin is active, this is prevent from multiply upserts*
                return true;
            }

            if ($this->checkIssetProgrammaticallyNewsletterStatus($observer->getSubscriber()->getEmail())) {
                $this->session->setProgrammaticallyNewsletterStatus(null);
                return true;
            }

            if (self::$counter > 0) {
                return true;
            }

            if (($this->session->getLastSubscribedEmail() == $observer->getSubscriber()->getEmail())
                && !$this->isSubscriberStatusChangedChecker($observer->getSubscriber())
            ) {
                return true;
            }

            $customerId = $observer->getSubscriber()->getCustomerId();

            if ($customerId != 0) {
                $Contact = $this->modelContact->getContactByCustomerId(
                    $customerId,
                    $observer->getSubscriber(),
                    true,
                    ($observer->getEvent() ? $observer->getEvent()->getName() : '')
                );
            } else {
                $Contact = $this->modelContact
                    ->getContactBySubscriber(
                        $observer->getSubscriber(),
                        $observer->getSubscriber(),
                        ($observer->getEvent() ? $observer->getEvent()->getName() : '')
                    );
            }

            $Contact->getOptions()
                ->setTagScoring(false)
                ->setTags(Conf::getInstance()->getNewsletterTags());

            $this->setContactApiDoubleOptInChecker($Contact, $observer->getSubscriber());

            $this->transferContact($Contact);

            self::$counter++;

            $this->session->setLastSubscribedEmail($Contact->getEmail());
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * Api double optin checker for contact
     *
     * @param Contact $Contact
     * @param mixed $Subscriber
     * @return Contact
     */
    protected function setContactApiDoubleOptInChecker(Contact $Contact, $Subscriber)
    {
        try {
            $apiDoubleOptin = $this->CustomerSession->getApiDoubleOptInData();
            $email = $Contact->getEmail();

            if (is_array($apiDoubleOptin)
                && array_key_exists($email, $apiDoubleOptin)
                && $apiDoubleOptin[$email]) {
                return $Contact;
            }

            if (Conf::getInstance()->getApiDoubleOptIn()->getEnabled()
                && $Contact->getOptions()->getIsSubscribes()
            ) {
                if (!empty($apiDoubleOptin)) {
                    $apiDoubleOptin = array_merge($apiDoubleOptin, [$email => true]);
                } else {
                    $apiDoubleOptin = [$email => true];
                }

                $this->session->setProgrammaticallyNewsletterStatus(hash('haval128,5', $email));
                $Subscriber->setStatus(Subscriber::STATUS_UNCONFIRMED)->save();

                $this->CustomerSession->setApiDoubleOptInData($apiDoubleOptin);
            }
            return $Contact;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * Customer subscription checker
     *
     * @param mixed $subscriber
     * @return bool
     */
    protected function isSubscriberStatusChangedChecker($subscriber)
    {
        try {
            $previousStatus = (int) $subscriber->getOrigData('subscriber_status');
            $currentStatus = (int) $subscriber->getStatus();

            if ($previousStatus != $currentStatus) {
                return true;
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
        return false;
    }

    /**
     * Prevent double optin checker
     *
     * @param mixed $subscriberStatus - one of Magento\Newsletter\Model\Subscriber statuses
     * @return bool
     */
    protected function checkPreventUpsertsWithDoubleOptIn($subscriberStatus)
    {
        if ($subscriberStatus == Subscriber::STATUS_UNCONFIRMED//FYI STATUS_UNCONFIRMED must be 4
            && Conf::getInstance()->getApiDoubleOptIn()->getEnabled() != false
        ) {
            return true;
        }

        return false;
    }

    /**
     * Check if hook is firing when module wants to change contact status on platform
     *
     * Return true if status for email is set programmatically by module
     *
     * @param string $hookEmail - email for which hook is firing out
     * @return bool
     */
    protected function checkIssetProgrammaticallyNewsletterStatus($hookEmail)
    {
        if ($this->session->getProgrammaticallyNewsletterStatus() != null
            && hash('haval128,5', $hookEmail) == $this->session->getProgrammaticallyNewsletterStatus()
        ) {
            return true;
        }

        return false;
    }
}
