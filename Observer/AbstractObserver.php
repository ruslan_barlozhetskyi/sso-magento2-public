<?php

namespace Benhauer\Salesmanago\Observer;

use Psr\Log\LoggerInterface;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface as MagentoObserverInterface;
use Magento\Checkout\Model\Session;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\UrlInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\NoSuchEntityException;

use SALESmanago\Exception\Exception;
use SALESmanago\Factories\FactoryOrganizer;
use SALESmanago\Entity\Contact\Contact;
use SALESmanago\Entity\Event\Event;
use SALESmanago\Entity\Response;

use Benhauer\Salesmanago\Model\Contact as ContactModel;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Helper\Conf;
use Benhauer\Salesmanago\Helper\CookieManager;
use Benhauer\Salesmanago\Model\Event as EventModel;

abstract class AbstractObserver implements MagentoObserverInterface
{
    public const
        UPSERT_FLAG_STATUS   = 'statusUpsert',
        UPSERT_FLAG_ALL_DATA = 'allDataUpser';

    /**
     * @var Session
     */
    public $session;

    /**
     * @var StoreInterface
     */
    public $store;

    /**
     * @var CookieManager
     */
    public $cookieManagerHelper;

    /**
     * @var Subscriber
     */
    public $subscriber;

    /**
     * @var mixed
     */
    public $model;

    /**
     * @var CustomerRepositoryInterface
     */
    public $customerRepositoryInterface;

    /**
     * @var MagentoDateTime
     */
    public $objDate;

    /**
     * @var FactoryOrganizer
     */
    public $factoryOrganizer;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var mixed
     */
    public $CustomerSession;

    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var EventModel
     */
    protected $modelEvent;

    /**
     * @var ContactModel
     */
    protected $modelContact;

    /**
     * @var mixed
     */
    protected $modelSettings;

    /**
     * @var Event
     */
    protected $Event;

    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var int
     */
    protected $scopeId = 0;

    /**
     * @var string
     */
    protected $scope = 'default';

    /**
     * @var ConfigModel
     */
    protected $confModel;

    /**
     * AbstractObserver constructor.
     *
     * @param Session $session
     * @param StoreInterface $store
     * @param Subscriber $subscriber
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CookieManagerInterface $cookieManager
     * @param CookieManager $cookieManagerHelper
     * @param MagentoDateTime $objDate
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     * @param ContactModel $modelContact
     * @param EventModel $modelEvent
     * @param ConfigModel $confModel
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Session $session,
        StoreInterface $store,
        Subscriber $subscriber,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CookieManagerInterface $cookieManager,
        CookieManager $cookieManagerHelper,
        MagentoDateTime $objDate,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger,
        ContactModel $modelContact,
        EventModel $modelEvent,
        ConfigModel $confModel,
        Event $Event,
        StoreManagerInterface $storeManager
    ) {
        $this->session                     = $session;
        $this->store                       = $store;
        $this->subscriber                  = $subscriber;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->factoryOrganizer            = $factoryOrganizer;
        $this->objDate                     = $objDate;
        $this->cookieManager               = $cookieManager;
        $this->cookieManagerHelper         = $cookieManagerHelper;
        $this->logger                      = $logger;
        $this->modelContact                = $modelContact;
        $this->confModel                   = $confModel;
        $this->modelEvent                  = $modelEvent;
        $this->Event                       = $Event;
        $this->storeManager                = $storeManager;
    }

    /**
     * Overwrite execute
     *
     * @param Observer $observer
     * @return bool|void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->resolveStoreScope();

            if (empty(Conf::getInstance()->getClientId())
                && $this->scopeId === 0
            ) {
                return true;
            } elseif (empty(Conf::getInstance()->getClientId())) {
                //change config to get from 0
                $this->resolveStoreScope(0);
            }

            if ($observer->getEvent()->getName() != 'customer_register_success'
                && Conf::getInstance()->getActiveSynchronization()
            ) {
                Conf::getInstance()->setActiveSynchronization(false);
            }

            $this->hookAct($observer);
        } catch (Exception $e) {
            $this->logger->critical($e->getLogMessage());
        }
    }

    /**
     * Sends contact to SM
     *
     * @param Contact $Contact
     * @return Response
     * @throws Exception
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    protected function transferContact(Contact $Contact)
    {
        $sender = $this->factoryOrganizer->getInst(
            FactoryOrganizer::CONTACT_AND_EVENT_TRANSFER_C,
            Conf::getInstance()
        );

        $Response = $sender->transferContact($Contact);

        $this->cookieManagerHelper->setCookie(
            CookieManager::CLIENT_COOKIE,
            $Response->getField('contactId'),
            Conf::getInstance()->getContactCookieTtl()
        );

        return $Response;
    }

    /**
     * Sends event to SM
     *
     * @param Event $Event
     * @return Response
     * @throws Exception
     */
    protected function transferEvent(Event $Event)
    {
        $sender = $this->factoryOrganizer->getInst(
            FactoryOrganizer::CONTACT_AND_EVENT_TRANSFER_C,
            Conf::getInstance()
        );

        return $sender->transferEvent($Event);
    }

    /**
     * Transfer contact and event in same api method
     *
     * @param Contact $Contact
     * @param Event $Event
     * @return Response
     * @throws CookieSizeLimitReachedException
     * @throws Exception
     * @throws FailureToSendException
     * @throws InputException
     */
    protected function transferBoth(Contact $Contact, Event $Event)
    {
        $sender = $this->factoryOrganizer->getInst(
            FactoryOrganizer::CONTACT_AND_EVENT_TRANSFER_C,
            Conf::getInstance()
        );

        $Response = $sender->transferBoth($Contact, $Event);
        $this->cookieManagerHelper->setCookie(
            CookieManager::CLIENT_COOKIE,
            $Response->getField('contactId')
        );

        return $Response;
    }

    /**
     * Main method
     *
     * @param Observer $observer
     * @return mixed
     */
    public function hookAct(Observer $observer)
    {
        return $observer;
    }

    /**
     * Set scopeId and scope in controller and in confModel
     *
     * Use request parameter store
     *
     * @param int $forceTo - scope id, force resole config by passed scope id;
     * @throws Exception
     */
    protected function resolveStoreScope($forceTo = null)
    {
        try {
            if ($forceTo !== null) {
                $this->scopeId = $forceTo;
            } else {
                //set store id by top select option chosen
                $this->scopeId = $this->storeManager->getStore()->getId() ?? $this->scopeId;
            }

            $this->scopeId = (int) $this->scopeId;
            $this->scope = $this->scopeId === 0
                ? 'default'
                : ScopeInterface::SCOPE_STORES;

            $this->confModel
                ->setScopeId($this->scopeId)
                ->setScope($this->scope)
                ->getCachedConf();

        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
