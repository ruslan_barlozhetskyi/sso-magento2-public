<?php

namespace Benhauer\Salesmanago\Observer;

use \Psr\Log\LoggerInterface;

use Magento\Checkout\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Event\Observer;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as MagentoDateTime;
use Magento\Newsletter\Model\Subscriber;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

use SALESmanago\Entity\Event\Event;
use SALESmanago\Factories\FactoryOrganizer;

use Benhauer\Salesmanago\Observer\AbstractObserver as SalesmanagoAbstractObserver;
use Benhauer\Salesmanago\Helper\CookieManager;
use Benhauer\Salesmanago\Model\Config as ConfigModel;
use Benhauer\Salesmanago\Model\Contact as ContactModel;
use Benhauer\Salesmanago\Model\Event as EventModel;

class CustomerModified extends SalesmanagoAbstractObserver
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * CustomerModified constructor.
     *
     * @param Session $session
     * @param StoreInterface $store
     * @param Subscriber $subscriber
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param CookieManagerInterface $cookieManager
     * @param CookieManager $cookieManagerHelper
     * @param MagentoDateTime $objDate
     * @param FactoryOrganizer $factoryOrganizer
     * @param LoggerInterface $logger
     * @param ContactModel $modelContact
     * @param EventModel $modelEvent
     * @param ConfigModel $confModel
     * @param Event $Event
     * @param StoreManagerInterface $storeManager
     * @param CustomerSession $customerSession
     */
    public function __construct(
        Session $session,
        StoreInterface $store,
        Subscriber $subscriber,
        CustomerRepositoryInterface $customerRepositoryInterface,
        CookieManagerInterface $cookieManager,
        CookieManager $cookieManagerHelper,
        MagentoDateTime $objDate,
        FactoryOrganizer $factoryOrganizer,
        LoggerInterface $logger,
        ContactModel $modelContact,
        EventModel $modelEvent,
        ConfigModel $confModel,
        Event $Event,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession
    ) {
        parent::__construct(
            $session,
            $store,
            $subscriber,
            $customerRepositoryInterface,
            $cookieManager,
            $cookieManagerHelper,
            $objDate,
            $factoryOrganizer,
            $logger,
            $modelContact,
            $modelEvent,
            $confModel,
            $Event,
            $storeManager
        );

        $this->customerSession = $customerSession;
    }

    /**
     * Overwrite hookAct()
     *
     * @param Observer $observer
     * @return Observer|mixed|void
     */
    public function hookAct(Observer $observer)
    {
        try {
            $Contact = $this->modelContact
                ->getContactByCustomerId(
                    $this->getCustomer($observer)->getId(),
                    '',
                    false,
                    ($observer->getEvent() ? $observer->getEvent()->getName() : '')
                );

            //this will update contact email when contact changes it
            if (!empty($this->customerSession->getCustomer()->getEmail())
                && $this->customerSession->getCustomer()->getEmail() != $Contact->getEmail()
            ) {
                //set $Contact email as new
                $Contact
                    ->getOptions()
                    ->setNewEmail($Contact->getEmail());
                //set customer email from session (previous) as Contact email
                $Contact->setEmail($this->customerSession->getCustomer()->getEmail());
            }

            $this->transferContact($Contact);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * Get customer by observer email
     *
     * @param mixed $observer
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomer($observer)
    {
        return $this->customerRepositoryInterface->get($observer->getData('email'));
    }
}
