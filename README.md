SALESmanago integration for Magento 2
=======================

Magento 2 package for SALESmanago integration

Just install the package, login to it from you Magento admin panel and it is ready!

More at https://support.salesmanago.com/integration-with-magento-1-2/

The scope of integration
========================

Basic scope of integration

Integrate external events like ‘cart’ or ‘purchase’ both for contacts who are logged in and those who are not – as long as they have a tracking cookie smclient
Export of contacts and historical transactions from e-commerce platform
Automatic addition of a monitoring code to a website
Possibility of assigning tags when a contact registers, signs up for a newsletter or makes a purchase, later these contacts with tags will be sent to the system
Automatic addition of such features as Live Chat, Pop-up Basic and Web Push
Contact form integration

Adding a monitoring parameter to contacts that filled in a contact form
Transfer of information given when contacts register and log in
Transfer of contact data (e.g. name and surname, email address, telephone number, address, company name, date of birth, date of creating a contact, tags assigned in the shop)
Transfer of contacts along with their opt-in/opt-out 
status

Back synchronisation (This feature enables checking if contacts who register on e-commerce platform can already be found in SALESmanago database. When a contact exists in the database with opt-in status but while registration does not give a consent, in both SALESmanago system and e-commerce platform the contact has opt-in status.)
Transfer of contacts together with tags indicating name of the shop

Requirements
============

* PHP >= 5.5
* cURL Extension

Installation
============
For production environment:

    composer require benhauer/salesmanago-integration --no-dev
    
For developer environment:

    composer require benhauer/salesmanago-integration

Run next Magento commands:
    
    php bin/magento setup:upgrade;
    
    php bin/magento setup:di:compile;
    
    php bin/magento setup:static-content:deploy;
    
    php bin/magento cache:flush;

