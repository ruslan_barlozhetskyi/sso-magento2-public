SALESmanago plugin for Magento2
-------------------------------
Version 4.1.7 12.07.2022
- Added copy buttons for callback urls in configuration;
- Added system debug tab;
- Fixed contact address country to ISO3166;
- Add configurable connection timeouts;
- Add configurable retry request feature;
- Update api-sso-util to 3.1.1

Version 4.1.6 02.06.2022
- Improved available settings before user is logged-in to SALESmanago account
- Fixed callbacks URLs when using "Add Store Code to URLs";
- Fixed expiry time for cookies;
- Added support for a virtual downloadable product type;
- Added validation for ADOI configuration fields;
- Removed overwriting empty tags field with default values;

Version 4.1.5 16.05.2022
- Added ignoring Contacts with specified email domains;
- Added coupon code (detail11) and discount value (detail12) to PURCHASE Events;
- Fixed transferring Contacts in PWA;
- Added GrandTotal (base) as purchase value to CART and PURCHASE Events;
- Fixed a bug with empty owner list after a first login;
- Fixed synchronization of newsletter statuses for a single session of user

Version 4.1.4 02.02.2022
- fix duplication PURCHASE events on order status changed;
- remove option to send PURCHASE with CRON method;

Version 4.1.3 25.01.2022
- add properties for contact from customer with customer group details;
- add metadata for contacts;
- add MetaDataHelper;

Version 4.1.2 21.01.2022
- refactor for exports;
- small changes with monitoring code;
- fix recovery cart out of stock qty error;
- refactor export js;

Version 4.1.1 10.12.2021
- add update owners list button to configuration;
- add disable monitoring code option in configuration;
- add fix processing CART and PURCHASE with virtual products;

Version 4.1.0 20.11.2021
- add export quotes as CART event;
- fix $shipping description in PURCHASE event while origData is null;
- change PURCHASE event detail4 from base currency code to order currency code;
- add to event CART detail4 with quote currency code;
- add to event PURCHASE detail5 with shipping description;
- add to event PURCHASE detail6 with order payment method;
- add to event PURCHASE detail7 with order shipping address countryId;
- add to events CART and PURCHASE detail8 with categories ids;
- add to events CART and PURCHASE detail9 with store locale;
- add to events CART and PURCHASE detail10 with metadata;

Version 4.0.9 03.11.2021
- fix null shipping_description for checkout_type_multishipping_create_orders_single hook;

Version 4.0.8 21.09.2021
- added configurable redirection after cart recovery;
- added configurable cookieTTl;
- added optionals order states for purchase events export;

Version 4.0.7 26.07.2021
- replace a reference to return type array CartRecovery::convertProductToEntity();
- fixed exports datetime parse;
- fixed add customer form admin panel;
- fixed overwrite empty billingAddress for Contact in Export;
- change product ids in PURCHASE events for configurable product types to exact product variant id;
- remove variant_id option form Event ProductId configuration select;
 
Version 4.0.6 07.07.2021
- fixed set tags to contact after purchase and guest purchase;

Version 4.0.5 25.06.2021
- functionality of cart recovery has been added;
- change product id to child product id in external events for configurable products;

Version 4.0.4 13.05.2021
- getting the shipping method as a SALESmanago 5th detail;
- add change\edit contact email from customer panel;

Version 4.0.3-p2 05.05.2021
 - fixed missing configuration active value;
  
Version 4.0.3.1 29.04.2021
 - fixed export event bug;  

Version 4.0.3 09.04.2021
 - fixed export mechanism;  
 - fixed login to component;
 - fixed add Purchase event for previous contactId;
 - fix set ApiDoubleOptIn while Synchronize from SM functionality is On;

Version 4.0.2 12.03.2021
 - fix purchase events;
 - fix admin create customer; 

Version 4.0.1 12.03.2021
 - removed unnecessary wish list observer;

Version 4.0.0 24.02.2021
 - configuration mechanism changed;
 - add custom endpoint field;
 - remove create account functionality;
 - totally refactored admin controllers and models;
 - change exports;
 - add smbanners and customjs options;
 - refactored hook mechanism;

Version 3.2.1 13.01.2021
 - add editable config location field;
 - add editable location input;
 - fix add customer form admin issue;
 - change monitoring code parent block (place);
 
Version 3.2.0 18.11.2020
 - implementation of new transfer method for contacts and events;
 - fix add smclient;
 - add synchronization;
 - fix config\login username;
 
Version 3.2.0 23.11.2020
 - fix syncRule;
 - fix newsletter subscribers statuses;
 - fix Go to configuration button
 
Version 3.0.9-p2 19.08.2020
 - fix newsletter subscription;

Version 3.0.9 06.08.2020
- fix callback optin/optout - when main config is logout callback not working
- fix sending emails to contact after export
- fix miss province after export

--

Version 3.0.9-RC1 26.07.2020
- fix callback optin/optout - when main config is 
logout callback not working

--

Version 3.0.8 16.07.2020
- fix optin/optout when confirm subscribe from url

--

Version 3.0.7-p1 18.06.2020
- add etc/acl.xml
- change TEST_MAIL value - the error concerns changes on the salesmanago side
- remove pageFactory from In and Out
- add guest purchase tag
- do not send the newsletter tag when the newsletter is not confirmed
- endpoint salesmanago.es

--

Version 3.0.6 21.02.2020
- adjust to magento 2.1.x;
- fix optout contact with guest purchase upsert;
- fix order cancelling;

--

Version 3.0.6-RC1 29.01.2020
- rename namespaces for Observers class;
- adjust to magento 2.1.x;
- fix optout contact with guest purchase upsert; 

--

Version 3.0.5-p1 22.01.2020
- remove sales_order_save_after duplicate hook for OrderStatusChanged;
- remove checkout_submit_all_after duplicate hook for KlarnaCheckout;

--

Version 3.0.5 23.12.2019
- change namespaces for AbstractModel;
- change logo;

--

Version 3.0.4 07.11.2019
- add new purchase events configuration;
- add metrix;
- minor bug fixes;

--

Version 3.0.3 07.10.2019
- UI fixes
- minor bug fixes

--

Version 3.0.2 Refreshed 12.09.2019
- fix welcome ui component error;

--

Version 3.0.1 Refreshed 08.09.2019
- fix exports;

--

Version 3.0.0 Refreshed 14.08.2019
- views upgrade to magento views;

--

Version 2.1.7 Refreshed 14.08.2019
- add new api method;

--

Version 2.1.6 Refreshed 30.07.2019
- add cancel event;
- views update;

--

Version 2.1.5 Refreshed 25.07.2019
- fix CART for new API;

--

Version 2.1.4 Refreshed 24.07.2019
- new API;

--

Version 2.1.3 Refreshed 05.07.2019
- refactoring tags manager;

--

Version 2.1.2 Refreshed 07.06.2019
- refactoring for magento 2.1.6;

--

Version 2.1.1 Refreshed 07.06.2019
- fix contact create;

--

Version 2.1.0 Refreshed 27.04.2019
- add store views;
- add order statuses/types;
- add Klarna-payment catch purchase;
- add storeId to each store views;

--

Version 2.0.7 Refreshed 19.04.2019
- fix newsletter callback;

--

Version 2.0.6 Refreshed 10.04.2019
- update service fix synchronize rule;

--

Version 2.0.5 Refreshed 18.03.2019
- add double opt in;

--

Version 2.0.4 Refreshed 24.01.2019
- add error handling;
- add advanced exports;
- security fixes;

--

Version 2.0.3 Refreshed 08.12.2018
- add error handling for observer;

--

Version 2.0.2 Refreshed 03.12.2018
- added Tagger;
- fix tags repeat;

--

Version 2.0.1 Refreshed 09.10.2018
- updated admin views;
- added synchronize rule to sychronize contact;

--

Version 2.0.0 Refreshed 06.09.2018
- new sso core;

--

Version 1.0.4 Refreshed 12.03.2018
- Remove underscore from variables; 
- Clear spaces;
- Use short array syntactics;
- Add comments;
- Remove clear SQL questions;
- Methods refactor;
- Remove logic from views files;

--

Version 1.0.3 Refreshed 30.03.2018
- Added StoreView tag when client upsert;
- Added CallBacks for optin and optout client;

--

Version 1.0.2 Refreshed 20.03.2018
- newsletter subscription fix

--

Version 1.0.1 Refreshed 09.01.2018
- Fixed package's name in composer ad registration.php + xml's

--

Version 1.0.0 Refreshed 06.10.2017
- New structure for marketplace and composer

--

Version 1.0.0 Released 15.02.2017

Integration Scope:
- Synchronize contact after:
    - Registration,
    - Login,
    - Purchase without register (as a guest),
    - Address modification,
    - Newsletter subscription modification,
- Synchronized external events:
    - Cart ( For signed up members and not - but with cookie )
    - Purchase ( For signed up members and guests )
- Adding "smclient" cookie:
    - For every client synchronization,
- Automatic Sm script adding,
- Standard integration fields, js working timeout and tags fields:
    - Newsletter tags,
    - Registration tags,
    - Purchase tags,
- Plugin translation for:
    - English,
    - Polish
    
Tested on:
    - Magento CE: 2.3.0
    - Magento CE: 2.3.5
    - Magento CE: 2.4.0
