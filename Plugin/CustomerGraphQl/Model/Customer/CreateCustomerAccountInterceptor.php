<?php

namespace Benhauer\Salesmanago\Plugin\CustomerGraphQl\Model\Customer;

use Magento\CustomerGraphQl\Model\Customer\CreateCustomerAccount;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Api\Data\StoreInterface;
use Psr\Log\LoggerInterface;

class CreateCustomerAccountInterceptor
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Core event manager proxy
     *
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * Constructor CreateCustomerAccountInterceptor
     *
     * @param LoggerInterface $logger
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        LoggerInterface $logger,
        ManagerInterface $eventManager
    ) {
        $this->logger = $logger;
        $this->eventManager = $eventManager;
    }

    /**
     * Interceptors after method action
     *
     * @param CreateCustomerAccount $subject
     * @param $result
     * @param array $data
     * @param StoreInterface $store
     * @return mixed
     */
    public function afterExecute(
        CreateCustomerAccount $subject,
        $result,
        array $data,
        StoreInterface $store
    ) {
        $this->eventManager->dispatch('sm_pwa_customer_create_interceptor', ['customer' => $result]);

        return $result;
    }
}
