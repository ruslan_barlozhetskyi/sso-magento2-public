<?php

namespace Benhauer\Salesmanago\Plugin\QuoteGraphQl\Model\Resolver;

use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Model\ResourceModel\Quote\QuoteIdMask as QuoteIdMaskResource;
use Psr\Log\LoggerInterface;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\QuoteGraphQl\Model\Resolver\AddProductsToCart;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Framework\Event\ManagerInterface;


class AddProductsToCartInterceptor
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var QuoteIdMaskFactory
     */
    private $quoteIdMaskFactory;

    /**
     * @var QuoteIdMaskResource
     */
    private $quoteIdMaskResource;

    /**
     * Core event manager proxy
     *
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * AddProductsToCartInterceptor constructor.
     *
     * @param LoggerInterface $logger
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param CartRepositoryInterface $cartRepository
     * @param QuoteIdMaskResource $quoteIdMaskResource
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        LoggerInterface $logger,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartRepositoryInterface $cartRepository,
        QuoteIdMaskResource $quoteIdMaskResource,
        ManagerInterface $eventManager
    ) {
        $this->logger = $logger;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->cartRepository = $cartRepository;
        $this->quoteIdMaskResource = $quoteIdMaskResource;
        $this->eventManager = $eventManager;
    }

    /**
     * Interceptors after method action
     *
     * @param AddProductsToCart $subject
     * @param $result
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     */
    public function afterResolve(
        AddProductsToCart $subject,
        $result,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $cart = $this->getCartByMaskedCartId($args['cartId']);
        $this->eventManager->dispatch('sm_pwa_cart_interceptor', ['cart' => $cart]);
        return $result;
    }

    /**
     * Return cart by masked quote id (by cartId)
     *
     * @param $maskedQuoteId
     * @return CartInterface
     */
    protected function getCartByMaskedCartId($maskedQuoteId)
    {
        try {
            $quoteIdMask = $this->quoteIdMaskFactory->create();
            $this->quoteIdMaskResource->load($quoteIdMask, $maskedQuoteId, 'masked_id');

            return $this->cartRepository->get($quoteIdMask->getQuoteId());
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getTraceAsString());
        }
    }
}
